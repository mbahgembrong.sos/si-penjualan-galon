<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/x-icon" href="{{ asset('icon.png') }}" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- IonIcons -->
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <!-- Theme style -->
    <link rel="stylesheet"
        href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/ekko-lightbox/ekko-lightbox.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('framework/select2/dist/css/select2.css') }}">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oxygen:wght@400;700&display=swap" rel="stylesheet">

    <title>Toko Galon Sido Mulyo</title>
</head>

<body style="font-family: 'Oxygen', sans-serif; background-image: url('/img_galon/bg.jpg');
background-size: 100%; background-repeat: no-repeat;
  background-position: right top;
  background-attachment: fixed;">
    <!-- Navbar -->
    <div class="fixed-top">
        <nav class="navbar navbar-expand-lg navbar-dark bg-info">
            <div class="container">

                <img src="{{asset('icon.png')}}" alt="" srcset=""
                    class="rounded-circle img-bordered border-info bg-white" width="45px">

                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('index') }}#">Home <span
                                    class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('index') }}#produk">Produk <span
                                    class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('index') }}#about">Tentang Kami <span
                                    class="sr-only">(current)</span></a>
                        </li>
                    </ul>
                </div>

                <div class="nav-item">
                    <a href="{{ route('login') }}" class="btn btn-outline-light border-light"><i
                            class="fas fa-sign-in-alt"></i></a>
                </div>
            </div>
        </nav>
    </div>
    <!-- NavbarEnd -->

    @yield('content')

    <!-- Footer -->
    <footer class="bg-info" style="min-height: 150px;">
        <div class="container ">
            <div class="row py-3">
                <div class="col-md-5 container">
                    <div class="text-center">
                        <img src="{{asset('icon.png')}}" alt="" srcset=""
                            class="rounded-circle img-bordered border-info mb-3 bg-white" width="100px">

                        <h4 class="text-light mb-4">
                            TOKO AIR GALON SIDO MULYO
                        </h4>
                        <p>
                            Toko Air Galon Sido Mulyo adalah toko yang menjual air galon dengan harga terjangkau dengan
                            kualitas terbaik langsung dari produsen
                        </p>
                        <div>
                            <button class=" btn btn-info px-1 py-0" style="">
                                <i class="fab fa-facebook-square" style="font-size: 45px;"></i>
                            </button>
                            <button class=" btn btn-info px-1 py-0" style="">
                                <i class="fab fa-instagram-square" style="font-size: 45px;"></i>
                            </button>
                            <button class=" btn btn-info px-1 py-0" style="">
                                <i class="fab fa-youtube-square" style="font-size: 45px;"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 my-4"></div>
                <div class="col-md-5 text-center container">
                    <div class="mapouter mb-4">
                        <div class="gmap_canvas"><iframe width="100%" height="250" id="gmap_canvas"
                                src="https://maps.google.com/maps?q=kediri%20wates&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        </div>
                    </div>
                    <p>
                        <i class="fas fa-map-marked-alt"></i> Jl. Basuki Rahmat No.15, Pocanan, Kec. Kota,
                        Kota Kediri,
                        Jawa Timur 64129
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)

    </script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- ChartJS -->
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script>
    <!-- JQVMap -->
    <script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <!-- Summernote -->
    <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('dist/js/pages/dashboard3.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>

    <script src="{{ asset('framework/select2/dist/js/select2.js') }}"></script>

    <!-- Page level plugins -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js">
    </script>

    <script src="{{asset('plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Toasr -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
        })

        $(document).ready(function () {
            $('#dataTable').DataTable();
        });

        @if(Session::has('sukses'))
        Toast.fire({
            icon: 'success',
            title: "{{Session::get('sukses')}}"
        })
        @endif
        @if(Session::has('gagal'))
        Toast.fire({
            icon: 'error',
            title: "{{Session::get('gagal')}}"
        })
        @endif
        // LogOut
        @if(Session::has('logout'))
        Toast.fire({
            icon: 'warning',
            title: "{{Session::get('logout')}}"
        })
        @endif

        $(document).delegate('*[data-toggle="lightbox"]', 'click', function (event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });

        $(".select2-responsive").select2({
            theme: 'classic',
        });

    </script>
    @yield('footer')
</body>

</html>