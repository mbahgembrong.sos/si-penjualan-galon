<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$title}} | Penjualan Galon</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- IonIcons -->
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <!-- Theme style -->
    <link rel="stylesheet"
        href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/ekko-lightbox/ekko-lightbox.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css') }}">
    <!-- DATE TIME PICKER -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
        rel="stylesheet" />
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('framework/select2/dist/css/select2.css') }}">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->

<body class="sidebar-mini layout-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Messages Dropdown Menu -->
                <li class="nav-item">
                    <a class="btn btn-outline-danger" href="{{ route('logout') }}" role="button"><i
                            class="fas fa-sign-out-alt"></i></a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <nav class="m-0 nav-flat">
            <aside class="main-sidebar sidebar-light-success elevation-4">
                <!-- Brand Logo -->
                <a href="{{ route('karyawan.dashboard') }}" class="brand-link bg-success">
                    <img src="{{ url('icon.png') }}" class="brand-image img-circle elevation-2 bg-light"
                        style="opacity: .8">
                    <h5 class="brand-text">Galon Sido Mulyo</h5>
                </a>

                <!-- Sidebar -->
                <div class="sidebar">
                    <div
                        class="user-panel mt-3 pb-2 mb-3 d-flex {{request()->is('karyawan/profile') ? ' border-success' : ''}}">
                        <div class="image mt-3 mx-2">
                            <a href="{{ route('karyawan.profile') }}">
                                <i class="fas fa-user-circle fa-lg" style="size: 1000px; font-size: 35px;"></i>
                            </a>
                        </div>
                        <div class="info">
                            <a href="{{ route('karyawan.profile') }}"
                                class="d-block font-weight-bold">{{Auth::user()->username}} <br>
                                <i class="rounded px-2 bg-success">
                                    @if (Auth::user()-> role == 1)
                                    Admin
                                    @elseif (Auth::user()-> role == 2)
                                    Karyawan
                                    @endif
                                </i>
                            </a>
                        </div>
                    </div>
                    <!-- Sidebar Menu -->
                    <nav class="mt-2 pb-5">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                            data-accordion="false">
                            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                            <li class="nav-item has-treeview">
                                <a href="{{ route('karyawan.dashboard') }}"
                                    class="nav-link {{request()->is('karyawan/dashboard') ? ' active' : ''}}">
                                    <i class="nav-icon fas fa-desktop"></i>
                                    <p>
                                        Dashboard
                                    </p>
                                </a>
                            </li>

                            <li class="nav-header font-weight-bold">USERS
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('karyawan.kurir') }}"
                                    class="nav-link {{request()->is('karyawan/kurir*') ? ' active' : ''}}">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>
                                        Kurir
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('karyawan.pelanggan') }}"
                                    class="nav-link {{request()->is('karyawan/pelanggan*') ? ' active' : ''}}">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>
                                        Pelanggan
                                    </p>
                                </a>
                            </li>

                            <li class="nav-header font-weight-bold">GALON
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('karyawan.galon') }}"
                                    class="nav-link {{request()->is('karyawan/galon*') ? ' active' : ''}}">
                                    <i class="nav-icon fas fa-tint"></i>
                                    <p>
                                        Data Galon
                                    </p>
                                </a>
                            </li>

                            <li class="nav-header font-weight-bold">TRANSAKSI
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('karyawan.transaksi') }}"
                                    class="nav-link {{request()->is('karyawan/transaksi') ? ' active' : ''}}">
                                    <i class=" nav-icon fas fa-file-invoice"></i>
                                    <p>
                                        Data Transaksi
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('karyawan.transaksi.tracking') }}"
                                    class="nav-link {{request()->is('karyawan/transaksi/tracking') ? ' active' : ''}}">
                                    <i class=" nav-icon fas fa-eye"></i>
                                    <p>
                                        Tracking Transaksi
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('karyawan.transaksi.rekap') }}"
                                    class="nav-link {{request()->is('karyawan/transaksi/rekap') ? ' active' : ''}}">
                                    <i class=" nav-icon fas fa-file-alt"></i>
                                    <p>
                                        Rekap Transaksi
                                    </p>
                                </a>
                            </li>

                            <li class="nav-header font-weight-bold">LAIN-LAIN
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('karyawan.desa') }}"
                                    class="nav-link {{request()->is('karyawan/desa*') ? ' active' : ''}}">
                                    <i class=" nav-icon fas fa-home"></i>
                                    <p>
                                        Data Desa
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('karyawan.diskon') }}"
                                    class="nav-link {{request()->is('karyawan/diskon*') ? ' active' : ''}}">
                                    <i class=" nav-icon fas fa-percent"></i>
                                    <p>
                                        Data Diskon
                                    </p>
                                </a>
                            </li>

                            <li class="nav-header font-weight-bold">GRAFIK
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('karyawan.grafikPenjualan') }}"
                                    class="nav-link {{request()->is('karyawan/grafik/penjualan') ? ' active' : ''}}">
                                    <i class="nav-icon fas fa-chart-bar"></i>
                                    <p>
                                        Penjualan
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('karyawan.grafikPendapatan') }}"
                                    class="nav-link {{request()->is('karyawan/grafik/pendapatan') ? ' active' : ''}}">
                                    <i class="nav-icon fas fa-chart-bar"></i>
                                    <p>
                                        Pendapatan
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>

            @yield('content')

            <!-- Main Footer -->
            <footer class="main-footer">
                <strong>Copyright &copy; 2022 <a href="http://adminlte.io">Toko Galon Sido Mulyo</a>.</strong>
                All rights reserved.
                <div class="float-right d-none d-sm-inline-block">

                </div>
            </footer>
    </div>
    </section>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)

    </script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- ChartJS -->
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script>
    <!-- JQVMap -->
    <script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <!-- Summernote -->
    <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('dist/js/pages/dashboard3.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>

    <script src="{{ asset('framework/select2/dist/js/select2.js') }}"></script>

    <!-- Page level plugins -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js">
    </script>

    <script src="{{asset('plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Toasr -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
        })

        @if(Session::has('sukses'))
        Toast.fire({
            icon: 'success',
            title: "{{Session::get('sukses')}}"
        })
        @endif
        @if(Session::has('gagal'))
        Toast.fire({
            icon: 'error',
            title: "{{Session::get('gagal')}}"
        })
        @endif
        // LogOut
        @if(Session::has('logout'))
        Toast.fire({
            icon: 'success',
            title: "{{Session::get('logout')}}"
        })
        @endif

        $(document).delegate('*[data-toggle="lightbox"]', 'click', function (event) {
            event.preventDefault();
            $(this).ekkoLightbox({
                showArrows: false,
            });
        });

        $(".select2-responsive").select2({
            theme: 'classic',
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>

    <!-- DATE TIME PICKER -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>

    <script type="text/javascript">
        $("#datepicker").datepicker({
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years"
        });
        $("#daypicker").datepicker({
            format: "dd",
            viewMode: "days", 
            minViewMode: "days"
        });
        $("#monthpicker").datepicker({
            format: "MM",
            viewMode: "months", 
            minViewMode: "months"
        });

        $("#yearM").datepicker({
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years"
        });
        $("#day").datepicker({
            format: "dd",
            viewMode: "days", 
            minViewMode: "days"
        });
        $("#month").datepicker({
            format: "MM",
            viewMode: "months", 
            minViewMode: "months"
        });
    </script>
    @yield('footer')
</body>

</html>