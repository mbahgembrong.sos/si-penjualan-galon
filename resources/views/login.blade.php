@extends('layouts.guestApp')

@section('content')
<div style="background-color: rgba(0, 0, 0, 0.3); padding:75px 20px;" class="container-fluid">
  <div style="container" class="py-1">
    <div class="col container-fluid text-center card my-5 bg-info" style="max-width: 400px;">
      <div class="card-header">
        <img src="{{asset('icon.png')}}" alt="" srcset=""
          class="rounded-circle img-bordered border-success bg-light mb-2" width="60px">
        <h4>LOGIN</h4>
      </div>
      <div class="card-body text-left bg-white">
        <form action="{{ route('login.request') }}" method="POST">
          @csrf
          <div class="form-group">
            <input type="text" class="form-control" id="username" name="username" placeholder="Username">
          </div>
          <div class="form-group">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
          </div>
          <button type="submit" class="form-control btn btn-outline-info"> <i class="fas fa-sign-in-alt"></i>
            Login</button>
        </form>
      </div>
      <div class="card-footer">

      </div>
    </div>
  </div>
</div>

@endsection