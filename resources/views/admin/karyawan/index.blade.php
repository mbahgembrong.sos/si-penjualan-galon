@extends('layouts.adminApp', [$title = 'Data Karyawan'])

@section('content')
<!-- Content Wrapper. Contains page content -->
<section class="content">
    <div class="container-fluid">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Karyawan</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Users</a></li>
                                <li class="breadcrumb-item active">Karyawan</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <div class="content">
                <div class="container-fluid">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h5 class="m-0 font-weight-bold text-primary">Data Karyawan
                                <div class="float-right">
                                    <a href="#" class="btn btn-outline-success" data-toggle="modal"
                                        data-target="#modalTambahKaryawan">
                                        <i class="fas fa-plus"></i></a>
                                </div>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive text-nowrap">
                                <table class="table table-striped dataTable" id="dataTable" width="100%"
                                    cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="6%" class="text-center">#</th>
                                            <th>Username</th>
                                            <th>Nama</th>
                                            <th>TTL</th>
                                            <th>JK</th>
                                            <th>Alamat</th>
                                            <th width="10%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Username</th>
                                            <th>Nama</th>
                                            <th>TTL</th>
                                            <th>JK</th>
                                            <th>Alamat</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- /.control-sidebar -->

    <div class="modal fade" id="modalTambahKaryawan" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog-scrollable modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bg-success">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Data Karyawan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-light">
                    <form enctype="multipart/form-data" autocomplete="off" id="formTambahKaryawan"
                        class="needs-validation" novalidate>
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <label for="username">Username</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Username" name="username" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="password">Password</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="password" placeholder="Password" name="password"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 mt-3">
                                <hr class="border-success">
                            </div>

                            <div class="col-md-12">
                                <label for="nama">Nama </label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Nama Karyawan" name="nama" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="">Tempat dan Tanggal Lahir</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-6 mt-1">
                                        <input type="text" placeholder="Tempat Lahir" name="tempat_lahir"
                                            class="form-control">
                                    </div>

                                    <div class="col-md-6 mt-1">
                                        <input type="date" placeholder="Tanggal Lahir" name="tanggal_lahir"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <select name="jenis_kelamin" id="" class="form-control">
                                            <option value="L" selected>Laki-laki</option>
                                            <option value="P">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="alamat">Alamat</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <textarea name="alamat" class="form-control" placeholder="Alamat"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer container-fluid">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                <button type="button" id="tambahData" class="btn btn-primary">Tambah Data</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEditKaryawan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog-scrollable modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bg-info">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Data Karyawan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-light">
                    <form enctype="multipart/form-data" autocomplete="off" id="formEditKaryawan" data-id=""
                        class="needs-validation" novalidate>
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <label for="username">Username</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Username" name="username" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="password">Password</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="password" placeholder="Password" name="password"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 mt-3">
                                <hr class="border-info">
                            </div>

                            <div class="col-md-12">
                                <label for="nama">Nama </label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Nama Karyawan" name="nama" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="">Tempat dan Tanggal Lahir</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-6 mt-1">
                                        <input type="text" placeholder="Tempat Lahir" name="tempat_lahir"
                                            class="form-control">
                                    </div>

                                    <div class="col-md-6 mt-1">
                                        <input type="date" placeholder="Tanggal Lahir" name="tanggal_lahir"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <select name="jenis_kelamin" id="" class="form-control">
                                            <option value="L" selected>Laki-laki</option>
                                            <option value="P">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="alamat">Alamat</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <textarea name="alamat" class="form-control" placeholder="Alamat"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer container-fluid">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                <button type="button" id="simpanData" class="btn btn-primary">Simpan Data</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('footer')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        getKaryawan()

        var Notif = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
        })

        function getKaryawan() {
            var htmlview ,no = 0
            $.ajax({
                url: "{{ route('admin.karyawan.data') }}",
                type: 'GET',
                success: function (res) {
                    $('tbody').html('')
                    $.each(res, function(i, data){
                            no = no+1
                            htmlview += `
                                <tr>
                                    <td class="text-center">`+no+`</td>
                                    <td>`+data.username+`</td>
                                    <td>`+data.nama+`</td>
                                    <td>`+data.tempat_lahir+`, `+data.tanggal_lahir+`</td>
                                    <td>`+data.jenis_kelamin+`</td>
                                    <td>`+data.alamat+`</td>
                                    <td> 
                                        <button id="editkry" class="btn btn-sm btn-outline-info" onClick="detailKaryawan(`+data.id+`)">
                                            <i id="editkry" class="fas fa-edit"></i></button>
                                        <button id="deletekry" class="btn btn-sm btn-outline-danger" onClick="deleteKaryawan(`+data.id+`)">
                                            <i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>`
                        });
                    $('tbody').html(htmlview)
                    $('#dataTable').dataTable();
                }
            })
        }

        $('#tambahData').on('click', function (e){
            e.preventDefault()
            $('.is-invalid').removeClass('is-invalid');
            $('.invalid-feedback').remove();
            addKaryawan()
        })

        $('#simpanData').on('click', function (e){
            e.preventDefault()
            $('.is-invalid').removeClass('is-invalid');
            $('.invalid-feedback').remove();
            updateKaryawan()
        })

        function addKaryawan() {
            $.ajax({
                url : "{{ route('admin.karyawan.add') }}",
                type : 'POST',
                data : $('#formTambahKaryawan').serialize(),
                dataType : 'json',
                success : function (res) {
                    if (res.code == 200){
                        $('#formTambahKaryawan').trigger('reset')
                        $('#modalTambahKaryawan').modal('hide')
                        
                        Notif.fire({
                            icon : 'success',
                            title : res.message,
                        })
                        
                        getKaryawan()
                    }
                },
                error : function (err) {
                    console.log(err);
                    Notif.fire({
                        icon : 'error',
                        title : 'Gagal Menyimpan Data Karyawan',
                    });
                    
                    $.each(err.responseJSON.errors, function (i, error) {
                        var el = $('#formTambahKaryawan').find('[name="'+i+'"]');
                        el.addClass('is-invalid');
                        el.after('<div class="invalid-feedback">'+error[0]+'</div>');
                    });
                }
            })
        }

        function detailKaryawan(id_kry) {
            var id = id_kry
            var _url = "{{ route('admin.karyawan.detail', '') }}/"+id
            $.ajax({
                url: _url,
                type: 'GET',
                success: function (res) {
                    $('#modalEditKaryawan').modal('show')
                    $('#formEditKaryawan').attr("data-id",id)
                    $.each(res, function (i, data) {
                        var el = $('#formEditKaryawan').find('[name="'+i+'"]');
                        el.val(data);
                    })
                }
            })
        }

        function updateKaryawan() {
            var id = $('#formEditKaryawan').data('id')
            var _url = "{{ route('admin.karyawan.update', '') }}/"+id

            $.ajax({
                url : _url,
                type : 'POST',
                data : $('#formEditKaryawan').serialize() + "&id=" +id,
                dataType : 'json',
                success : function (res) {
                    if (res.code == 200){
                        $('#formEditKaryawan').trigger('reset')
                        $('#modalEditKaryawan').modal('hide')
                        
                        Notif.fire({
                            icon : 'success',
                            title : res.message,
                        })
                        
                        getKaryawan()
                    }
                },
                error : function (err) {
                    console.log(err);
                    Notif.fire({
                        icon : 'error',
                        title : 'Gagal Menyimpan Data Karyawan',
                    });
                    
                    $.each(err.responseJSON.errors, function (i, error) {
                        var el = $('#formEditKaryawan').find('[name="'+i+'"]');
                        el.addClass('is-invalid');
                        el.after('<div class="invalid-feedback">'+error[0]+'</div>');
                    });
                }
            })
        }

        function deleteKaryawan(id_kry) {
            Swal.fire({
                title: "Apakah anda yakin hapus data ini?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Ya, Hapus!",
                cancelButtonText: "Tidak",
            })
            .then((result) => {
                if (result.isConfirmed) {
                    var id = id_kry
                    var _url = "{{ route('admin.karyawan.delete', '') }}/"+id
                    var _token   = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({
                        url: _url,
                        type: 'DELETE',
                        data : {
                            _token : _token
                        },
                        success: function (res) {
                            Notif.fire({
                                icon : 'success',
                                title : res.message,
                            })
                            
                            getKaryawan()
                        },
                        error : function (err) {
                            console.log(err);
                        }
                    })
                }
            });
        }
    </script>
    @endsection