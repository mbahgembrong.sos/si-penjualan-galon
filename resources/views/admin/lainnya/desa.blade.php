@extends('layouts.adminApp', [($title = 'Data Desa')])

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <section class="content">
        <div class="container-fluid">
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark">Desa</h1>
                            </div><!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="#">Lainnya</a></li>
                                    <li class="breadcrumb-item active">Data Desa</li>
                                </ol>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <div class="content">
                    <div class="container-fluid">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h5 class="m-0 font-weight-bold text-primary">Data Desa
                                    <div class="float-right">
                                        <a href="#" class="btn btn-outline-success" data-toggle="modal"
                                            data-target="#modalTambahDesa">
                                            <i class="fas fa-plus"></i></a>
                                    </div>
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive text-nowrap">
                                    <table class="table table-striped dataTable" id="dataTable" width="100%"
                                        cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th width="6%" class="text-center">#</th>
                                                <th>Desa</th>
                                                <th class="text-right">Ongkir</th>
                                                <th class="text-right">Kode Pos</th>
                                                <th width="10%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th width="6%" class="text-center">#</th>
                                                <th>Desa</th>
                                                <th class="text-right">Ongkir</th>
                                                <th class="text-right">Kode Pos</th>
                                                <th width="10%">Aksi</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- /.control-sidebar -->

        <div class="modal fade" id="modalTambahDesa" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
            <div class="modal-dialog-scrollable modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content bg-success">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Data Desa</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body bg-light">
                        <form enctype="multipart/form-data" autocomplete="off" id="formTambahDesa" class="needs-validation"
                            novalidate>
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="nama_desa">Desa</label>
                                    <div class="col-md-14 row">
                                        <div class="col-md-12">
                                            <input type="text" placeholder="Nama Desa" name="nama_desa"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label for="ongkir">Ongkir</label>
                                    <div class="col-md-14 row">
                                        <div class="col-md-12">
                                            <input type="number" placeholder="Ongkir ke desa" name="ongkir" min="0"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label for="kode_pos">Kode Pos</label>
                                    <div class="col-md-14 row">
                                        <div class="col-md-12">
                                            <input type="text" placeholder="kode_pos ke desa" name="kode_pos"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer container-fluid">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                    <button type="button" id="tambahData" class="btn btn-primary">Tambah Data</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalEditDesa" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
            <div class="modal-dialog-scrollable modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content bg-info">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Data Desa</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body bg-light">
                        <form enctype="multipart/form-data" autocomplete="off" id="formEditDesa" data-id=""
                            class="needs-validation" novalidate>
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="nama_desa">Desa</label>
                                    <div class="col-md-14 row">
                                        <div class="col-md-12">
                                            <input type="text" placeholder="Nama Desa" name="nama_desa"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label for="ongkir">Ongkir</label>
                                    <div class="col-md-14 row">
                                        <div class="col-md-12">
                                            <input type="number" placeholder="Ongkir ke desa" name="ongkir"
                                                min="0" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label for="kode_pos">Kode Pos</label>
                                    <div class="col-md-14 row">
                                        <div class="col-md-12">
                                            <input type="text" placeholder="kode_pos ke desa" name="kode_pos"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer container-fluid">
                                    <button type="button" class="btn btn-secondary"
                                        data-dismiss="modal">Kembali</button>
                                    <button type="button" id="simpanData" class="btn btn-primary">Simpan Data</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('footer')
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            getDesa()

            var Notif = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true,
            })

            function getDesa() {
                var htmlview, no = 0
                $.ajax({
                    url: "{{ route('admin.desa.data') }}",
                    type: 'GET',
                    success: function(res) {
                        $('tbody').html('')
                        $.each(res, function(i, data) {
                            no = no + 1

                            htmlview += `
                                <tr>
                                    <td class="text-center">` + no + `</td>
                                    <td>` + data.nama_desa + `</td>
                                    <td class="text-right">Rp. ` + data.ongkir +
                                `</td>
                                  <td>` + data.kode_pos +
                                `</td>
                                    <td>
                                        <button id="editplg" class="btn btn-sm btn-outline-info" onClick="detailDesa(` +
                                data.id +
                                `)">
                                            <i class="fas fa-edit"></i></button>
                                        <button id="deleteplg" class="btn btn-sm btn-outline-danger" onClick="deleteDesa(` +
                                data.id + `)">
                                            <i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>`
                        });
                        $('tbody').html(htmlview)
                        $('#dataTable').dataTable();
                    }
                })
            }

            $('#tambahData').on('click', function(e) {
                e.preventDefault()
                $('.is-invalid').removeClass('is-invalid');
                $('.invalid-feedback').remove();
                addDesa()
            })

            $('#simpanData').on('click', function(e) {
                e.preventDefault()
                $('.is-invalid').removeClass('is-invalid');
                $('.invalid-feedback').remove();
                updateDesa()
            })

            function addDesa() {
                $.ajax({
                    url: "{{ route('admin.desa.add') }}",
                    type: 'POST',
                    data: $('#formTambahDesa').serialize(),
                    dataType: 'json',
                    success: function(res) {
                        if (res.code == 200) {
                            $('#formTambahDesa').trigger('reset')
                            $('#modalTambahDesa').modal('hide')

                            Notif.fire({
                                icon: 'success',
                                title: res.message,
                            })

                            getDesa()
                        }
                    },
                    error: function(err) {
                        Notif.fire({
                            icon: 'error',
                            title: 'Gagal Menyimpan Data Desa',
                        });

                        $.each(err.responseJSON.errors, function(i, error) {
                            var el = $('#formTambahDesa').find('[name="' + i + '"]');
                            el.addClass('is-invalid');
                            el.after('<div class="invalid-feedback">' + error[0] + '</div>');
                        });
                    }
                })
            }

            function detailDesa(id_ds) {
                var id = id_ds
                var _url = "{{ route('admin.desa.detail', ':id') }}"
                _url = _url.replace(':id', id)

                $.ajax({
                    url: _url,
                    type: 'GET',
                    success: function(res) {
                        $('#modalEditDesa').modal('show')
                        $('#formEditDesa').attr("data-id", id)
                        $.each(res, function(i, data) {
                            var el = $('#formEditDesa').find('[name="' + i + '"]');
                            el.val(data);
                        })
                    }
                })
            }

            function updateDesa() {
                var id = $('#formEditDesa').data('id')
                var _url = "{{ route('admin.desa.update', ':id') }}"
                _url = _url.replace(':id', id)

                $.ajax({
                    url: _url,
                    type: 'POST',
                    data: $('#formEditDesa').serialize() + "&id=" + id,
                    dataType: 'json',
                    success: function(res) {
                        if (res.code == 200) {
                            $('#formEditDesa').trigger('reset')
                            $('#modalEditDesa').modal('hide')

                            Notif.fire({
                                icon: 'success',
                                title: res.message,
                            })

                            getDesa()
                        }
                    },
                    error: function(err) {
                        Notif.fire({
                            icon: 'error',
                            title: 'Gagal Menyimpan Data Desa',
                        });

                        $.each(err.responseJSON.errors, function(i, error) {
                            var el = $('#formEditDesa').find('[name="' + i + '"]');
                            el.addClass('is-invalid');
                            el.after('<div class="invalid-feedback">' + error[0] + '</div>');
                        });
                    }
                })
            }

            function deleteDesa(id_ds) {
                Swal.fire({
                        title: "Apakah anda yakin hapus data ini?",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Ya, Hapus!",
                        cancelButtonText: "Tidak",
                    })
                    .then((result) => {
                        if (result.isConfirmed) {
                            var id = id_ds
                            var _url = "{{ route('admin.desa.delete', ':id') }}"
                            _url = _url.replace(':id', id)
                            var _token = $('meta[name="csrf-token"]').attr('content');

                            $.ajax({
                                url: _url,
                                type: 'DELETE',
                                data: {
                                    _token: _token
                                },
                                success: function(res) {
                                    Notif.fire({
                                        icon: 'success',
                                        title: res.message,
                                    })

                                    getDesa()
                                },
                                error: function(err) {
                                    console.log(err);
                                }
                            })
                        }
                    });
            }
        </script>
    @endsection
