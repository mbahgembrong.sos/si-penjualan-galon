@extends('layouts.adminApp', [$title = 'Data Diskon'])

@section('content')
<!-- Content Wrapper. Contains page content -->
<section class="content">
    <div class="container-fluid">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Diskon</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Lainnya</a></li>
                                <li class="breadcrumb-item active">Data Diskon</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <div class="content">
                <div class="container-fluid">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h5 class="m-0 font-weight-bold text-primary">Data Diskon
                                <div class="float-right">
                                    <a href="#" class="btn btn-outline-success" data-toggle="modal"
                                        data-target="#modalTambahDiskon">
                                        <i class="fas fa-plus"></i></a>
                                </div>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive text-nowrap">
                                <table class="table table-striped dataTable" id="dataTable" width="100%"
                                    cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="6%" class="text-center">#</th>
                                            <th>Nama Diskon</th>
                                            <th>Jenis Potongan</th>
                                            <th class="text-right">Diskon</th>
                                            <th width="10%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th width="6%" class="text-center">#</th>
                                            <th>Nama Diskon</th>
                                            <th>Jenis Potongan</th>
                                            <th class="text-right">Diskon</th>
                                            <th width="10%">Aksi</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- /.control-sidebar -->

    <div class="modal fade" id="modalTambahDiskon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog-scrollable modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bg-success">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Data Diskon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-light">
                    <form enctype="multipart/form-data" autocomplete="off" id="formTambahDiskon"
                        class="needs-validation" novalidate>
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <label for="nama_diskon">Nama Diskon</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Nama Diskon" name="nama_diskon"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="jenis_potongan">Jenis Potongan</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <select name="jenis_potongan" id="" class="form-control">
                                            <option value="Langsung">Langsung (-)</option>
                                            <option value="Persen">Persen (%)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="diskon">Diskon</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="number" placeholder="Diskon" name="diskon" min="0"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer container-fluid">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                <button type="button" id="tambahData" class="btn btn-primary">Tambah Data</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEditDiskon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog-scrollable modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bg-info">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Data Diskon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-light">
                    <form enctype="multipart/form-data" autocomplete="off" id="formEditDiskon" data-id=""
                        class="needs-validation" novalidate>
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <label for="nama_diskon">Nama Diskon</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Nama Diskon" name="nama_diskon"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="jenis_potongan">Jenis Potongan</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <select name="jenis_potongan" id="" class="form-control">
                                            <option value="Langsung">Langsung (-)</option>
                                            <option value="Persen">Persen (%)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="diskon">Diskon</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="number" placeholder="Diskon" name="diskon" min="0"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer container-fluid">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                <button type="button" id="simpanData" class="btn btn-primary">Simpan Data</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('footer')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        getDiskon()

        var Notif = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
        })

        function getDiskon() {
            var htmlview ,no = 0, jn_potongan ,diskon
            $.ajax({
                url: "{{ route('admin.diskon.data') }}",
                type: 'GET',
                success: function (res) {
                    $('tbody').html('')
                    $.each(res, function(i, data){
                            no = no+1

                            if (data.jenis_potongan == 'Langsung'){
                                jn_potongan = data.jenis_potongan+' (-)'
                                diskon = '- Rp. '+data.diskon
                            } else {
                                jn_potongan = data.jenis_potongan+' (%)'
                                diskon = data.diskon+'%'
                            }

                            htmlview += `
                                <tr>
                                    <td class="text-center">`+no+`</td>
                                    <td>`+data.nama_diskon+`</td>
                                    <td>`+jn_potongan+`</td>
                                    <td class="text-right">`+diskon+`</td>
                                    <td> 
                                        <button id="editplg" class="btn btn-sm btn-outline-info" onClick="detailDiskon(`+data.id+`)">
                                            <i class="fas fa-edit"></i></button>
                                        <button id="deleteplg" class="btn btn-sm btn-outline-danger" onClick="deleteDiskon(`+data.id+`)">
                                            <i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>`
                        });
                    $('tbody').html(htmlview)
                    $('#dataTable').dataTable();
                }
            })
        }

        $('#tambahData').on('click', function (e){
            e.preventDefault()
            $('.is-invalid').removeClass('is-invalid');
            $('.invalid-feedback').remove();
            addDiskon()
        })

        $('#simpanData').on('click', function (e){
            e.preventDefault()
            $('.is-invalid').removeClass('is-invalid');
            $('.invalid-feedback').remove();
            updateDiskon()
        })

        function addDiskon() {
            $.ajax({
                url : "{{ route('admin.diskon.add') }}",
                type : 'POST',
                data : $('#formTambahDiskon').serialize(),
                dataType : 'json',
                success : function (res) {
                    if (res.code == 200){
                        $('#formTambahDiskon').trigger('reset')
                        $('#modalTambahDiskon').modal('hide')
                        
                        Notif.fire({
                            icon : 'success',
                            title : res.message,
                        })
                        
                        getDiskon()
                    }
                },
                error : function (err) {
                    Notif.fire({
                        icon : 'error',
                        title : 'Gagal Menyimpan Data Diskon',
                    });

                    $.each(err.responseJSON.errors, function (i, error) {
                        var el = $('#formTambahDiskon').find('[name="'+i+'"]');
                        el.addClass('is-invalid');
                        el.after('<div class="invalid-feedback">'+error[0]+'</div>');
                    });
                }
            })
        }

        function detailDiskon(id_dskn) {
            var id = id_dskn
            var _url = "{{ route('admin.diskon.detail', ":id") }}"
            _url = _url.replace(':id',id)
            
            $.ajax({
                url: _url,
                type: 'GET',
                success: function (res) {
                    $('#modalEditDiskon').modal('show')
                    $('#formEditDiskon').attr("data-id",id)
                    $.each(res, function (i, data) {
                        var el = $('#formEditDiskon').find('[name="'+i+'"]');
                        el.val(data);
                    })
                }
            })
        }

        function updateDiskon() {
            var id = $('#formEditDiskon').data('id')
            var _url = "{{ route('admin.diskon.update', ":id") }}"
            _url = _url.replace(':id',id)

            $.ajax({
                url : _url,
                type : 'POST',
                data : $('#formEditDiskon').serialize() + "&id=" +id,
                dataType : 'json',
                success : function (res) {
                    if (res.code == 200){
                        $('#formEditDiskon').trigger('reset')
                        $('#modalEditDiskon').modal('hide')
                        
                        Notif.fire({
                            icon : 'success',
                            title : res.message,
                        })
                        
                        getDiskon()
                    }
                },
                error : function (err) {
                    Notif.fire({
                        icon : 'error',
                        title : 'Gagal Menyimpan Data Diskon',
                    });
                    
                    $.each(err.responseJSON.errors, function (i, error) {
                        var el = $('#formEditDiskon').find('[name="'+i+'"]');
                        el.addClass('is-invalid');
                        el.after('<div class="invalid-feedback">'+error[0]+'</div>');
                    });
                }
            })
        }

        function deleteDiskon(id_dskn) {
            Swal.fire({
                title: "Apakah anda yakin hapus data ini?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Ya, Hapus!",
                cancelButtonText: "Tidak",
            })
            .then((result) => {
                if (result.isConfirmed) {
                    var id = id_dskn
                    var _url = "{{ route('admin.diskon.delete', ":id") }}"
                    _url = _url.replace(':id',id)
                    var _token   = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({
                        url: _url,
                        type: 'DELETE',
                        data : {
                            _token : _token
                        },
                        success: function (res) {
                            Notif.fire({
                                icon : 'success',
                                title : res.message,
                            })
                            
                            getDiskon()
                        },
                        error : function (err) {
                            console.log(err);
                        }
                    })
                }
            });
        }
    </script>
    @endsection