@extends('layouts.adminApp', [$title = 'Data Pembelian Galon'])

@section('content')
<!-- Content Wrapper. Contains page content -->
<section class="content">
    <div class="container-fluid">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Pembelian Galon</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Pembelian Galon</a></li>
                                <li class="breadcrumb-item active">Data Pembelian Galon</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <div class="content">
                <div class="container-fluid">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h5 class="m-0 font-weight-bold text-primary">Data Pembelian Galon
                                <div class="float-right">
                                    <a href="#" class="btn btn-outline-success" data-toggle="modal"
                                        data-target="#modalTambahPembelian">
                                        <i class="fas fa-plus"></i></a>
                                </div>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive text-nowrap">
                                <table class="table table-striped dataTable" id="dataTable" width="100%"
                                    cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="6%" class="text-center">#</th>
                                            <th>Merk</th>
                                            <th>Isi Galon</th>
                                            <th class="text-right">Harga Beli</th>
                                            <th>Jumlah Beli</th>
                                            <th class="text-right">Total Pembelian</th>
                                            <th>Tanggal Pembelian</th>
                                            <th width="10%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th width="6%" class="text-center">#</th>
                                            <th>Merk</th>
                                            <th>Isi Galon</th>
                                            <th class="text-right">Harga Beli</th>
                                            <th>Jumlah Beli</th>
                                            <th class="text-right">Total Pembelian</th>
                                            <th>Tanggal Pembelian</th>
                                            <th width="10%">Aksi</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- /.control-sidebar -->

    <div class="modal fade" id="modalTambahPembelian" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog-scrollable modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bg-success">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Data Pembelian Galon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-light">
                    <form enctype="multipart/form-data" autocomplete="off" id="formTambahPembelian"
                        class="needs-validation" novalidate>
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <label for="galon">Galon</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <select name="id_galon" id="data_galon" class="form-control"
                                            onchange="changeTotal()">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="harga_satuan">Harga Satuan</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="number" name="harga_satuan" id="harga_satuan" class="form-control"
                                            min="0" value="0" onchange="changeTotal()">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="harga_jual">Harga Jual</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="number" name="harga_jual" id="harga_jual" class="form-control"
                                            min="0" value="0">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="jumlah_beli">Jumlah Beli</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="number" id="jumlah_beli" name="jumlah_beli" class="form-control"
                                            min="0" value="0" onchange="changeTotal()">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="total_harga_beli">Total Pembelian</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="number" id="total" placeholder="Total Pembelian"
                                            name="total_harga_beli" min="0" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer container-fluid">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                <button type="button" id="tambahData" class="btn btn-primary">Tambah Data</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('footer')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        getPembelian()
        getDataGalon()

        var Notif = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
        })

        function changeTotal()
        {
            var harga = $('#harga_satuan').val()
            var jumlah = $('#jumlah_beli').val()
            var total = harga * jumlah

            $('#total').val(total)
        }

        function getDataGalon() {
            var htmlview = '<option value="" selected disabled=""> -- Pilih Galon -- </option> '
            $.ajax({
                url: "{{ route('admin.pembelian.dataGalon') }}",
                type: 'GET',
                success: function(res) {
                    $.each(res, function(i, data){
                        htmlview += `
                            <option value="`+data.id+`">`+data.merk+` (`+data.isi_galon+`L)</option>
                        `
                    })
                    $('#data_galon').html(htmlview);
                    console.log(res);
                }
            })
        }

        $('#data_galon').on('change', function(){
            var id = $(this).val()
            getDataGalonSelected(id)
        })

        function getDataGalonSelected(id) {
            var _url = "{{ route('admin.pembelian.dataGalonSelected',":id") }}"
            _url = _url.replace(':id',id)

            $.ajax({
                url: _url,
                type: 'GET',
                success: function(res) {
                    $('#harga_satuan').val(res.harga_awal);
                    $('#harga_jual').val(res.harga_jual);
                }
            })
        }

        function getPembelian() {
            var htmlview ,no = 0 ,tgl_beli
            $.ajax({
                url: "{{ route('admin.pembelian.data') }}",
                type: 'GET',
                success: function (res) {
                    $('tbody').html('')
                    $.each(res, function(i, data){
                            no = no+1
                            var d = new Date(data.created_at);
                            tgl_beli = d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear();

                            htmlview += `
                                <tr>
                                    <td class="text-center">`+no+`</td>
                                    <td>`+data.merk+`</td>
                                    <td>`+data.isi_galon+`L</td>
                                    <td class="text-right">Rp. `+data.harga_satuan+`</td>
                                    <td class="text-success" ><b> +`+data.jumlah_beli+`</b></td>
                                    <td class="text-right">Rp. `+data.total_harga_beli+`</td>
                                    <td>`+tgl_beli+`</td>
                                    <td> 
                                        <button id="deleteplg" class="btn btn-sm btn-outline-danger" onClick="deletePembelian(`+data.id+`)">
                                            <i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>`
                        });
                    $('tbody').html(htmlview)
                    $('#dataTable').dataTable();
                }
            })
        }

        $('#tambahData').on('click', function (e){
            e.preventDefault()
            $('.is-invalid').removeClass('is-invalid');
            $('.invalid-feedback').remove();
            addPembelian()
        })

        function addPembelian() {
            $.ajax({
                url : "{{ route('admin.pembelian.add') }}",
                type : 'POST',
                data : $('#formTambahPembelian').serialize(),
                dataType : 'json',
                success : function (res) {
                    if (res.code == 200){
                        $('#formTambahPembelian').trigger('reset')
                        $('#modalTambahPembelian').modal('hide')
                        
                        Notif.fire({
                            icon : 'success',
                            title : res.message,
                        })
                        
                        getPembelian()
                    }
                },
                error : function (err) {
                    Notif.fire({
                        icon : 'error',
                        title : 'Gagal Menyimpan Data Pembelian Galon',
                    });

                    $.each(err.responseJSON.errors, function (i, error) {
                        var el = $('#formTambahPembelian').find('[name="'+i+'"]');
                        el.addClass('is-invalid');
                        el.after('<div class="invalid-feedback">'+error[0]+'</div>');
                    });
                }
            })
        }

        function deletePembelian(id_beli) {
            Swal.fire({
                title: "Apakah anda yakin hapus data ini?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Ya, Hapus!",
                cancelButtonText: "Tidak",
            })
            .then((result) => {
                if (result.isConfirmed) {
                    var id = id_beli
                    var _url = "{{ route('admin.pembelian.delete', ":id") }}"
                    _url = _url.replace(':id',id)
                    var _token   = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({
                        url: _url,
                        type: 'DELETE',
                        data : {
                            _token : _token
                        },
                        success: function (res) {
                            Notif.fire({
                                icon : 'success',
                                title : res.message,
                            })
                            
                            getPembelian()
                        },
                        error : function (err) {
                            console.log(err);
                        }
                    })
                }
            });
        }
    </script>
    @endsection