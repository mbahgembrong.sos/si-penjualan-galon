@extends('layouts.adminApp', [$title = 'Data Galon'])

@section('content')
<!-- Content Wrapper. Contains page content -->
<section class="content">
    <div class="container-fluid">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Galon</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Galon</a></li>
                                <li class="breadcrumb-item active">Data Galon</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <div class="content">
                <div class="container-fluid">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h5 class="m-0 font-weight-bold text-primary">Data Galon
                                <div class="float-right">
                                    <a href="#" class="btn btn-outline-success" data-toggle="modal"
                                        data-target="#modalTambahGalon">
                                        <i class="fas fa-plus"></i></a>
                                </div>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive text-nowrap">
                                <table class="table table-striped dataTable" id="dataTable" width="100%"
                                    cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="6%" class="text-center">#</th>
                                            <th>Gambar</th>
                                            <th>Merk</th>
                                            <th>Isi</th>
                                            <th>Stok</th>
                                            <th class="text-right">Harga Awal</th>
                                            <th class="text-right">Harga Jual</th>
                                            <th width="10%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Gambar</th>
                                            <th>Merk</th>
                                            <th>Isi</th>
                                            <th>Stok</th>
                                            <th class="text-right">Harga Awal</th>
                                            <th class="text-right">Harga Jual</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- /.control-sidebar -->

    <div class="modal fade" id="modalTambahGalon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog-scrollable modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bg-success">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Data Galon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-light">
                    <form enctype="multipart/form-data" autocomplete="off" id="formTambahGalon" class="needs-validation"
                        novalidate>
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <label for="merk">Merk</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Merk" name="merk" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="isi_galon">Isi Galon (Liter)</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="number" placeholder="Isi Galon" name="isi_galon" value="1" min="1"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="harga_awal">Harga Awal</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="number" placeholder="Harga Awal" name="harga_awal" min="0"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="harga_jual">Harga Jual</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="number" placeholder="Harga Jual" name="harga_jual" min="0"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 mt-3">
                                <label for="gambar_galon">Gambar Galon</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="file" placeholder="Gambar Galon" name="gambar_galon"
                                            class="form-control-file">
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer container-fluid">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                <button type="button" id="tambahData" class="btn btn-primary">Tambah Data</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEditGalon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog-scrollable modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bg-info">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Data Galon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-light">
                    <form enctype="multipart/form-data" autocomplete="off" id="formEditGalon" data-id=""
                        class="needs-validation" novalidate>
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <label for="merk">Merk</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Merk" name="merk" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="isi_galon">Isi Galon (Liter)</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="number" placeholder="Isi Galon" name="isi_galon" value="1" min="1"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="harga_awal">Harga Awal</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="number" placeholder="Harga Awal" name="harga_awal" min="0"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="harga_jual">Harga Jual</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="number" placeholder="Harga Jual" name="harga_jual" min="0"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 mt-3">
                                <label for="gambar_galon">Gambar Galon</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <input type="file" placeholder="Gambar Galon" name="gambar_galon"
                                            class="form-control-file">
                                    </div>
                                    <span class="text-muted text-sm ml-2">*upload gambar jika ingin mengubah
                                        gambar</span>
                                </div>
                            </div>

                            <div class="modal-footer container-fluid">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                <button type="button" id="simpanData" class="btn btn-primary">Simpan Data</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('footer')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        getGalon()

        var Notif = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
        })

        function getGalon() {
            var htmlview ,no = 0
            $.ajax({
                url: "{{ route('admin.galon.data') }}",
                type: 'GET',
                success: function (res) {
                    $('tbody').html('')
                    $.each(res, function(i, data){
                            no = no+1
                            var img_link = "{{ url('/img_galon/:gambar_galon') }}"
                            img_link = img_link.replace(":gambar_galon",data.gambar_galon)

                            htmlview += `
                                <tr>
                                    <td class="text-center">`+no+`</td>
                                    <td> <a href="`+img_link+`" data-toggle="lightbox"><img src="`+img_link+`"  width="50px"></a>
                                    <td>`+data.merk+`</td>
                                    <td>`+data.isi_galon+` L</td>
                                    <td>`+data.jml_stok+`</td>
                                    <td class="text-right">Rp. `+data.harga_awal+`</td>
                                    <td class="text-right">Rp. `+data.harga_jual+`</td>
                                    <td> 
                                        <button id="editplg" class="btn btn-sm btn-outline-info" onClick="detailGalon(`+data.id+`)">
                                            <i class="fas fa-edit"></i></button>
                                        <button id="deleteplg" class="btn btn-sm btn-outline-danger" onClick="deleteGalon(`+data.id+`)">
                                            <i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>`
                        });
                    $('tbody').html(htmlview)
                    $('#dataTable').dataTable();
                }
            })
        }

        $('#tambahData').on('click', function (e){
            e.preventDefault()
            $('.is-invalid').removeClass('is-invalid');
            $('.invalid-feedback').remove();
            addGalon()
        })

        $('#simpanData').on('click', function (e){
            e.preventDefault()
            $('.is-invalid').removeClass('is-invalid');
            $('.invalid-feedback').remove();
            updateGalon()
        })

        function addGalon() {
            var formData = new FormData($("#formTambahGalon")[0]);
            $.ajax({
                url : "{{ route('admin.galon.add') }}",
                type : 'POST',
                data : formData,
                async: false, 
                cache: false, 
                contentType: false, 
                processData: false,
                dataType : 'json',
                success : function (res) {
                    if (res.code == 200){
                        $('#formTambahGalon').trigger('reset')
                        $('#modalTambahGalon').modal('hide')
                        
                        Notif.fire({
                            icon : 'success',
                            title : res.message,
                        })
                        
                        getGalon()
                    }
                },
                error : function (err) {
                    Notif.fire({
                        icon : 'error',
                        title : 'Gagal Menyimpan Data Galon',
                    });

                    $.each(err.responseJSON.errors, function (i, error) {
                        var el = $('#formTambahGalon').find('[name="'+i+'"]');
                        el.addClass('is-invalid');
                        el.after('<div class="invalid-feedback">'+error[0]+'</div>');
                    });
                }
            })
        }

        function detailGalon(id_gln) {
            var id = id_gln
            var _url = "{{ route('admin.galon.detail', ":id") }}"
            _url = _url.replace(':id',id)
            
            $.ajax({
                url: _url,
                type: 'GET',
                success: function (res) {
                    $('#modalEditGalon').modal('show')
                    $('#formEditGalon').attr("data-id",id)
                    $.each(res, function (i, data) {
                        if (i == 'gambar_galon') {
                            data = '';
                        }
                        var el = $('#formEditGalon').find('[name="'+i+'"]');
                        el.val(data);
                    })
                }
            })
        }

        function updateGalon() {
            var id = $('#formEditGalon').data('id')
            var _url = "{{ route('admin.galon.update', ":id") }}"
            _url = _url.replace(':id',id)
            var formData = new FormData($("#formEditGalon")[0]);
            $.ajax({
                url : _url,
                type : 'POST',
                data : formData,
                dataType : 'json',
                async: false, 
                cache: false, 
                contentType: false, 
                processData: false,
                success : function (res) {
                    if (res.code == 200){
                        $('#formEditGalon').trigger('reset')
                        $('#modalEditGalon').modal('hide')
                        
                        Notif.fire({
                            icon : 'success',
                            title : res.message,
                        })
                        
                        getGalon()
                    }
                },
                error : function (err) {
                    Notif.fire({
                        icon : 'error',
                        title : 'Gagal Menyimpan Data Galon',
                    });
                    
                    $.each(err.responseJSON.errors, function (i, error) {
                        var el = $('#formEditGalon').find('[name="'+i+'"]');
                        el.addClass('is-invalid');
                        el.after('<div class="invalid-feedback">'+error[0]+'</div>');
                    });
                }
            })
        }

        function deleteGalon(id_gln) {
            Swal.fire({
                title: "Apakah anda yakin hapus data ini?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Ya, Hapus!",
                cancelButtonText: "Tidak",
            })
            .then((result) => {
                if (result.isConfirmed) {
                    var id = id_gln
                    var _url = "{{ route('admin.galon.delete', ":id") }}"
                    _url = _url.replace(':id',id)
                    var _token   = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({
                        url: _url,
                        type: 'DELETE',
                        data : {
                            _token : _token
                        },
                        success: function (res) {
                            Notif.fire({
                                icon : 'success',
                                title : res.message,
                            })
                            
                            getGalon()
                        },
                        error : function (err) {
                            console.log(err);
                        }
                    })
                }
            });
        }
    </script>
    @endsection