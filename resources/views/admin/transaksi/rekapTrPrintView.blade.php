<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Rekap PDF</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
        body {
            margin: 0;
            padding: 0;
        }

        td,
        th,
        span {
            font-size: 12px;
        }

        th {
            font-weight: bold;
        }
    </style>
</head>

<body>
    <div class="container-fluid">

        <span class="text-secondary float-right">{{now()}}</span> <br>
        <h4 class="text-center">
            Laporan Rekap Transaksi
        </h4>
        <h5 class="text-center">
            Toko Sido Mulyo {{ request('bln') }}/{{ request('thn') }}
        </h5>
        <hr>
        <table class="table table-striped table-bordered table-sm mt-4">
            <thead class="bg-dark text-light text-center">
                <tr>
                    <th width="3%" class="text-center">No</th>
                    <th>Tgl Transaksi</th>
                    <th>Pembeli</th>
                    <th>Alamat Kirim</th>
                    <th>Karyawan</th>
                    <th>Kurir</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @php
                $no = 1;
                $totalrekap = 0;
                @endphp

                @foreach ($data as $datatr)
                @php
                $totalrekap = $totalrekap + $datatr->total;
                @endphp
                <tr>
                    <td class="text-center">
                        {{$no++}}
                    </td>
                    <td>
                        {{ $datatr->created_at }}
                    </td>
                    <td>
                        {{ $datatr->nama_pelanggan }}
                    </td>
                    <td>
                        ({{ $datatr->desa }})
                    </td>
                    <td>
                        {{ $datatr->nama_karyawan }}
                    </td>
                    <td>
                        {{ $datatr->nama_kurir }}
                    </td>
                    <td class="text-right">
                        Rp. {{ $datatr->total }}
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot class="table-success">
                <th colspan="6" class="text-right">TOTAL : </th>
                <th class="text-right">Rp. {{$totalrekap}}</th>
            </tfoot>
        </table>
    </div>
</body>

</html>