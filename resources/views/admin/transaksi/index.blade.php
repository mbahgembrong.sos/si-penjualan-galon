@extends('layouts.adminApp', [$title = 'Data Transaksi'])

@section('content')
<!-- Content Wrapper. Contains page content -->
<section class="content">
    <div class="container-fluid">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Transaksi</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
                                <li class="breadcrumb-item active">Data Transaksi</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <div class="content">
                <div class="container-fluid">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h5 class="m-0 font-weight-bold text-primary">Data Transaksi
                                <div class="float-right">
                                </div>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive text-nowrap">
                                <table class="table table-striped dataTable" id="dataTable" width="100%"
                                    cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="10%" class="text-center">Aksi</th>
                                            <th>Tgl Transaksi</th>
                                            <th>Pembeli</th>
                                            <th>Alamat Kirim</th>
                                            <th>Karyawan</th>
                                            <th>Kurir</th>
                                            <th class="text-right">Total</th>
                                            <th>Status</th>
                                            <th>Ket</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th width="10%" class="text-center">Aksi</th>
                                            <th>Tgl Transaksi</th>
                                            <th>Pembeli</th>
                                            <th>Alamat Kirim</th>
                                            <th>Karyawan</th>
                                            <th>Kurir</th>
                                            <th class="text-right">Total</th>
                                            <th>Status</th>
                                            <th>Ket</th>
                                        </tr>
                                    </tfoot>
                                    <tbody id="data">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- /.control-sidebar -->

    <div class="modal fade" id="modalDetailTransaksi" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog-scrollable modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content border-top border-info">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Detail Transaksi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-light">
                    <div class="container-fluid">
                        <div class="border-top border-dark border-bottom text-center">
                            <h5 class="my-1"><b> Toko Sido Mulyo </b></h5>
                        </div>
                        <div class="row">
                            <div class="col-12 text-sm">
                                <table>
                                    <tr>
                                        <th class="align-top" width="100px">No. Transaksi </th>
                                        <td class="align-top" id="idtr">...</td>
                                    </tr>
                                    <tr>
                                        <th class="align-top">Tanggal</th>
                                        <td class="align-top" id="tgltr">...</td>
                                    </tr>
                                    <tr>
                                        <th class="align-top">Pembeli</th>
                                        <td class="align-top" id="pembelitr">...</td>
                                    </tr>
                                    <tr>
                                        <th class="align-top">Alamat</th>
                                        <td class="align-top" id="alamattr">...</td>
                                    </tr>
                                    <tr>
                                        <th class="align-top">Petugas</th>
                                        <td class="align-top" id="krytr">...</td>
                                    </tr>
                                    <tr>
                                        <th class="align-top">Kurir</th>
                                        <td class="align-top" id="krrtr">...</td>
                                    </tr>
                                    <tr>
                                        <th class="align-top">Status</th>
                                        <td class="align-top" id="ststr">
                                            ...
                                        </td>
                                    </tr>
                                </table>
                                <div class="border-bottom border-top border-dark">
                                    <div class="mt-2 text-bold">Galon dibeli :</div>
                                    <ol class="pl-4" id="dataBeli">
                                        ...
                                    </ol>
                                </div>
                                <div>
                                    <ul class="pl-0" style="list-style:none;">
                                        <li>
                                            <b>Sub. Total :</b> <a class="float-right" id="subtotal">...</a>
                                        </li>
                                        <li>
                                            <b>Diskon :</b> <a class="float-right" id="diskon">...</a>
                                        </li>
                                        <li>
                                            <b>Ongkir :</b> <a class="float-right" id="ongkir">...</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="border-top border-dark text-lg">
                                    <ul class="pl-0" style="list-style:none;">
                                        <li>
                                            <b>Total :</b> <b class="float-right" id="total">...</b>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('footer')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        getTransaksi()

        var Notif = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
        })

        function getTransaksi() {
            var htmlview ,tgl_transaksi, alamat ,d ,bukti,statustr
            $.ajax({
                url: "{{ route('admin.transaksi.data') }}",
                type: 'GET',
                success: function (res) {
                    $('#data').html('')
                    $.each(res, function(i, data){

                        if (data.status == 0) {
                        statustr = `<i class="text-light bg-secondary px-2">Belum dikonfirmasi</i>`
                        } else if (data.status == 1) {
                            statustr = `<i class="text-light bg-info px-2">Belum dibayar</i>`
                        } else if (data.status == 2) {
                            statustr = `<i class="text-light bg-warning px-2">Pembayaran Gagal</i>`
                        } else if (data.status == 3) {
                            statustr = `<i class="text-light bg-primary px-2">Proses pengiriman</i>`
                        } else if (data.status == 4) {
                            statustr = `<i class="text-light bg-success px-2">Selesai</i>`
                        } else if (data.status == 5) {
                            statustr = `<i class="text-light bg-danger px-2">Dibatalkan</i>`
                        } 

                        alamat = data.alamat

                        if (data.bukti_transaksi == null) {
                            bukti = `<button id="detail" class="btn btn-sm btn-outline-danger disabled">
                                        <i class="fas fa-file-invoice"></i>
                                        </button>`
                        } else {
                            var img_link = "{{ url('/bukti_transaksi/:buktitr') }}"
                            img_link = img_link.replace(":buktitr",data.bukti_transaksi)

                        bukti = `
                        <a href="`+img_link+`" data-toggle="lightbox">
                        <button id="detail" class="btn btn-sm btn-outline-success">
                                        <i class="fas fa-file-invoice"></i>
                                </button> </a>`
                        }

                            htmlview += `
                                <tr>
                                    <td> 
                                        <button id="detail" class="btn btn-sm btn-outline-info" onClick="detailTransaksi(`+data.id+`)">
                                            <i class="fas fa-eye"></i>
                                        </button>
                                        `+bukti+`
                                    </td>
                                    <td>`+data.created_at+`</td>
                                    <td>`+data.nama_pelanggan+`</td>
                                    <td>`+alamat+`</td>
                                    <td>`+data.nama_karyawan+`</td>
                                    <td>`+data.nama_kurir+`</td>
                                    <td class="text-right text-success"><b> Rp. `+data.total+`</b></td>
                                    <td class="text-center">`+statustr+`</td>
                                    <td>`+data.keterangan+`</td>
                                </tr>`
                        });
                    $('#data').html(htmlview)
                    $('#dataTable').dataTable();
                }
            })
        }

        function detailTransaksi(id){
            var _url = "{{ route('admin.transaksi.getDetailTransaksi',":id") }}"
            _url = _url.replace(':id',id)
            $.ajax({
                url: _url,
                type: 'GET',
                success: function (res) {
                    $('#modalDetailTransaksi').modal('show')

                    var statustr = res.status
                    var htmlDetail = '';
                    var subtotal = 0;
                    var diskon = 0;

                    if (statustr == 0) {
                        statustr = `<i class="text-light bg-secondary px-2">Belum dikonfirmasi</i>`
                    } else if (statustr == 1) {
                        statustr = `<i class="text-light bg-info px-2">Belum dibayar</i>`
                    } else if (statustr == 2) {
                        statustr = `<i class="text-light bg-warning px-2">Pembayaran Gagal</i>`
                    } else if (statustr == 3) {
                        statustr = `<i class="text-light bg-primary px-2">Proses pengiriman</i>`
                    } else if (statustr == 4) {
                        statustr = `<i class="text-light bg-success px-2">Selesai</i>`
                    } else if (statustr == 5) {
                        statustr = `<i class="text-light bg-danger px-2">Dibatalkan</i>`
                    } 

                    if (res.jenis_potongan == 'Langsung') {
                        diskon = '- Rp. '+res.diskon
                    } else {
                        diskon = '- '+res.diskon+'%'
                    }

                    $('#idtr').html(res.id)
                    $('#tgltr').html(res.created_at)
                    $('#pembelitr').html(res.nama_pelanggan)
                    $('#alamattr').html(res.alamat)
                    $('#krytr').html(res.nama_karyawan)
                    $('#krrtr').html(res.nama_kurir)
                    $('#ststr').html(statustr)
                    $.each(res.detail, function(i,data){
                        subtotal = subtotal + data.total_harga
                        htmlDetail += `<li> `+data.merk+` `+data.isi_galon+`L (Rp. `+data.harga+`) x`+data.jumlah+` <a class="float-right">Rp. `+data.total_harga+`</a> </li>`
                    })
                    $('#dataBeli').html(htmlDetail)
                    $('#subtotal').html('Rp. '+subtotal)
                    $('#diskon').html(diskon)
                    $('#ongkir').html('+ Rp. '+res.ongkir)
                    $('#total').html('Rp. '+res.total)
                }
            })
        }
    </script>
    @endsection