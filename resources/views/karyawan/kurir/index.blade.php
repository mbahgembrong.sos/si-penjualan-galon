@extends('layouts.karyawanApp', [$title = 'Data Kurir'])

@section('content')
<!-- Content Wrapper. Contains page content -->
<section class="content">
    <div class="">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Kurir</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Users</a></li>
                                <li class="breadcrumb-item active">Kurir</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <div class="content">
                <div class="container-fluid">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h5 class="m-0 font-weight-bold text-primary">Data Kurir
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive text-nowrap">
                                <table class="table table-striped dataTable" id="dataTable" width="100%"
                                    cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="6%" class="text-center">#</th>
                                            <th>Username</th>
                                            <th>Nama</th>
                                            <th>TTL</th>
                                            <th>JK</th>
                                            <th>Alamat</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Username</th>
                                            <th>Nama</th>
                                            <th>TTL</th>
                                            <th>JK</th>
                                            <th>Alamat</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- /.control-sidebar -->

    @endsection

    @section('footer')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        getKurir()

        var Notif = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
        })

        function getKurir() {
            var htmlview ,no = 0
            $.ajax({
                url: "{{ route('karyawan.kurir.data') }}",
                type: 'GET',
                success: function (res) {
                    $('tbody').html('')
                    $.each(res, function(i, data){
                            no = no+1
                            htmlview += `
                                <tr>
                                    <td class="text-center">`+no+`</td>
                                    <td>`+data.username+`</td>
                                    <td>`+data.nama+`</td>
                                    <td>`+data.tempat_lahir+`, `+data.tanggal_lahir+`</td>
                                    <td>`+data.jenis_kelamin+`</td>
                                    <td>`+data.alamat+`</td>
                                </tr>`
                        });
                    $('tbody').html(htmlview)
                    $('#dataTable').dataTable();
                }
            })
        }
    </script>
    @endsection