@extends('layouts.karyawanApp', [$title = 'Data Galon'])

@section('content')
<!-- Content Wrapper. Contains page content -->
<section class="content">
    <div class="">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Galon</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Galon</a></li>
                                <li class="breadcrumb-item active">Data Galon</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <div class="content">
                <div class="container-fluid">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h5 class="m-0 font-weight-bold text-primary">Data Galon
                                <div class="float-right">
                                </div>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive text-nowrap">
                                <table class="table table-striped dataTable" id="dataTable" width="100%"
                                    cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="6%" class="text-center">#</th>
                                            <th>Gambar</th>
                                            <th>Merk</th>
                                            <th>Isi</th>
                                            <th>Stok</th>
                                            <th class="text-right">Harga Awal</th>
                                            <th class="text-right">Harga Jual</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Gambar</th>
                                            <th>Merk</th>
                                            <th>Isi</th>
                                            <th>Stok</th>
                                            <th class="text-right">Harga Awal</th>
                                            <th class="text-right">Harga Jual</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- /.control-sidebar -->
    @endsection

    @section('footer')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        getGalon()

        var Notif = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
        })

        function getGalon() {
            var htmlview ,no = 0
            $.ajax({
                url: "{{ route('karyawan.galon.data') }}",
                type: 'GET',
                success: function (res) {
                    $('tbody').html('')
                    $.each(res, function(i, data){
                            no = no+1
                            var img_link = "{{ url('/img_galon/:gambar_galon') }}"
                            img_link = img_link.replace(":gambar_galon",data.gambar_galon)

                            htmlview += `
                                <tr>
                                    <td class="text-center">`+no+`</td>
                                    <td> <a href="`+img_link+`" data-toggle="lightbox"><img src="`+img_link+`" class="img-bordered-sm"  width="50px"></a>
                                    <td>`+data.merk+`</td>
                                    <td>`+data.isi_galon+` L</td>
                                    <td>`+data.jml_stok+`</td>
                                    <td class="text-right">Rp. `+data.harga_awal+`</td>
                                    <td class="text-right">Rp. `+data.harga_jual+`</td>
                                </tr>`
                        });
                    $('tbody').html(htmlview)
                    $('#dataTable').dataTable();
                }
            })
        }
    </script>
    @endsection