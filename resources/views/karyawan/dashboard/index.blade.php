@extends('layouts.karyawanApp', [$title = 'Dashboard'])

@section('content')
<section class="content">
    <div class="">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Dashboard</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card border border-success">
                                <div class="card-body row container-fluid text-center">
                                    <div class="col-5">
                                        <button class="btn btn-success float-left" style="font-size: 32px;">
                                            <i class="fas fa-shopping-cart p-1"></i>
                                        </button>
                                    </div>
                                    <div class="col-7 text-right" style="font-size: 38px;">
                                        <span class="align-middle">{{$trjml}}</span>
                                    </div>
                                    <div class="col-12 py-2 mt-2 border-top border-success">
                                        <span class="text-muted text-center align-middle">Penjualan Bulan Ini</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card border border-info">
                                <div class="card-body row container-fluid text-center">
                                    <div class="col-5">
                                        <button class="btn btn-info float-left" style="font-size: 32px;">
                                            <i class="fas fa-tint p-2"></i>
                                        </button>
                                    </div>
                                    <div class="col-7 text-right" style="font-size: 38px;">
                                        <span class="align-middle">{{$glnterjual}}</span>
                                    </div>
                                    <div class="col-12 py-2 mt-2 border-top border-info">
                                        <span class="text-muted text-center align-middle">Galon Terjual Bulan Ini</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="card border border-warning">
                                <div class="card-body row container-fluid text-center">
                                    <div class="col-5">
                                        <button class="btn btn-warning float-left" style="font-size: 32px;">
                                            <i class="fas fa-cubes p-1"></i>
                                        </button>
                                    </div>
                                    <div class="col-7 text-right" style="font-size: 38px;">
                                        <span class="align-middle">{{$stokgln}}</span>
                                    </div>
                                    <div class="col-12 py-2 mt-2 border-top border-warning">
                                        <span class="text-muted text-center align-middle">Sisa Stok Galon</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card border border-primary">
                                <div class="card-body row container-fluid text-center">
                                    <div class="col-5">
                                        <button class="btn btn-primary float-left" style="font-size: 32px;">
                                            <i class="fas fa-shopping-basket"></i>
                                        </button>
                                    </div>
                                    <div class="col-7 text-right" style="font-size: 38px;">
                                        <span class="align-middle">{{$stokdibeli}}</span>
                                    </div>
                                    <div class="col-12 py-2 mt-2 border-top border-primary">
                                        <span class="text-muted text-center align-middle">Pembelian Stok Bulan
                                            Ini</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body container-fluid">
                            {!! $chart->container() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <script src="{{ $chart->cdn() }}"></script>

    {{ $chart->script() }}
    @endsection