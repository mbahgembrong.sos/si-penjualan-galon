@extends('layouts.karyawanApp', [$title = 'My Profile'])

@section('content')
<section class="content">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">My Profile</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">

                        <!-- Profile Image -->
                        <div class="card card-success card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <h1>
                                        <i class="fas fa-user-circle fa-lg"></i>
                                    </h1>
                                </div>

                                <h3 class="profile-username text-center">{{ Auth::user()->username }}</h3>

                                <p class="text-muted text-center">
                                    @if (Auth::user()->role == 1)
                                    Admin
                                    @elseif (Auth::user()->role == 2)
                                    Karyawan
                                    @elseif (Auth::user()->role == 3)
                                    Jukir
                                    @endif
                                </p>

                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Nama</b> <a class="float-right">{{ $users['nama'] }}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>TTL</b>
                                        <a class="float-right">
                                            {{ $users['tempat_lahir'] }}, {{ $users['tanggal_lahir'] }}
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Jenis Kelamin</b> <a class="float-right">
                                            @if ( $users['jenis_kelamin'] == 'L' )
                                            Laki-laki
                                            @else
                                            Perempuan
                                            @endif
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Alamat</b>
                                        <p class="text-left">
                                            {{ $users['alamat'] }}
                                        </p>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-9">
                        <div class="card card-success card-outline">
                            <div class="card-header">
                                <h5> <i class="fas fa-edit"></i> Edit Profile</h5>
                            </div><!-- /.card-header -->
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="active tab-pane" id="settings">
                                        <form class="form-horizontal text-justify" method="post"
                                            action="{{ route('karyawan.profile.update', $users['id_user']) }}"
                                            enctype="multipart/form-data" autocomplete="off">
                                            @csrf
                                            <div class="form-group row">
                                                <label for="inputName2" class="col-sm-3 col-form-label">Nama</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="nama" class="form-control
                                                     @if($errors->first('nama') != null) is-invalid @endif"
                                                        id="inputName2" placeholder="Nama" value="{{ $users['nama'] }}">

                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('nama') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputTanggal" class="col-md-3 col-form-label">TTL</label>
                                                <div class="col-md-9 row">
                                                    <div class="col-md-6 mt-1">
                                                        <input type="text" name="tempat_lahir"
                                                            class="form-control
                                                            @if($errors->first('tempat_lahir') != null) is-invalid @endif" placeholder="Tempat Lahir"
                                                            value="{{ $users['tempat_lahir'] }}">

                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('tempat_lahir') }}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mt-1">
                                                        <input type="date" name="tanggal_lahir"
                                                            class="form-control
                                                            @if($errors->first('tanggal_lahir') != null) is-invalid @endif"
                                                            value="{{ $users['tanggal_lahir'] }}">

                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('tanggal_lahir') }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputJk" class="col-md-3 col-form-label">Jenis
                                                    Kelamin</label>
                                                <div class="col-md-9">
                                                    <select name="jenis_kelamin" class="form-control" id="inputJk">
                                                        <option value="" disabled="">--- Pilih Jenis Kelamin ---
                                                        </option>
                                                        <option value="L" @if($users['jenis_kelamin']=='L' ) Selected
                                                            @endif>Laki-laki</option>
                                                        <option value="P" @if($users['jenis_kelamin']=='P' ) Selected
                                                            @endif>Perempuan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputExperience"
                                                    class="col-md-3 col-form-label">Alamat</label>
                                                <div class="col-md-9 row">
                                                    <div class="col-md-12 mt-1">
                                                        <textarea placeholder="Alamat" name="alamat"
                                                            class="form-control @if($errors->first('alamat') != null) is-invalid @endif">{{ $users['alamat'] }}</textarea>

                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('alamat') }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="mt-5">
                                            <span class="text-danger text-sm">Biarkan password kosong jika tidak ingin
                                                merubah password!</span>
                                            <div class="form-group row my-3">
                                                <label for="inputpasslama"
                                                    class="col-sm-3 col-form-label">Username</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="username"
                                                        class="form-control @if($errors->first('username') != null) is-invalid @endif"
                                                        id="inputusername" placeholder="Username"
                                                        value="{{ Auth::user()->username }}">

                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('username') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row my-3">
                                                <label for="inputpasslama" class="col-sm-3 col-form-label">Password
                                                    Lama</label>
                                                <div class="col-md-9">
                                                    <input type="password" name="passwordlama" class="form-control"
                                                        id="inputpasslama" placeholder="Password Lama" value="">
                                                </div>
                                            </div>
                                            <div class="form-group row my-3">
                                                <label for="inputpassbaru" class="col-sm-3 col-form-label">Password
                                                    Baru</label>
                                                <div class="col-md-9">
                                                    <input type="password" name="passwordbaru" class="form-control"
                                                        id="inputpassbaru" placeholder="Password Baru" value="">
                                                </div>
                                            </div>
                                            <div class="form-group row text-right">
                                                <div class="offset-sm-3 col-md-9">
                                                    <button type="submit" class="btn btn-warning">Update
                                                        Profile</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                            </div><!-- /.card-body -->
                        </div>
                        <!-- /.nav-tabs-custom -->
                    </div>
                    <!-- /.col -->
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
</section>
@endsection