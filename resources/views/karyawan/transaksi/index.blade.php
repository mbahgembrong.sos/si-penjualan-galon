@extends('layouts.karyawanApp', [$title = 'Data Transaksi'])

@section('content')
<!-- Content Wrapper. Contains page content -->
<section class="content">
    <div class="">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Transaksi</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
                                <li class="breadcrumb-item active">Data Transaksi</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <div class="content">
                <div class="container-fluid">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h5 class="m-0 font-weight-bold text-primary">Data Transaksi
                                <div class="float-right">
                                </div>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive text-nowrap">
                                <table class="table table-striped dataTable" id="dataTable" width="100%"
                                    cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="10%" class="text-center">Aksi</th>
                                            <th>Tgl Transaksi</th>
                                            <th>Pembeli</th>
                                            <th>Alamat Kirim</th>
                                            <th>Karyawan</th>
                                            <th>Kurir</th>
                                            <th class="text-right">Total</th>
                                            <th>Status</th>
                                            <th>Ket</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th width="10%" class="text-center">Aksi</th>
                                            <th>Tgl Transaksi</th>
                                            <th>Pembeli</th>
                                            <th>Alamat Kirim</th>
                                            <th>Karyawan</th>
                                            <th>Kurir</th>
                                            <th class="text-right">Total</th>
                                            <th>Status</th>
                                            <th>Ket</th>
                                        </tr>
                                    </tfoot>
                                    <tbody id="data">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- /.control-sidebar -->

    <div class="modal fade" id="modalDetailTransaksi" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog-scrollable modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content border-top border-info">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Detail Transaksi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-light">
                    <div class="container-fluid">
                        <div class="border-top border-dark border-bottom text-center">
                            <h5 class="my-1"><b> Toko Sido Mulyo </b></h5>
                        </div>
                        <div class="row">
                            <div class="col-12 text-sm">
                                <table>
                                    <tr>
                                        <th class="align-top" width="100px">No. Transaksi </th>
                                        <td class="align-top" id="idtr">...</td>
                                    </tr>
                                    <tr>
                                        <th class="align-top">Tanggal</th>
                                        <td class="align-top" id="tgltr">...</td>
                                    </tr>
                                    <tr>
                                        <th class="align-top">Pembeli</th>
                                        <td class="align-top" id="pembelitr">...</td>
                                    </tr>
                                    <tr>
                                        <th class="align-top">Alamat</th>
                                        <td class="align-top" id="alamattr">...</td>
                                    </tr>
                                    <tr>
                                        <th class="align-top">Petugas</th>
                                        <td class="align-top" id="krytr">...</td>
                                    </tr>
                                    <tr>
                                        <th class="align-top">Kurir</th>
                                        <td class="align-top" id="krrtr">...</td>
                                    </tr>
                                    <tr>
                                        <th class="align-top">Status</th>
                                        <td class="align-top" id="ststr">
                                            ...
                                        </td>
                                    </tr>
                                </table>
                                <div class="border-bottom border-top border-dark">
                                    <div class="mt-2 text-bold">Galon dibeli :</div>
                                    <ol class="pl-4" id="dataBeli">
                                        ...
                                    </ol>
                                </div>
                                <div>
                                    <ul class="pl-0" style="list-style:none;">
                                        <li>
                                            <b>Sub. Total :</b> <a class="float-right" id="subtotal">...</a>
                                        </li>
                                        <li>
                                            <b>Diskon :</b> <a class="float-right" id="diskon">...</a>
                                        </li>
                                        <li>
                                            <b>Ongkir :</b> <a class="float-right" id="ongkir">...</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="border-top border-dark text-lg">
                                    <ul class="pl-0" style="list-style:none;">
                                        <li>
                                            <b>Total :</b> <b class="float-right" id="total">...</b>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalTRKurir" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog-scrollable modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bg-info">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"> Pilih Kurir untuk konfirmasi pembayaran! </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-light">
                    <form enctype="multipart/form-data" autocomplete="off" id="formtrkurir" data-id=""
                        class="needs-validation" novalidate>
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <label for="kurir">Pilih Kurir</label>
                                <div class="col-md-14 row">
                                    <div class="col-md-12">
                                        <select name="kurir_tr" id="kurir" class="form-control">
                                            <option disabled value>- Pilih kurir - </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer container-fluid">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                <button type="button" id="simpanData" class="btn btn-primary">Konfirmasi
                                    Pembayaran</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('footer')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        getTransaksi()

        var Notif = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
        })

        function getTransaksi() {
            var htmlview ,tgl_transaksi, alamat ,d ,bukti,statustr, res = ''
            $.ajax({
                url: "{{ route('karyawan.transaksi.data') }}",
                type: 'GET',
                success: function(response) {
                    res = response;
                },
                complete: function () {
                    console.log(res);
                    $('#data').html('')

                    var karyawan = ''
                    $.each(res, function(i, data){

                        if (data.status == 0) {
                            statustr = `<div class="text-light bg-secondary container">Belum dikonfirmasi</div>
                            <button class="btn btn-sm btn-success py-0"  onclick="konfirmasiPesanan(`+data.id+`)">Konfirmasi</button>
                            <button class="btn btn-sm btn-danger py-0"  onclick="batalkanPesanan(`+data.id+`)">Batal</button>`
                        } else if (data.status == 1) {
                            statustr = `<div class="text-light bg-info container">Belum dibayar</div>
                            <button class="btn btn-sm btn-success py-0"  onclick="konfirmasiPembayaran(`+data.id+`)">Konfirmasi</button>
                            <button class="btn btn-sm btn-warning py-0"  onclick="gagalkanPembayaran(`+data.id+`)">Gagal</button>
                            <button class="btn btn-sm btn-danger py-0"  onclick="batalkanPesanan(`+data.id+`)">Batal</button>`
                        } else if (data.status == 2) {
                            statustr = `<div class="text-light bg-warning container">Pembayaran Gagal</div>
                            <button class="btn btn-sm btn-success py-0"  onclick="konfirmasiPembayaran(`+data.id+`)">Konfirmasi</button>
                            <button class="btn btn-sm btn-warning py-0"  onclick="gagalkanPembayaran(`+data.id+`)">Gagal</button>
                            <button class="btn btn-sm btn-danger py-0"  onclick="batalkanPesanan(`+data.id+`)">Batal</button>`
                        } else if (data.status == 3) {
                            statustr = `<div class="text-light bg-primary container">Proses pengiriman</div>`
                        } else if (data.status == 4) {
                            statustr = `<div class="text-light bg-success container">Selesai</div>`
                        } else if (data.status == 5) {
                            statustr = `<div class="text-light bg-danger container">Dibatalkan</div>`
                        } 

                        alamat = "("+data.desa+") <br> "+data.alamat

                        
                        if (data.bukti_transaksi == null) {
                            bukti = `<button id="detail" class="btn btn-sm btn-outline-danger disabled">
                                        <i class="fas fa-file-invoice"></i>
                                        </button>`
                        } else {
                            var img_link = "{{ url('/bukti_transaksi/:buktitr') }}"
                            img_link = img_link.replace(":buktitr",data.bukti_transaksi)

                        bukti = `
                        <a href="`+img_link+`" data-toggle="lightbox">
                        <button id="detail" class="btn btn-sm btn-outline-success">
                                        <i class="fas fa-file-invoice"></i>
                                </button> </a>`
                        }


                        htmlview += `
                                <tr>
                                    <td> 
                                        <button id="detail" class="btn btn-sm btn-outline-info" onClick="detailTransaksi(`+data.id+`)">
                                            <i class="fas fa-eye"></i>
                                        </button>
                                        `+bukti+`
                                    </td>
                                    <td>`+data.created_at+`</td>
                                    <td>`+data.nama_pelanggan+`</td>
                                    <td>`+data.alamat+`</td>
                                    <td>`+data.nama_karyawan+`</td>
                                    <td>`+data.nama_kurir+`</td>
                                    <td class="text-right text-success"><b> Rp. `+data.total+`</b></td>
                                    <td class="text-center">`+statustr+`</td>
                                    <td>`+data.keterangan+`</td>
                                </tr>`
                        });
                    $('#data').html(htmlview)
                    $('#dataTable').dataTable();
                }
            })
        }

        function detailTransaksi(id){
            var _url = "{{ route('karyawan.transaksi.getDetailTransaksi',":id") }}"
            _url = _url.replace(':id',id)
            $.ajax({
                url: _url,
                type: 'GET',
                success: function (res) {
                    $('#modalDetailTransaksi').modal('show')

                    console.log(res);

                    var statustr = res.status
                    var htmlDetail = '';
                    var subtotal = 0;
                    var diskon = 0;

                    if (statustr == 0) {
                        statustr = `<i class="text-light bg-secondary px-2">Belum dikonfirmasi</i>`
                    } else if (statustr == 1) {
                        statustr = `<i class="text-light bg-info px-2">Belum dibayar</i>`
                    } else if (statustr == 2) {
                        statustr = `<i class="text-light bg-warning px-2">Pembayaran Gagal</i>`
                    } else if (statustr == 3) {
                        statustr = `<i class="text-light bg-primary px-2">Proses pengiriman</i>`
                    } else if (statustr == 4) {
                        statustr = `<i class="text-light bg-success px-2">Selesai</i>`
                    } else if (statustr == 5) {
                        statustr = `<i class="text-light bg-danger px-2">Dibatalkan</i>`
                    } 

                    if (res.jenis_potongan == 'Langsung') {
                        diskon = '- Rp. '+res.diskon
                    } else {
                        diskon = '- '+res.diskon+'%'
                    }

                    $('#idtr').html(res.id)
                    $('#tgltr').html(res.created_at)
                    $('#pembelitr').html(res.nama_pelanggan)
                    $('#alamattr').html(res.alamat)
                    $('#krytr').html(res.nama_karyawan)
                    $('#krrtr').html(res.nama_kurir)
                    $('#ststr').html(statustr)
                    $.each(res.detail, function(i,data){
                        subtotal = subtotal + data.total_harga
                        htmlDetail += `<li> `+data.merk+` `+data.isi_galon+`L (Rp. `+data.harga+`) x`+data.jumlah+` <a class="float-right">Rp. `+data.total_harga+`</a> </li>`
                    })
                    $('#dataBeli').html(htmlDetail)
                    $('#subtotal').html('Rp. '+subtotal)
                    $('#diskon').html(diskon)
                    $('#ongkir').html('+ Rp. '+res.ongkir)
                    $('#total').html('Rp. '+res.total)
                }
            })
        }

        function konfirmasiPesanan(id) {
            Swal.fire({
                title: "Apakah anda yakin untuk konfirmasi pesanan transaksi ini?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
            })
            .then((result) => {
                if (result.isConfirmed) {
                    var _url = "{{ route('karyawan.transaksi.konfirmasiPesanan',":id") }}"
                    _url = _url.replace(':id',id)
                    $.ajax({
                        url : _url,
                        type : 'GET',
                        success : function(res){
                            Notif.fire({
                                    icon : 'success',
                                    title : res.message,
                                })
                            getTransaksi()
                        } ,
                        error : function (err) {
                            console.log(err);
                            Notif.fire({
                                icon : 'error',
                                title : 'Gagal konfirmasi pesanan',
                            });
                        }
                    })
                }
            });
        }

        function batalkanPesanan(id) {
            (async () => {
            const { value: ket } = await Swal.fire({
            title: 'Masukkan Keterangan Pesanan Dibatalkan!',
            input: 'textarea',
            inputLabel: 'Keterangan Pesanan Dibatalkan',
            inputPlaceholder: 'Ketik disini...!',
            inputAttributes: {
                'aria-label': 'Ketik disini...!'
            },
            confirmButtonText: "Konfirmasi Pesanan Dibatalkan!",
            cancelButtonText: "Batal",
            inputValidator: (value) => {
                return new Promise((resolve) => {
                    if (value == '') {
                        resolve('Keterangan tidak boleh kosong!')
                    } else {
                        resolve()
                    }
                })
            } ,
            showCancelButton: true
            })

            if (ket) {
                var _url = "{{ route('karyawan.transaksi.batalkanPesanan', [':id', ':ket']) }}"
                _url = _url.replace(':id',id)
                _url = _url.replace(':ket',`${ket}`)
                $.ajax({
                    url : _url,
                    type : 'GET',
                    success : function(res){
                    Notif.fire({
                        icon : 'success',
                        title : res.message,
                    })
                    getTransaksi()
                    } ,
                    error : function (err) {
                        console.log(err);
                        Notif.fire({
                                icon : 'error',
                                title : 'Gagal membatalkan pesanan',
                        });
                    }
                })
            }
            })()
        }

        function gagalkanPembayaran(id) {
            (async () => {
            const { value: ket } = await Swal.fire({
            title: 'Masukkan Keterangan Pembayaran Gagal!',
            input: 'textarea',
            inputLabel: 'Keterangan Pembayaran Gagal',
            inputPlaceholder: 'Ketik disini...!',
            inputAttributes: {
                'aria-label': 'Ketik disini...!'
            },
            confirmButtonText: "Konfirmasi Pembayaran Gagal!",
            cancelButtonText: "Batal",
            inputValidator: (value) => {
                return new Promise((resolve) => {
                    if (value == '') {
                        resolve('Keterangan tidak boleh kosong!')
                    } else {
                        resolve()
                    }
                })
            } ,
            showCancelButton: true
            })

            if (ket) {
                var _url = "{{ route('karyawan.transaksi.gagalkanPembayaran', [':id', ':ket']) }}"
                _url = _url.replace(':id',id)
                _url = _url.replace(':ket',`${ket}`)
                $.ajax({
                    url : _url,
                    type : 'GET',
                    success : function(res){
                    Notif.fire({
                        icon : 'success',
                        title : res.message,
                    })
                    getTransaksi()
                    } ,
                    error : function (err) {
                        console.log(err);
                        Notif.fire({
                                icon : 'error',
                                title : 'Gagal menggagalkan pesanan',
                        });
                    }
                })
            }
            })()
        }

        function konfirmasiPembayaran(id) {
            $('[name=kurir_tr]').html('');
            $.ajax({
                url: "{{ route('karyawan.transaksi.dataKurir') }}",
                type: 'GET',
                success: function (res) {
                    $('#modalTRKurir').modal('show')
                    $('#formtrkurir').attr("data-id",id)
                    $.each(res, function (i, data) {
                        $('[name=kurir_tr]').append('<option value='+data.id+'>'+data.nama+'</option>');
                    })
                }
            })
        }

        function updatePembayaran() {
            var id = $('#formtrkurir').data('id')
            var _url = "{{ route('karyawan.transaksi.konfirmasiPembayaran', ":id") }}"
            _url = _url.replace(':id',id)

            $.ajax({
                url : _url,
                type : 'POST',
                data : $('#formtrkurir').serialize() + "&id=" +id,
                dataType : 'json',
                success : function (res) {
                    if (res.code == 200){
                        $('#formtrkurir').trigger('reset')
                        $('#modaltrkurir').modal('hide')
                        
                        Notif.fire({
                            icon : 'success',
                            title : res.message,
                        })
                        
                        getTransaksi()
                    }
                },
                error : function (err) {
                    console.log(err);
                    Notif.fire({
                        icon : 'error',
                        title : 'Gagal Konfirmasi Pembayaran',
                    });
                    
                    $.each(err.responseJSON.errors, function (i, error) {
                        var el = $('#formtrkurir').find('[name="'+i+'"]');
                        el.addClass('is-invalid');
                        el.after('<div class="invalid-feedback">'+error[0]+'</div>');
                    });
                }
            })
        }

        $('#simpanData').on('click', function (e){
            e.preventDefault()
            $('.is-invalid').removeClass('is-invalid');
            $('.invalid-feedback').remove();
            updatePembayaran()
        })
    </script>
    @endsection