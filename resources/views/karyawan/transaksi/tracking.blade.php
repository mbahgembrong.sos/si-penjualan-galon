@extends('layouts.karyawanApp', [$title = 'Data Transaksi'])

@section('content')
<!-- Content Wrapper. Contains page content -->
<section class="content">
    <div class="">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Transaksi</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
                                <li class="breadcrumb-item active">Data Transaksi</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <div class="content">
                <div class="container-fluid">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h5 class="m-0 font-weight-bold text-primary">Tracking Transaksi
                                <div class="float-right">
                                </div>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="container text-center">
                                <h2>Masukkan Nomor Transaksi</h2>
                                <input type="text" name="no_transaksi" id="no_transaksi"
                                    class="form-control-lg form-control mt-2" placeholder="No. Transaksi">

                                <button class="btn btn-success btn-lg mt-3" onclick="detailTransaksi()"> <i
                                        class="fas fa-search"></i>
                                    Tracking</button>
                                <button class="btn btn-danger btn-lg mt-3"> <i class="fas fa-times"></i>
                                    Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- /.control-sidebar -->

    <div class="modal fade" id="modalDetailTransaksi" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog-scrollable modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content border-top border-info">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tracking Transaksi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-light">
                    <div class="container-fluid">
                        <h1 class="text-center">No Transaksi : <span id="idtr" class="text-success"></span></h1>
                        <table class="container-fluid border-top border-dark">
                            <tr>
                                <td style="vertical-align: top;">
                                    <table class="container-fluid mt-2">
                                        <tr>
                                            <th class="align-top">Tanggal</th>
                                            <td class="align-top" id="tgltr">...</td>
                                        </tr>
                                        <tr>
                                            <th class="align-top">Pembeli</th>
                                            <td class="align-top" id="pembelitr">...</td>
                                        </tr>
                                        <tr>
                                            <th class="align-top">Alamat</th>
                                            <td class="align-top" id="alamattr">...</td>
                                        </tr>
                                        <tr>
                                            <th class="align-top">Petugas</th>
                                            <td class="align-top" id="krytr">...</td>
                                        </tr>
                                        <tr>
                                            <th class="align-top">Kurir</th>
                                            <td class="align-top" id="krrtr">...</td>
                                        </tr>
                                        <tr>
                                            <th class="align-top">Ket</th>
                                            <td class="align-top" id="keterangan">...</td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table class="container-fluid">
                                        <tr>
                                            <td>
                                                <div class="border-bottom border-dark">
                                                    <div class="mt-2 text-bold">Galon dibeli :</div>
                                                    <ol class="pl-4" id="dataBeli">
                                                        ...
                                                    </ol>
                                                </div>
                                                <div>
                                                    <ul class="pl-0" style="list-style:none;">
                                                        <li>
                                                            <b>Sub. Total :</b> <a class="float-right"
                                                                id="subtotal">...</a>
                                                        </li>
                                                        <li>
                                                            <b>Diskon :</b> <a class="float-right" id="diskon">...</a>
                                                        </li>
                                                        <li>
                                                            <b>Ongkir :</b> <a class="float-right" id="ongkir">...</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="border-top border-dark text-lg">
                                                    <ul class="pl-0" style="list-style:none;">
                                                        <li>
                                                            <b>Total :</b> <b class="float-right" id="total">...</b>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <div class="container-fluid text-center border-top border-dark bg-light pt-3 pb-5">
                            <h1>Status Transaksi</h1>
                            <div id="ststr" class="progress">

                            </div>
                            <div class="row" id="sts" class="font-weight-bold">
                                <div class="col-3" id="sts1" class="text-muted">Belum Dikonfirmasi</div>
                                <div class="col-3" id="sts2" class="text-muted">Belum Dibayar</div>
                                <div class="col-3" id="sts3" class="text-muted">Proses Pengiriman</div>
                                <div class="col-3" id="sts4" class="text-muted">Selesai</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('footer')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })

        function detailTransaksi(){
            var id = $('[name=no_transaksi]').val();
            var _url = "{{ route('karyawan.transaksi.getDetailTransaksi',":id") }}"
            _url = _url.replace(':id',id)
            $.ajax({
                url: _url,
                type: 'GET',
                success: function (res) {
                    $('#modalDetailTransaksi').modal('show')

                    var statustr = res.status
                    var htmlDetail = '';
                    var subtotal = 0;
                    var diskon = 0;

                    if (statustr == 0) {
                        statustr = `
                        <div class="progress-bar bg-secondary progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        `
                        $('#sts').html(`
                            <div class="col-3 text-secondary" id="sts1">Sudah Dikonfirmasi</div>
                            <div class="col-3 text-muted" id="sts2">Belum Dibayar</div>
                            <div class="col-3 text-muted" id="sts3">Proses Pengiriman</div>
                            <div class="col-3 text-muted" id="sts4">Selesai</div>
                        `)
                    } else if (statustr == 1) {
                        statustr = `
                        <div class="progress-bar bg-secondary progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        <div class="progress-bar bg-info progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        `
                        $('#sts').html(`
                            <div class="col-3 text-secondary" id="sts1">Sudah Dikonfirmasi</div>
                            <div class="col-3 text-info" id="sts2">Belum Dibayar</div>
                            <div class="col-3 text-muted" id="sts3">Proses Pengiriman</div>
                            <div class="col-3 text-muted" id="sts4">Selesai</div>
                        `)
                    } else if (statustr == 2) {
                        statustr = `
                        <div class="progress-bar bg-secondary progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        <div class="progress-bar bg-warning progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        `
                        $('#sts').html(`
                            <div class="col-3 text-secondary" id="sts1">Sudah Dikonfirmasi</div>
                            <div class="col-3 text-warning" id="sts2">Pembayaran Gagal</div>
                            <div class="col-3 text-muted" id="sts3">Proses Pengiriman</div>
                            <div class="col-3 text-muted" id="sts4">Selesai</div>
                        `)
                    } else if (statustr == 3) {
                        statustr = `
                        <div class="progress-bar bg-secondary progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        <div class="progress-bar bg-info progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        <div class="progress-bar bg-primary progress-bar-striped" role="progressbar" style="width: 25%;" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        `
                        $('#sts').html(`
                            <div class="col-3 text-secondary" id="sts1">Sudah Dikonfirmasi</div>
                            <div class="col-3 text-info" id="sts2">Sudah Dibayar</div>
                            <div class="col-3 text-primary" id="sts3">Proses Pengiriman</div>
                            <div class="col-3 text-muted" id="sts4">Selesai</div>
                        `)
                    } else if (statustr == 4) {
                        statustr = `
                        <div class="progress-bar bg-secondary progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        <div class="progress-bar bg-info progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        <div class="progress-bar bg-primary progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        <div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                        `
                        $('#sts').html(`
                            <div class="col-3 text-secondary" id="sts1">Sudah Dikonfirmasi</div>
                            <div class="col-3 text-info" id="sts2">Sudah Dibayar</div>
                            <div class="col-3 text-primary" id="sts3">Proses Pengiriman</div>
                            <div class="col-3 text-success" id="sts4">Selesai</div>
                        `)
                    } else if (statustr == 5) {
                        statustr = `
                        <div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                        `
                        $('#sts').html(`
                            <div class="col-12 text-danger" id="sts1">Pesanan Dibatalkan</div>
                        `)
                    } 

                    if (res.jenis_potongan == 'Langsung') {
                        diskon = '- Rp. '+res.diskon
                    } else {
                        diskon = '- '+res.diskon+'%'
                    }

                    $('#idtr').html(res.id)
                    $('#tgltr').html(res.created_at)
                    $('#pembelitr').html(res.nama_pelanggan)
                    $('#alamattr').html(res.alamat)
                    $('#krytr').html(res.nama_karyawan)
                    $('#krrtr').html(res.nama_kurir)
                    $('#ststr').html(statustr)
                    $.each(res.detail, function(i,data){
                        subtotal = subtotal + data.total_harga
                        htmlDetail += `<li> `+data.merk+` `+data.isi_galon+`L (Rp. `+data.harga+`) x`+data.jumlah+` <a class="float-right">Rp. `+data.total_harga+`</a> </li>`
                    })
                    $('#dataBeli').html(htmlDetail)
                    $('#subtotal').html('Rp. '+subtotal)
                    $('#diskon').html(diskon)
                    $('#ongkir').html('+ Rp. '+res.ongkir)
                    $('#total').html('Rp. '+res.total)
                    $('#keterangan').html(res.keterangan)
                }
            })
        }

        
    </script>
    @endsection