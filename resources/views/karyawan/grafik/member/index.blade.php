@extends('layouts.karyawanApp', [$title = 'Grafik Member & Non Member'])

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Grafik Pelanggan</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item"><a href="#">Grafik</a></li>
                                <li class="breadcrumb-item active">Pelanggan</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <div class="content">
                <div class="container">
                    <div class="card">
                        <div class="card-body">
                        <form action="{{ route('karyawan.searchGrafikMember') }}" method="POST" enctype="multipart/form-data" id="form-data">
                            @csrf
                            <div class="card">
                                <div class="card-header bg-primary">
                                    <h4 class="card-title text-white float-left">Cari Penghasilan</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="metode">Pilih Metode Pencarian</label>
                                                <div class="input-group">
                                                    <select class="form-control" id="metode" name="metode">
                                                        <option value="" selected disabled>Pilih Metode Pencarian</option>
                                                        <option value="bulan">Bulan</option>
                                                        <option value="tahun">Tahun</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Bulan -->
                                    <div class="row" id="bulan_input" style="display: none;">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="month">Pilih Bulan : </label>
                                                <input type="month" name="bulan" class="form-control"  autocomplete="off" placeholder="Pilih Bulan">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="tahun_input" style="display: none;">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="year">Pilih Tahun : </label>
                                                <input type="text" id="yearM" name="tahun" class="form-control" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <a href="{{ route('karyawan.grafikMember') }}" class="btn btn-primary float-right ml-2" id="reset">Reset Data Bulan Ini</a>
                                    <button type="submit" class="btn btn-success float-right" id="cariPenghasilan">Cari Penghasilan</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            {!! $chart->container() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</section>

@stop

@section('footer')

<script type="text/javascript">
    $(document).ready(function(){
        var form = document.getElementById('form-data');
        var bulan,tahun;

        bulan = document.createAttribute('action');
        bulan.value = "";

        tahun = document.createAttribute('action');
        tahun.value = "";
        $('#metode').click(function(){
            var value = $(this).val();
            if(value == "bulan"){
                $('#minggu_input').each(function(){
                    form.removeAttribute(minggu);
                    $(this).hide();
                });
                $('#bulan_input').each(function(){
                    form.setAttributeNode(bulan);
                    $(this).show();
                });
                $('#tahun_input').each(function(){
                    form.removeAttribute(tahun);
                    $(this).hide();
                });
            }else if(value == "tahun"){
                $('#bulan_input').each(function(){
                    form.removeAttribute(bulan);
                    $(this).hide();
                });
                $('#tahun_input').each(function(){
                    form.setAttributeNode(tahun);
                    $(this).show();
                });
            }
        });
    });
</script>

<script src="{{ $chart->cdn() }}"></script>

{{ $chart->script() }}
@stop