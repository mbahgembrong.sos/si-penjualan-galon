@extends('layouts.karyawanApp', [($title = 'Data Desa')])

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <section class="content">
        <div class="">
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark">Desa</h1>
                            </div><!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="#">Lainnya</a></li>
                                    <li class="breadcrumb-item active">Data Desa</li>
                                </ol>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <div class="content">
                    <div class="container-fluid">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h5 class="m-0 font-weight-bold text-primary">Data Desa

                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive text-nowrap">
                                    <table class="table table-striped dataTable" id="dataTable" width="100%"
                                        cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th width="6%" class="text-center">#</th>
                                                <th>Desa</th>
                                                <th>Ongkir</th>
                                                <th>Kode Pos</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th width="6%" class="text-center">#</th>
                                                <th>Desa</th>
                                                <th>Ongkir</th>
                                                <th>Kode Pos</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- /.control-sidebar -->
    @endsection

    @section('footer')
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            getDesa()

            var Notif = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true,
            })

            function getDesa() {
                var htmlview, no = 0
                $.ajax({
                    url: "{{ route('karyawan.desa.data') }}",
                    type: 'GET',
                    success: function(res) {
                        $('tbody').html('')
                        $.each(res, function(i, data) {
                            no = no + 1

                            htmlview += `
                                <tr>
                                    <td class="text-center">` + no + `</td>
                                    <td>` + data.nama_desa + `</td>
                                    <td>Rp. ` + data.ongkir + `</td>
                                    <td>` + data.kode_pos + `</td>
                                </tr>`
                        });
                        $('tbody').html(htmlview)
                        $('#dataTable').dataTable();
                    }
                })
            }
        </script>
    @endsection
