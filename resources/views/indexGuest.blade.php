@extends('layouts.guestApp')

@section('content')
<div class="container-fluid" style="min-height: 600px; background-color:rgba(0, 0, 0, 0.3);">

    <div style="padding-top:16%;">
        <div class="p-3 text-center">
            <img src="{{asset('icon.png')}}" alt="" srcset=""
                class="rounded-circle img-bordered border-info mb-3 bg-white" width="150px">

            <h2 class="text-light">
                TOKO AIR GALON SIDO MULYO
            </h2>
        </div>
    </div>

</div>
<div class="container-fluid bg-white" id="produk">
    <div class="container pb-5">
        <h1 class="mb-5 pt-5 pb-3 text-center text-info border-bottom border-info"> Daftar
            Produk Galon
        </h1>
        <div class="row">
            @foreach ($data as $galon)
            <div class="col-md-3">
                <div class="card border-info border">
                    <div class="card-body">
                        <h4 class="text-center">{{$galon->merk}} {{ $galon->isi_galon }}L</h4>
                        <p class="card-text container">
                        <div class="row bg-info">
                            <div class="col-3">Harga</div>
                            <div class="col-9 text-right">Rp. {{$galon->harga_jual}}</div>
                        </div>
                        </p>
                    </div>
                    <img src="{{ url('img_galon/'.$galon->gambar_galon) }}" class="card-img-top border-info" alt=""
                        width="100%" height="250px">
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<div class="container-fluid bg-secondary p-3" id="about" style="min-height: 100px;">
    <div class="container">
        <h1>Tentang Kami</h1>
        <div>
            <p class="text-justify">
                Toko Air Galon Sido Mulyo adalah toko yang menjual air galon dengan harga terjangkau dengan kualitas
                terbaik langsung dari produsen, sehingga terjamin kualitasnya. Terdapat karyawan dan kurir yang siap
                melayani pesanan anda melalui pemesanan online maupun offline memudahkan pelanggan untuk melakukan
                pemesanan. Kami telah bekerja sama langsung dengan produsen air minum di Indonesia, berletak di
                Kabupaten Kediri, melayani pesanan di daerah Karisidenan kediri.
            </p>
        </div>
    </div>
</div>

@endsection