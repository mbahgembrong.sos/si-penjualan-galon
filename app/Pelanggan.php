<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $fillable = [
        'nama', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'alamat', 'member', 'id_desa', 'id_user'
    ];
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }
    public function desa()
    {
        return $this->belongsTo('App\Desa', 'id_desa');
    }
    public function transaksi()
    {
        return $this->hasMany('App\Transaksi', 'id_pelanggan', 'id');
    }
}
