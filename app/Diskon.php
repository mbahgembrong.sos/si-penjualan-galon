<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diskon extends Model
{
    protected $fillable = [
        'nama_diskon', 'jenis_potongan', 'diskon'
    ];
}
