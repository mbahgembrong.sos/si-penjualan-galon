<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{
    protected $fillable = [
        'id_galon', 'harga_satuan', 'jumlah_beli', 'total_harga_beli'
    ];
}
