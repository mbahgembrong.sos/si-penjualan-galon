<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galon extends Model
{
    protected $fillable = [
        'merk', 'isi_galon', 'jml_stok', 'harga_awal', 'harga_jual', 'gambar_galon'
    ];
    public function detail_transaksi()
    {
        return $this->hasMany('App\DetailTransaksi','id_galon','id');
    }
}
