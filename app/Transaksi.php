<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $fillable = [
        'id_pelanggan', 'id_karyawan', 'id_jukir', 'id_diskon', 'total', 'status', 'alamat', 'lang', 'long', 'id_desa'
    ];

    public function detail_transaksi()
    {
        return $this->hasMany('App\DetailTransaksi', 'id_transaksi', 'id');
    }
    public function pelanggan()
    {
        return $this->belongsTo('App\Pelanggan', 'id_pelanggan', 'id');
    }
    public function karyawan()
    {
        return $this->belongsTo('App\Karyawan', 'id_karyawan', 'id');
    }
    public function kurir()
    {
        return $this->belongsTo('App\Kurir', 'id_kurir', 'id');
    }
    public function desa()
    {
        return $this->belongsTo('App\Desa', 'id_desa', 'id');
    }
    public function getCreatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])->format('d/m/Y H:i:s');
    }
}
