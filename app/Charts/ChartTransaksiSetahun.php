<?php

namespace App\Charts;

use App\Transaksi;
use ArielMejiaDev\LarapexCharts\LarapexChart;

class ChartTransaksiSetahun
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    public function build()
    {
        $thn = date('Y');

        $dataNow = array();

        $dataBefore = array();

        for ($i = 1; $i <= 12; $i++) {
            $dataNow[$i] = Transaksi::whereMonth('created_at', $i)
                ->whereYear('created_at', $thn)->count();
        }

        for ($i = 1; $i <= 12; $i++) {
            $dataBefore[$i] = Transaksi::whereMonth('created_at', $i)
                ->whereYear('created_at', ($thn - 1))->count();
        }


        return $this->chart->barChart()
            ->setTitle('Data Transaksi')
            ->setSubtitle("Perbandingan Transaksi Tahun " . ($thn - 1) . " vs $thn")
            ->addData($thn, [$dataNow[1], $dataNow[2], $dataNow[3], $dataNow[4], $dataNow[5], $dataNow[6], $dataNow[7], $dataNow[8], $dataNow[9], $dataNow[10], $dataNow[11], $dataNow[12]])
            ->addData(($thn - 1), [$dataBefore[1], $dataBefore[2], $dataBefore[3], $dataBefore[4], $dataBefore[5], $dataBefore[6], $dataBefore[7], $dataBefore[8], $dataBefore[9], $dataBefore[10], $dataBefore[11], $dataBefore[12]])
            ->setXAxis(['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']);
    }
}
