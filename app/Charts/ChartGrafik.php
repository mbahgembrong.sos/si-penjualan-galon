<?php


namespace App\Charts;

use App\Pelanggan;
use App\Transaksi;
use ArielMejiaDev\LarapexCharts\LarapexChart;
use Illuminate\Support\Facades\DB;

class ChartGrafik
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    // ================================================== GRAFIK PENJUALAN ================================================== //

    public function penjualanBulan($bulan)
    {
        $totDay = intval(date('t', strtotime($bulan)));
        $nameMonth = date('F', strtotime($bulan));

        for ($i = 1; $i <= $totDay; $i++) {
            if ($i === 1) {
                $tg = '01';
            } elseif ($i === 2) {
                $tg = '02';
            } elseif ($i === 3) {
                $tg = '03';
            } elseif ($i === 4) {
                $tg = '04';
            } elseif ($i === 5) {
                $tg = '05';
            } elseif ($i === 6) {
                $tg = '06';
            } elseif ($i === 7) {
                $tg = '07';
            } elseif ($i === 8) {
                $tg = '08';
            } elseif ($i === 9) {
                $tg = '09';
            } else {
                $tg = $i;
            }

            $date = $bulan . '-' . $tg;
            // $dt[$i] = Count(DB::select("
            //     SELECT *
            //     FROM transaksis
            //     WHERE DATE(created_at) = '$date'
            // "));

            $dt[$i] = Transaksi::whereDay('created_at', $tg)
                ->whereMonth('created_at', date('m', strtotime($bulan)))
                ->whereYear('created_at', date('Y', strtotime($bulan)))
                ->where('status', 4)
                ->count();
        }


        $chart = $this->chart->barChart()
            ->setTitle('Data Penjualan')
            ->setSubtitle('Grafik Penjualan Bulan ' . $nameMonth);
        if ($totDay === 28) {
            $chart->addData($nameMonth, [
                $dt[1], $dt[2], $dt[3], $dt[4], $dt[5], $dt[6], $dt[7], $dt[8], $dt[9], $dt[10], $dt[11], $dt[12], $dt[13], $dt[14], $dt[15], $dt[16], $dt[17], $dt[18], $dt[19], $dt[20], $dt[21], $dt[22], $dt[23], $dt[24], $dt[25], $dt[26], $dt[27], $dt[28]
            ])
                ->setXAxis([
                    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28'
                ]);
        } elseif ($totDay === 29) {
            $chart->addData($nameMonth, [
                $dt[1], $dt[2], $dt[3], $dt[4], $dt[5], $dt[6], $dt[7], $dt[8], $dt[9], $dt[10], $dt[11], $dt[12], $dt[13], $dt[14], $dt[15], $dt[16], $dt[17], $dt[18], $dt[19], $dt[20], $dt[21], $dt[22], $dt[23], $dt[24], $dt[25], $dt[26], $dt[27], $dt[28], $dt[29]
            ])
                ->setXAxis([
                    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29'
                ]);
        } elseif ($totDay === 30) {
            $chart->addData($nameMonth, [
                $dt[1], $dt[2], $dt[3], $dt[4], $dt[5], $dt[6], $dt[7], $dt[8], $dt[9], $dt[10], $dt[11], $dt[12], $dt[13], $dt[14], $dt[15], $dt[16], $dt[17], $dt[18], $dt[19], $dt[20], $dt[21], $dt[22], $dt[23], $dt[24], $dt[25], $dt[26], $dt[27], $dt[28], $dt[29], $dt[30]
            ])
                ->setXAxis([
                    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'
                ]);
        } else {
            $chart->addData($nameMonth, [
                $dt[1], $dt[2], $dt[3], $dt[4], $dt[5], $dt[6], $dt[7], $dt[8], $dt[9], $dt[10], $dt[11], $dt[12], $dt[13], $dt[14], $dt[15], $dt[16], $dt[17], $dt[18], $dt[19], $dt[20], $dt[21], $dt[22], $dt[23], $dt[24], $dt[25], $dt[26], $dt[27], $dt[28], $dt[29], $dt[30], $dt[31]
            ])
                ->setXAxis([
                    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'
                ]);
        }

        return $chart;
    }

    public function penjualanTahun($tahun)
    {
        for ($i = 1; $i <= 12; $i++) {
            $dt[$i] = Transaksi::whereMonth('created_at', $i)
                ->whereYear('created_at', $tahun)->where('status', 4)->count();
        }


        return $this->chart->barChart()
            ->setTitle('Data Penjualan')
            ->setSubtitle('Grafik Penjualan Tahun ' . $tahun)
            ->addData(($tahun), [$dt[1], $dt[2], $dt[3], $dt[4], $dt[5], $dt[6], $dt[7], $dt[8], $dt[9], $dt[10], $dt[11], $dt[12]])
            ->setXAxis(['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']);
    }


    // ================================================== GRAFIK PENJUALAN ================================================== //


    // ================================================== GRAFIK PENJUALAN ================================================== //

    public function pendapatanBulan($bulan)
    {
        $totDay = intval(date('t', strtotime($bulan)));
        $nameMonth = date('F', strtotime($bulan));

        for ($i = 1; $i <= $totDay; $i++) {
            if ($i === 1) {
                $tg = '01';
            } elseif ($i === 2) {
                $tg = '02';
            } elseif ($i === 3) {
                $tg = '03';
            } elseif ($i === 4) {
                $tg = '04';
            } elseif ($i === 5) {
                $tg = '05';
            } elseif ($i === 6) {
                $tg = '06';
            } elseif ($i === 7) {
                $tg = '07';
            } elseif ($i === 8) {
                $tg = '08';
            } elseif ($i === 9) {
                $tg = '09';
            } else {
                $tg = $i;
            }

            $date = $bulan . '-' . $tg;
            // $dt[$i] = Count(DB::select("
            //     SELECT *
            //     FROM transaksis
            //     WHERE DATE(created_at) = '$date'
            // "));

            $total = 0;

            $pendapatan = Transaksi::whereDay('created_at', $tg)
                ->whereMonth('created_at', date('m', strtotime($bulan)))
                ->whereYear('created_at', date('Y', strtotime($bulan)))
                ->where('status', 4)
                ->get();

            foreach ($pendapatan as $datas) {
                $total = $total + $datas->total;
            }

            $dt[$i] = $total;
        }


        $chart = $this->chart->barChart()
            ->setTitle('Data Pendapatan')
            ->setSubtitle('Grafik Pendapatan Bulan ' . $nameMonth);
        if ($totDay === 28) {
            $chart->addData($nameMonth, [
                $dt[1], $dt[2], $dt[3], $dt[4], $dt[5], $dt[6], $dt[7], $dt[8], $dt[9], $dt[10], $dt[11], $dt[12], $dt[13], $dt[14], $dt[15], $dt[16], $dt[17], $dt[18], $dt[19], $dt[20], $dt[21], $dt[22], $dt[23], $dt[24], $dt[25], $dt[26], $dt[27], $dt[28]
            ])
                ->setXAxis([
                    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28'
                ]);
        } elseif ($totDay === 29) {
            $chart->addData($nameMonth, [
                $dt[1], $dt[2], $dt[3], $dt[4], $dt[5], $dt[6], $dt[7], $dt[8], $dt[9], $dt[10], $dt[11], $dt[12], $dt[13], $dt[14], $dt[15], $dt[16], $dt[17], $dt[18], $dt[19], $dt[20], $dt[21], $dt[22], $dt[23], $dt[24], $dt[25], $dt[26], $dt[27], $dt[28], $dt[29]
            ])
                ->setXAxis([
                    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29'
                ]);
        } elseif ($totDay === 30) {
            $chart->addData($nameMonth, [
                $dt[1], $dt[2], $dt[3], $dt[4], $dt[5], $dt[6], $dt[7], $dt[8], $dt[9], $dt[10], $dt[11], $dt[12], $dt[13], $dt[14], $dt[15], $dt[16], $dt[17], $dt[18], $dt[19], $dt[20], $dt[21], $dt[22], $dt[23], $dt[24], $dt[25], $dt[26], $dt[27], $dt[28], $dt[29], $dt[30]
            ])
                ->setXAxis([
                    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'
                ]);
        } else {
            $chart->addData($nameMonth, [
                $dt[1], $dt[2], $dt[3], $dt[4], $dt[5], $dt[6], $dt[7], $dt[8], $dt[9], $dt[10], $dt[11], $dt[12], $dt[13], $dt[14], $dt[15], $dt[16], $dt[17], $dt[18], $dt[19], $dt[20], $dt[21], $dt[22], $dt[23], $dt[24], $dt[25], $dt[26], $dt[27], $dt[28], $dt[29], $dt[30], $dt[31]
            ])
                ->setXAxis([
                    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'
                ]);
        }

        return $chart;
    }

    public function pendapatanTahun($tahun)
    {
        for ($i = 1; $i <= 12; $i++) {

            $total = 0;

            $pendapatan = Transaksi::whereMonth('created_at', $i)->where('status', 4)
                ->whereYear('created_at', $tahun)->get();

            foreach ($pendapatan as $datas) {
                $total = $total + $datas->total;
            }

            $dt[$i] = $total;
        }


        return $this->chart->barChart()
            ->setTitle('Data Pendapatan')
            ->setSubtitle('Grafik Pendapatan Tahun ' . $tahun)
            ->addData(($tahun), [$dt[1], $dt[2], $dt[3], $dt[4], $dt[5], $dt[6], $dt[7], $dt[8], $dt[9], $dt[10], $dt[11], $dt[12]])
            ->setXAxis(['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']);
    }


    // ================================================== GRAFIK PENJUALAN ================================================== //

    // ================================================== GRAFIK MEMBER ================================================== //
    public function memberBulan($bulan)
    {
        $totDay = intval(date('t', strtotime($bulan)));
        $nameMonth = date('F', strtotime($bulan));

        for ($i = 1; $i <= $totDay; $i++) {
            if ($i === 1) {
                $tg = '01';
            } elseif ($i === 2) {
                $tg = '02';
            } elseif ($i === 3) {
                $tg = '03';
            } elseif ($i === 4) {
                $tg = '04';
            } elseif ($i === 5) {
                $tg = '05';
            } elseif ($i === 6) {
                $tg = '06';
            } elseif ($i === 7) {
                $tg = '07';
            } elseif ($i === 8) {
                $tg = '08';
            } elseif ($i === 9) {
                $tg = '09';
            } else {
                $tg = $i;
            }

            $date = $bulan . '-' . $tg;
            // $dt[$i] = Count(DB::select("
            //     SELECT *
            //     FROM transaksis
            //     WHERE DATE(created_at) = '$date'
            // "));

            $dtm[$i] = Pelanggan::whereDay('created_at', $tg)
                ->whereMonth('created_at', date('m', strtotime($bulan)))
                ->whereYear('created_at', date('Y', strtotime($bulan)))
                ->where('member', 1)
                ->where('status', 4)
                ->count();
            $dtn[$i] = Pelanggan::whereDay('created_at', $tg)
                ->whereMonth('created_at', date('m', strtotime($bulan)))
                ->whereYear('created_at', date('Y', strtotime($bulan)))
                ->where('member', 0)
                ->where('status', 4)
                ->count();
        }


        $chart = $this->chart->barChart()
            ->setTitle('Data Member')
            ->setSubtitle('Grafik Member Bulan ' . $nameMonth);
        if ($totDay === 28) {
            $chart->addData('Member', [
                $dtm[1], $dtm[2], $dtm[3], $dt[4], $dtm[5], $dtm[6], $dtm[7], $dtm[8], $dtm[9], $dtm[10], $dtm[11], $dtm[12], $dtm[13], $dtm[14], $dtm[15], $dtm[16], $dtm[17], $dtm[18], $dtm[19], $dtm[20], $dtm[21], $dtm[22], $dtm[23], $dtm[24], $dtm[25], $dtm[26], $dtm[27], $dtm[28]
            ])
                ->addData('Non-Member', [
                    $dtn[1], $dtn[2], $dtn[3], $dtn[4], $dtn[5], $dtn[6], $dtn[7], $dtn[8], $dtn[9], $dtn[10], $dtn[11], $dtn[12], $dtn[13], $dtn[14], $dtn[15], $dtn[16], $dtn[17], $dtn[18], $dtn[19], $dtn[20], $dtn[21], $dtn[22], $dtn[23], $dtn[24], $dtn[25], $dtn[26], $dtn[27], $dtn[28]
                ])
                ->setXAxis([
                    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28'
                ]);
        } elseif ($totDay === 29) {
            $chart->addData('Member', [
                $dtm[1], $dtm[2], $dtm[3], $dtm[4], $dtm[5], $dtm[6], $dtm[7], $dtm[8], $dtm[9], $dtm[10], $dtm[11], $dtm[12], $dtm[13], $dtm[14], $dtm[15], $dtm[16], $dtm[17], $dtm[18], $dtm[19], $dtm[20], $dtm[21], $dtm[22], $dtm[23], $dtm[24], $dtm[25], $dtm[26], $dtm[27], $dtm[28], $dtm[29]
            ])
                ->addData('Non-Member', [
                    $dtn[1], $dtn[2], $dtn[3], $dtn[4], $dtn[5], $dtn[6], $dtn[7], $dtn[8], $dtn[9], $dtn[10], $dtn[11], $dtn[12], $dtn[13], $dtn[14], $dtn[15], $dtn[16], $dtn[17], $dtn[18], $dtn[19], $dtn[20], $dtn[21], $dtn[22], $dtn[23], $dtn[24], $dtn[25], $dtn[26], $dtn[27], $dtn[28], $dtn[29]
                ])
                ->setXAxis([
                    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29'
                ]);
        } elseif ($totDay === 30) {
            $chart->addData('Member', [
                $dtm[1], $dtm[2], $dtm[3], $dtm[4], $dtm[5], $dtm[6], $dtm[7], $dtm[8], $dtm[9], $dtm[10], $dtm[11], $dtm[12], $dtm[13], $dtm[14], $dtm[15], $dtm[16], $dtm[17], $dtm[18], $dtm[19], $dtm[20], $dtm[21], $dtm[22], $dtm[23], $dtm[24], $dtm[25], $dtm[26], $dtm[27], $dtm[28], $dtm[29], $dtm[30]
            ])
                ->addData('Non-Member', [
                    $dtn[1], $dtn[2], $dtn[3], $dtn[4], $dtn[5], $dtn[6], $dtn[7], $dtn[8], $dtn[9], $dtn[10], $dtn[11], $dtn[12], $dtn[13], $dtn[14], $dtn[15], $dtn[16], $dtn[17], $dtn[18], $dtn[19], $dtn[20], $dtn[21], $dtn[22], $dtn[23], $dtn[24], $dtn[25], $dtn[26], $dtn[27], $dtn[28], $dtn[29], $dtn[30]
                ])
                ->setXAxis([
                    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'
                ]);
        } else {
            $chart->addData('Member', [
                $dtm[1], $dtm[2], $dtm[3], $dtm[4], $dtm[5], $dtm[6], $dtm[7], $dtm[8], $dtm[9], $dtm[10], $dtm[11], $dtm[12], $dtm[13], $dtm[14], $dtm[15], $dtm[16], $dtm[17], $dtm[18], $dtm[19], $dtm[20], $dtm[21], $dtm[22], $dtm[23], $dtm[24], $dtm[25], $dtm[26], $dtm[27], $dtm[28], $dtm[29], $dtm[30], $dtm[31]
            ])
                ->addData('Non-Member', [
                    $dtn[1], $dtn[2], $dtn[3], $dtn[4], $dtn[5], $dtn[6], $dtn[7], $dtn[8], $dtn[9], $dtn[10], $dtn[11], $dtn[12], $dtn[13], $dtn[14], $dtn[15], $dtn[16], $dtn[17], $dtn[18], $dtn[19], $dtn[20], $dtn[21], $dtn[22], $dtn[23], $dtn[24], $dtn[25], $dtn[26], $dtn[27], $dtn[28], $dtn[29], $dtn[30], $dtn[31]
                ])
                ->setXAxis([
                    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'
                ]);
        }

        return $chart;
    }

    public function membertahun($tahun)
    {
        for ($i = 1; $i <= 12; $i++) {

            $dtm[$i] = Pelanggan::whereMonth('created_at', $i)
                ->whereYear('created_at', $tahun)
                ->where('member', 1)
                ->where('status', 4)
                ->count();
            $dtn[$i] = Pelanggan::whereMonth('created_at', $i)
                ->whereYear('created_at', $tahun)
                ->where('member', 0)
                ->where('status', 4)
                ->count();
        }


        return $this->chart->barChart()
            ->setTitle('Data Member')
            ->setSubtitle('Grafik Member ' . $tahun)
            ->addData('Member', [$dtm[1], $dtm[2], $dtm[3], $dtm[4], $dtm[5], $dtm[6], $dtm[7], $dtm[8], $dtm[9], $dtm[10], $dtm[11], $dtm[12]])
            ->addData('Non Member', [$dtn[1], $dtn[2], $dtn[3], $dtn[4], $dtn[5], $dtn[6], $dtn[7], $dtn[8], $dtn[9], $dtn[10], $dtn[11], $dtn[12]])
            ->setXAxis(['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']);
    }
    // ================================================== GRAFIK MEMBER ================================================== //
}
