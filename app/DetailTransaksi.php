<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailTransaksi extends Model
{
    protected $fillable = [
        'id_transaksi', 'id_galon', 'jumlah', 'total_harga'
    ];
    public function galon()
    {
        return $this->belongsTo('App\Galon', 'id_galon', 'id');
    }
    public function transaksi()
    {
        return $this->belongsTo('App\Transaksi','id_transaksi','id');
    }
}
