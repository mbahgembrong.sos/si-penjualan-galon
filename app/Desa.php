<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desa extends Model
{
    protected $fillable = [
        'nama_desa', 'ongkir', 'kode_pos'
    ];
    public function pelanggan()
    {
        return $this->hasMany('App\Pelanggan', 'id_desa', 'id');
    }
}
