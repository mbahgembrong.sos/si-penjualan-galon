<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kurir extends Model
{
    protected $fillable = [
        'nama', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'alamat', 'id_user'
    ];
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user','id');
    }
    public function transaksi()
    {
        return $this->hasMany('App\Transaksi', 'id_kurir', 'id');
    }
}
