<?php

namespace App\Http\Controllers;

use App\Desa;
use App\Pelanggan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PelangganController extends Controller
{
    public function index()
    {
        $desa = Desa::all();
        return view('admin.pelanggan.index', compact('desa'));
    }

    public function indexKR()
    {
        $desa = Desa::all();
        return view('karyawan.pelanggan.index', compact('desa'));
    }

    public function dataDesa()
    {
        $data = Desa::all();
        return response()->json($data);
    }

    public function indexData()
    {
        $data = Pelanggan::join('users', 'users.id', '=', 'pelanggans.id_user')
            ->join('desas', 'desas.id', '=', 'pelanggans.id_desa')
            ->select('users.username', 'desas.nama_desa', 'pelanggans.*')
            ->orderBy('pelanggans.id', 'asc')->get();

        return response()->json($data);
    }

    public function addData(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date',
            'jenis_kelamin' => 'required|max:1',
            'alamat' => 'required',
            'desa' => 'required',
            'username' => 'required|min:8|max:16',
            'password' => 'required|min:8|max:16',
        ]);

        $user = User::create([
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'role' => 4,
        ]);

        Pelanggan::create([
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat' => $request->alamat,
            'id_desa' => $request->desa,
            'id_user' => $user->id,
        ]);

        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Menyimpan data Pelanggan',
        ]);
    }

    public function detailData($id)
    {
        $data = Pelanggan::join('users', 'users.id', '=', 'pelanggans.id_user')
            ->select('users.username', 'pelanggans.*')
            ->where('pelanggans.id', $id)->first();

        return response()->json($data);
    }

    public function updateData(Request $request, $id)
    {
        if ($request->password == '') {
            $this->validate($request, [
                'nama' => 'required',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required|date',
                'jenis_kelamin' => 'required|max:1',
                'alamat' => 'required',
                'desa' => 'required',
                'username' => 'required|min:8|max:16',
            ]);
        } else {
            $this->validate($request, [
                'nama' => 'required',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required|date',
                'jenis_kelamin' => 'required|max:1',
                'alamat' => 'required',
                'desa' => 'required',
                'username' => 'required|min:8|max:16',
                'password' => 'required|min:8|max:16',
            ]);
        }

        Pelanggan::where('id', $id)->update([
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat' => $request->alamat,
            'id_desa' => $request->desa,
        ]);

        $pelanggan = Pelanggan::where('id', $id)->first();

        if ($request->password == '') {
            User::where('id', $pelanggan->id_user)->update([
                'username' => $request->username,
            ]);
        } else {
            User::where('id', $pelanggan->id_user)->update([
                'username' => $request->username,
                'password' => bcrypt($request->password),
            ]);
        }
        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Menyimpan data Pelanggan',
        ]);
    }

    public function deleteData($id)
    {
        $pelanggan = Pelanggan::where('id', $id)->first();
        if ($pelanggan) {
            User::where('id', $pelanggan->id_user)->delete();
            $pelanggan->delete();

            return response()->json([
                'code' => 200,
                'message' => 'Berhasil Hapus data Pelanggan',
            ]);
        }
    }

    public function changeMember($id)
    {
        $pelanggan = Pelanggan::where('id', $id)->first();
        if ($pelanggan) {
            if ($pelanggan->member == 0) {
                Pelanggan::where('id', $id)->update([
                    'member' => 1,
                ]);
            } else {
                Pelanggan::where('id', $id)->update([
                    'member' => 0,
                ]);
            }

            return response()->json([
                'code' => 200,
                'message' => 'Berhasil Mengubah Member Pelanggan',
            ]);
        }
    }
}
