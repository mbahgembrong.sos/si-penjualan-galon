<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::attempt([
            'username' => $request->username,
            'password' => $request->password,
            'role' => 1,
        ])) {
            return redirect()->route('admin.dashboard')->with('sukses', 'Selamat Datang Di Admin Page!');
        } elseif (Auth::attempt([
            'username' => $request->username,
            'password' => $request->password,
            'role' => 2,
        ])) {
            return redirect()->route('karyawan.dashboard')->with('sukses', 'Selamat Datang Di Karyawan Page!');
        }
        if (Auth::attempt([
            'username' => $request->username,
            'password' => $request->password,
            'role' => 3,
        ])) {
            return redirect()->route('kurir.dashboard')->with('sukses', 'Selamat Datang Di Kurir Page!');
        }
        if (Auth::attempt([
            'username' => $request->username,
            'password' => $request->password,
            'role' => 4,
        ])) {
            return redirect()->route('pelanggan.dashboard')->with('sukses', 'Selamat Datang ' . $request->username . '!');
        } elseif ($request->nik == "" || $request->password == "") {
            return redirect()->route('login')->with('gagal', 'Username atau password anda masih kosong!');
        } else {
            return redirect()->route('login')->with('gagal', 'Username atau password salah!');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login')->with('logout', 'Anda telah logout!');
    }
}
