<?php

namespace App\Http\Controllers;

use App\Karyawan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KaryawanController extends Controller
{

    public function index()
    {
        return view('admin.karyawan.index');
    }

    public function indexData()
    {
        $data = Karyawan::join('users', 'users.id', '=', 'karyawans.id_user')
            ->select('users.username', 'karyawans.*')
            ->orderBy('karyawans.id', 'asc')->get();

        return response()->json($data);
    }

    public function addData(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date',
            'jenis_kelamin' => 'required|max:1',
            'alamat' => 'required',
            'username' => 'required|min:8|max:16',
            'password' => 'required|min:8|max:16',
        ]);

        $user = User::create([
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'role' => 2,
        ]);

        Karyawan::create([
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat' => $request->alamat,
            'id_user' => $user->id,
        ]);

        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Menyimpan data Karyawan',
        ]);
    }

    public function detailData($id)
    {
        $data = Karyawan::join('users', 'users.id', '=', 'karyawans.id_user')
            ->select('users.username', 'karyawans.*')
            ->where('karyawans.id', $id)->first();

        return response()->json($data);
    }

    public function updateData(Request $request, $id)
    {
        if ($request->password == '') {
            $this->validate($request, [
                'nama' => 'required',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required|date',
                'jenis_kelamin' => 'required|max:1',
                'alamat' => 'required',
                'username' => 'required|min:8|max:16',
            ]);
        } else {
            $this->validate($request, [
                'nama' => 'required',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required|date',
                'jenis_kelamin' => 'required|max:1',
                'alamat' => 'required',
                'username' => 'required|min:8|max:16',
                'password' => 'required|min:8|max:16',
            ]);
        }

        Karyawan::where('id', $id)->update([
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat' => $request->alamat,
        ]);

        $karyawan = Karyawan::where('id', $id)->first();

        if ($request->password == '') {
            User::where('id', $karyawan->id_user)->update([
                'username' => $request->username,
            ]);
        } else {
            User::where('id', $karyawan->id_user)->update([
                'username' => $request->username,
                'password' => bcrypt($request->password),
            ]);
        }
        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Menyimpan data Karyawan ',
        ]);
    }

    public function deleteData($id)
    {
        $karyawan = Karyawan::where('id', $id)->first();
        if ($karyawan) {
            User::where('id', $karyawan->id_user)->delete();
            $karyawan->delete();

            return response()->json([
                'code' => 200,
                'message' => 'Berhasil Hapus data Karyawan ',
            ]);
        }
    }
}
