<?php

namespace App\Http\Controllers\Api;

use App\Desa;
use App\DetailTransaksi;
use App\Diskon;
use App\Galon;
use App\Http\Controllers\Controller;
use App\Karyawan;
use App\Kurir;
use App\Pelanggan;
use App\Transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ApiTransaksiController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pelanggan_id'   => 'required',
        ]);

        if ($validator->fails()) {
            return [
                'status'    => false,
                'messages'  => $validator->messages()
            ];
        }

        $transaksi = Transaksi::where('id_pelanggan', $request->pelanggan_id)
            ->where('id', 'LIKE', "%$request->search%")
            ->with([
                'pelanggan' => function ($query) {
                    $query->with(['desa']);
                },
                'karyawan',
                'kurir',
                'detail_transaksi' => function ($query) {
                    $query->with('galon');
                },
                'desa'
            ])->orderBy('id', 'desc')->get();

        return response()->json([
            'status' => true,
            'data' => $transaksi
        ]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pelanggan'       =>  'required',
            'detail_transaksi' =>  'required',
        ]);

        if ($validator->fails()) {
            return [
                'status'    => false,
                'messages'  => $validator->messages()
            ];
        }

        $pelanggan = Pelanggan::join('desas', 'desas.id', 'pelanggans.id_desa')
            ->where('pelanggans.id', $request->pelanggan)->first();

        $transaksi = new Transaksi();
        $transaksi->id_pelanggan = $request->pelanggan;
        $transaksi->ongkir = $pelanggan->ongkir;
        $transaksi->alamat = $pelanggan->alamat;
        $transaksi->lang = $pelanggan->lang;
        $transaksi->long = $pelanggan->long;
        $transaksi->id_desa = $pelanggan->id_desa;

        $transaksi->id_diskon = 0;
        $transaksi->jenis_potongan = 'Langsung';
        $transaksi->diskon = 0;

        $transaksi->total = 0;
        $transaksi->save();

        $totaltr = 0;
        foreach ($request->detail_transaksi as $data) {

            $galon = Galon::where('id', $data['galon']['id'])->first();
            $totalharga = $data['quantity'] * $galon->harga_jual;

            $detail = new DetailTransaksi();
            $detail->id_transaksi = $transaksi->id;
            $detail->id_galon = $data['galon']['id'];
            $detail->harga = $galon->harga_jual;
            $detail->jumlah = $data['quantity'];
            $detail->total_harga = $totalharga;
            $detail->save();

            $totaltr = $totaltr + $totalharga;
        }

        if ($totaltr <= 100000 && $pelanggan->member == 1) {
            $diskon = Diskon::where('id', 1)->first();

            Transaksi::where('id', $transaksi->id)->update([
                'id_diskon' => 1,
                'jenis_potongan' => $diskon->jenis_potongan,
                'diskon' => $diskon->diskon,
            ]);
        }

        $totaltr = $totaltr + $transaksi->ongkir;

        if ($transaksi->jenis_potongan == 'Langsung') {
            $totaltr = $totaltr - $transaksi->diskon;
        } else {
            $totaltr = $totaltr - ($totaltr * (round($transaksi->diskon / 100)));
        }

        Transaksi::where('id', $transaksi->id)->update([
            'total' => $totaltr
        ]);

        $datas = Transaksi::join('pelanggans', 'pelanggans.id', '=', 'transaksis.id_pelanggan')
            ->join('desas', 'desas.id', '=', 'transaksis.id_desa')
            ->select(
                'transaksis.*',
                'transaksis.id as id_tr',
                'pelanggans.*',
                'pelanggans.id as id_pelanggan',
                'desas.nama_desa as desa',
            )
            ->where('transaksis.id', $transaksi->id)
            ->orderBy('transaksis.created_at', 'DESC')->first();

        $detailTR = [];
        $j = 0;

        $details = DetailTransaksi::join('transaksis', 'transaksis.id', '=', 'detail_transaksis.id_transaksi')
            ->join('galons', 'galons.id', '=', 'detail_transaksis.id_galon')
            ->where('transaksis.id', $transaksi->id)
            ->select('galons.*', 'detail_transaksis.*', 'detail_transaksis.id AS detail_id')->get();

        foreach ($details as $detail) {
            $detailTR += [
                $j => [
                    'id' => $detail->detail_id,
                    'id_transaksi' => $detail->id_transaksi,
                    'id_galon' => $detail->id_galon,
                    'jumlah' => $detail->jumlah,
                    'total_harga' => $detail->total_harga,
                    'galon' => [
                        'id' => $detail->id_galon,
                        'merk' => $detail->merk,
                        'isi_galon' => $detail->isi_galon,
                        'stok' => $detail->jml_stok,
                        'harga_jual' => $detail->harga_jual,
                        'gambar_galon' => $detail->gambar_galon

                    ]
                ]
            ];
            $j += 1;
        }

        $desa = Desa::where('id', $datas->id_desa)->first();

        $transaksi = [
            'id' => $datas->id_tr,
            'created_at' => date('d/m/Y H:i', strtotime($datas->created_at)),
            'pelanggan' => [
                'id'           => $datas->id_pelanggan,
                'nama'         => $datas->nama,
                'tempat_lahir'  => $datas->tempat_lahir,
                'tanggal_lahir' => $datas->tanggal_lahir,
                'jenis_kelamin' => $datas->jenis_kelamin,
                'alamat'       => $datas->alamat,
                'lang'         => $datas->lang,
                'long'         => $datas->long,
                'member'       => $datas->member,
                'desa'         => $desa,
            ],
            'karyawan' => $datas->id_karyawan,
            'kurir' => $datas->id_kurir,
            'diskon' => $datas->diskon,
            'ongkir' => $datas->ongkir,
            'total' => $datas->total,
            'status' => $datas->status,
            'keterangan' => $datas->keterangan,
            'bukti_transaksi' => $datas->bukti_transaksi,
            'detail_transaksi' => $detailTR,
            'alamat' => $datas->alamat,
            'lang' => $datas->lang,
            'long' => $datas->long,
            'desa' => $desa
        ];

        return response()->json([
            'status' => true,
            'message' => 'Berhasil Membuat Transaksi',
            'data' => $transaksi
        ]);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'              => 'required',
            'pelanggan'       => 'required',
            'status'          => 'required|integer',
            'file_image'      => 'string'
        ]);

        if ($validator->fails()) {
            return [
                'status'    => false,
                'messages'  => $validator->messages()
            ];
        }

        $pelanggan = Pelanggan::join('desas', 'desas.id', 'pelanggans.id_desa')
            ->where('pelanggans.id', $request->pelanggan)->first();

        $transaksi = new Transaksi();
        $transaksi->id_pelanggan   = $request->pelanggan;
        $transaksi->status         = $request->status;

        if ($request->file_image != null) {

            $image_64 = $request->file_image;

            // $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];
            // $replace = substr($image_64, 0, strpos($image_64, ',') + 1);

            // $image = str_replace($replace, '', $image_64);
            // $image = str_replace(' ', '+', $image);

            $imageName = time() . '.' . "jpg";

            Storage::disk('public')->put('bukti_transaksi/' . $imageName, base64_decode($image_64));

            $transaksi->bukti_transaksi = $imageName;

            Transaksi::where('id', $request->id)->update([
                'bukti_transaksi' => $transaksi->bukti_transaksi,
            ]);
        }

        if ($transaksi->status == 5) {
            $trx = Transaksi::where('id', $request->id)->first();
            if ($trx->status == 1 || $trx->status == 2) {
                $dtrx = DetailTransaksi::where('id_transaksi', $request->id)->get();
                foreach ($dtrx as $galons) {
                    $gln = Galon::where('id', $galons->id_galon)->select('jml_stok')->first();
                    $jml_stok = $gln->jml_stok + $galons->jumlah;
                    Galon::where('id', $galons->id_galon)->update([
                        'jml_stok' => $jml_stok
                    ]);
                }
            }
        }

        Transaksi::where('id', $request->id)->update([
            'status' => $transaksi->status,
        ]);
        $transaksi = Transaksi::where('id', $request->id)
            ->with([
                'pelanggan' => function ($query) {
                    $query->with(['desa']);
                },
                'karyawan',
                'kurir',
                'detail_transaksi' => function ($query) {
                    $query->with('galon');
                },
                'desa'
            ])->first();
        return response()->json([
            'status'  => true,
            'message' => 'Berhasil Update Transaksi',
            'data' => $transaksi
        ]);
    }

    public function listOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kurir_id'   => 'required',
        ]);

        if ($validator->fails()) {
            return [
                'status'    => false,
                'messages'  => $validator->messages()
            ];
        }

        $transaksi = Transaksi::where('id_kurir', $request->kurir_id)
            ->where('id', 'LIKE', "%$request->search%")
            ->with([
                'pelanggan' => function ($query) {
                    $query->with(['desa']);
                },
                'karyawan',
                'kurir',
                'detail_transaksi' => function ($query) {
                    $query->with('galon');
                },
                'desa'
            ])->orderBy('id', 'desc')->get();

        return response()->json([
            'status' => true,
            'data' => $transaksi
        ]);
    }

    public function listOrderUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'              => 'required',
            'kurir'           => 'required',
            'status'          => 'required|integer',
        ]);

        if ($validator->fails()) {
            return [
                'status'    => false,
                'messages'  => $validator->messages()
            ];
        }

        $pelanggan = Pelanggan::join('desas', 'desas.id', 'pelanggans.id_desa')
            ->where('pelanggans.id', $request->pelanggan)->first();

        $transaksi = new Transaksi();
        $transaksi->id_kurir   = $request->kurir;
        $transaksi->status         = $request->status;

        if ($transaksi->status == 5) {
            $trx = Transaksi::where('id', $request->id)->first();
            if ($trx->status == 1 || $trx->status == 2) {
                $dtrx = DetailTransaksi::where('id_transaksi', $request->id)->get();
                foreach ($dtrx as $galons) {
                    $gln = Galon::where('id', $galons->id_galon)->select('jml_stok')->first();
                    $jml_stok = $gln->jml_stok + $galons->jumlah;
                    Galon::where('id', $galons->id_galon)->update([
                        'jml_stok' => $jml_stok
                    ]);
                }
            }
        }

        Transaksi::where('id', $request->id)->where('id_kurir', $transaksi->id_kurir)->update([
            'status' => $transaksi->status,
        ]);

        Transaksi::where('id', $request->id)->update([
            'status' => $transaksi->status,
        ]);
        $transaksi = Transaksi::where('id', $request->id)
            ->with([
                'pelanggan' => function ($query) {
                    $query->with(['desa']);
                },
                'karyawan',
                'kurir',
                'detail_transaksi' => function ($query) {
                    $query->with('galon');
                },
                'desa'
            ])->first();

        return response()->json([
            'status'  => true,
            'message' => 'Berhasil Update Transaksi',
            'data' => $transaksi
        ]);
    }
}
