<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transaksi;
use Illuminate\Http\Request;

class ApiLaporanKurirControlller extends Controller
{
    function index($id)
    {
        $belumDikirim = Transaksi::where([
            ['id_kurir', $id],
            ['status', '3']
        ])->count();

        $selesai = Transaksi::where([
            ['id_kurir', $id],
            ['status', '4']
        ])->count();

        $dibatalkan = Transaksi::where([
            ['id_kurir', $id],
            ['status', '5']
        ])->count();

        $data = [];
        $data[0] = [
            'nama' => 'Jumlah Belum Terkirim',
            'jumlah' => $belumDikirim,
            'icon' => 'icon_laporan_kurir/not-delivered.png',
        ];
        $data[1] = [
            'nama' => 'Jumlah Terkirim',
            'jumlah' => $selesai,
            'icon' => 'icon_laporan_kurir/delivered.png',
        ];
        $data[2] = [
            'nama' => 'Jumlah Pesanan Dibatalkan',
            'jumlah' => $dibatalkan,
            'icon' => 'icon_laporan_kurir/cenceled.png',
        ];

        return response()->json([
            'status'    => true,
            'data'      => $data
        ]);
    }
}
