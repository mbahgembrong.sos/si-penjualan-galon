<?php

namespace App\Http\Controllers\Api;

use App\Galon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiGalonControlller extends Controller
{
    function index()
    {
        $galon = Galon::select('*', 'jml_stok as stok')->get();

        return response()->json([
            'status'    => true,
            'data'      => $galon
        ]);
    }
}
