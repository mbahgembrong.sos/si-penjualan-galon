<?php

namespace App\Http\Controllers\Api;

use App\Desa;
use App\Helper\UserServices;
use App\Http\Controllers\Controller;
use App\Kurir;
use App\Pelanggan;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ApiAuthControlller extends Controller
{
    function login(Request $request)
    {
        $response = (new UserServices($request->username, $request->password))->login($request->deviceName);
        return response()->json($response);
    }

    function logout(Request $request)
    {
        $userdt = $request->user();
        $request->user()->tokens()->delete();
        return response()->json([
            'status' => true,
            'message' => 'Logout Success',
            'data' => $userdt
        ]);
    }


    function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama'              =>  'required',
            'tempat_lahir'      =>  'required',
            'tanggal_lahir'     =>  'required',
            'jenis_kelamin'     =>  'required',
            'alamat'            =>  'required',
            'desa'              =>  'required',
            'username'          =>  'required|min:8|max:16',
            'password'          =>  'required|min:8|max:16',
        ]);

        if ($validator->fails()) {
            return [
                'status'    => false,
                'messages'  => $validator->messages()
            ];
        }

        $user               = new User();
        $user->username     = $request->username;
        $user->password     = bcrypt($request->password);
        $user->role         = 4;
        $user->created_at   = Carbon::now();
        $user->updated_at   = Carbon::now();
        $user->save();

        $pelanggan = new Pelanggan();
        $pelanggan->nama              = $request->nama;
        $pelanggan->tempat_lahir      = $request->tempat_lahir;
        $pelanggan->tanggal_lahir     = Carbon::parse($request->tanggal_lahir)->format('Y-m-d');
        $pelanggan->jenis_kelamin     = $request->jenis_kelamin;
        $pelanggan->alamat            = $request->alamat;
        $pelanggan->member            = 0;
        $pelanggan->id_desa           = $request->desa;
        $pelanggan->id_user           = $user->id;
        $pelanggan->lang              = $request->lang;
        $pelanggan->long              = $request->long;
        $pelanggan->created_at        = Carbon::now();
        $pelanggan->updated_at        = Carbon::now();
        $pelanggan->save();

        $response = (new UserServices($request->nama, $request->password))->login($request->deviceName);
        return response()->json($response);
    }

    function edit($id)
    {
        $profile = DB::select("
            SELECT u.username, p.*
            FROM users u, pelanggans p
            WHERE u.id = p.id_user AND u.id = $id
        ");

        $desa = Desa::where('id', $profile[0]->id_desa)->first();

        $profile[0]->desa = $desa;

        return response()->json([
            'status'    => true,
            'data'      => $profile
        ]);
    }

    function listDesa()
    {
        $desa = Desa::all();

        return response()->json([
            'status'    => true,
            'data'      => $desa
        ]);
    }

    function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama'              =>  'required',
            'tempat_lahir'      =>  'required',
            'tanggal_lahir'     =>  'required',
            'jenis_kelamin'     =>  'required',
            'alamat'            =>  'required',
            'desa'              =>  'required',
        ]);

        if ($validator->fails()) {
            return [
                'status'    => false,
                'messages'  => $validator->messages()
            ];
        }
        if ($request->has('password') || $request->has('username')) {
            $user = User::where('id', $id)->update([
                'username' => $request->username,
                'password' => bcrypt($request->password),
            ]);
        }


        $user = User::where('id', $id)->first();

        if ($user->role == 3) {
            $data = Kurir::where('id_user', $id)->update([
                'nama'              => $request->nama,
                'tempat_lahir'      => $request->tempat_lahir,
                'tanggal_lahir'     => date('Y-m-d', strtotime($request->tanggal_lahir)),
                'jenis_kelamin'     => $request->jenis_kelamin,
                'alamat'            => $request->alamat,
            ]);

            $data = Kurir::where('id_user', $id)->get();
        } elseif ($user->role == 4) {
            $data = Pelanggan::where('id_user', $id)->update([
                'nama'              => $request->nama,
                'tempat_lahir'      => $request->tempat_lahir,
                'tanggal_lahir'     => date('Y-m-d', strtotime($request->tanggal_lahir)),
                'jenis_kelamin'     => $request->jenis_kelamin,
                'alamat'            => $request->alamat,
                'id_desa'           => $request->desa,
                'lang'              => $request->lang,
                'long'              => $request->long,
            ]);

            $data = Pelanggan::where('id_user', $id)->get();
            $desa = Desa::where('id', $data[0]['id_desa'])->first();
            $data[0]['desa'] = $desa;
        }

        return response()->json([
            'status'    => true,
            'data'      => $data
        ]);
    }
}
