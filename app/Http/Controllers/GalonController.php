<?php

namespace App\Http\Controllers;

use App\Galon;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Http\Request;

class GalonController extends Controller
{
    public function index()
    {
        return view('admin.galon.index');
    }

    public function indexKR()
    {
        return view('karyawan.galon.index');
    }

    public function indexData()
    {
        $data = Galon::all();

        return response()->json($data);
    }

    public function addData(Request $request)
    {
        $this->validate($request, [
            'merk' => 'required',
            'isi_galon' => 'required',
            'harga_awal' => 'required|integer',
            'harga_jual' => 'required|integer',
            'gambar_galon' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $nm_gambar = time() . '.' . $request->gambar_galon->extension();
        $request->gambar_galon->move(public_path('img_galon'), $nm_gambar);


        Galon::create([
            'merk' => $request->merk,
            'isi_galon' => $request->isi_galon,
            'harga_awal' => $request->harga_awal,
            'harga_jual' => $request->harga_jual,
            'gambar_galon' => $nm_gambar,
        ]);

        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Menyimpan data Galon',
        ]);
    }

    public function detailData($id)
    {
        $data = Galon::where('id', $id)->first();

        return response()->json($data);
    }

    public function updateData(Request $request, $id)
    {
        if ($request->gambar_galon == '') {
            $this->validate($request, [
                'merk' => 'required',
                'isi_galon' => 'required',
                'harga_awal' => 'required|integer',
                'harga_jual' => 'required|integer',
            ]);
        } else {
            $this->validate($request, [
                'merk' => 'required',
                'isi_galon' => 'required',
                'harga_awal' => 'required|integer',
                'harga_jual' => 'required|integer',
                'gambar_galon' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
        }

        if ($request->gambar_galon == '') {
            Galon::where('id', $id)->update([
                'merk' => $request->merk,
                'isi_galon' => $request->isi_galon,
                'harga_awal' => $request->harga_awal,
                'harga_jual' => $request->harga_jual,
            ]);
        } else {
            $cek = Galon::where('id', $id)->first();
            if ($cek->gambar_galon != '') {
                unlink(public_path('img_galon/' . $cek->gambar_galon));
            }

            $nm_gambar = time() . '.' . $request->gambar_galon->extension();
            $request->gambar_galon->move(public_path('img_galon'), $nm_gambar);

            Galon::where('id', $id)->update([
                'merk' => $request->merk,
                'isi_galon' => $request->isi_galon,
                'harga_awal' => $request->harga_awal,
                'harga_jual' => $request->harga_jual,
                'gambar_galon' => $nm_gambar,
            ]);
        }


        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Menyimpan data Galon ',
        ]);
    }

    public function deleteData($id)
    {
        $galon = Galon::where('id', $id)->first();
        if ($galon) {
            $galon->delete();
            if ($galon->gambar_galon != '') {
                unlink(public_path('img_galon/' . $galon->gambar_galon));
            }

            return response()->json([
                'code' => 200,
                'message' => 'Berhasil Hapus data Galon ',
            ]);
        }
    }
}
