<?php

namespace App\Http\Controllers;

use App\Diskon;
use Illuminate\Http\Request;

class DiskonController extends Controller
{
    public function index()
    {
        return view('admin.lainnya.diskon');
    }

    public function indexKR()
    {
        return view('karyawan.lainnya.diskon');
    }

    public function indexData()
    {
        $data = Diskon::all();

        return response()->json($data);
    }

    public function addData(Request $request)
    {
        $this->validate($request, [
            'nama_diskon' => 'required',
            'jenis_potongan' => 'required',
            'diskon' => 'required|integer',
        ]);

        Diskon::create([
            'nama_diskon' => $request->nama_diskon,
            'jenis_potongan' => $request->jenis_potongan,
            'diskon' => $request->diskon,
        ]);

        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Menyimpan data Diskon',
        ]);
    }

    public function detailData($id)
    {
        $data = Diskon::where('id', $id)->first();

        return response()->json($data);
    }

    public function updateData(Request $request, $id)
    {
        $this->validate($request, [
            'nama_diskon' => 'required',
            'jenis_potongan' => 'required',
            'diskon' => 'required|integer',
        ]);

        Diskon::where('id', $id)->update([
            'nama_diskon' => $request->nama_diskon,
            'jenis_potongan' => $request->jenis_potongan,
            'diskon' => $request->diskon,
        ]);

        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Menyimpan data Diskon',
        ]);
    }

    public function deleteData($id)
    {
        $desa = Diskon::where('id', $id)->first();
        if ($desa) {
            $desa->delete();

            return response()->json([
                'code' => 200,
                'message' => 'Berhasil Hapus data Diskon ',
            ]);
        }
    }
}
