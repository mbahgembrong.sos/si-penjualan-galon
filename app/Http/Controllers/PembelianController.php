<?php

namespace App\Http\Controllers;

use App\Galon;
use App\Pembelian;
use Illuminate\Http\Request;

class PembelianController extends Controller
{
    public function index()
    {
        return view('admin.pembelian.index');
    }

    public function indexData()
    {
        $data = Pembelian::join('galons', 'galons.id', '=', 'pembelians.id_galon')
            ->select('galons.merk', 'galons.isi_galon', 'pembelians.*')->orderBy('pembelians.created_at', 'DESC')->get();

        return response()->json($data);
    }

    public function getDataGalon()
    {
        $data = Galon::all();
        return response()->json($data);
    }

    public function getDataGalonSelected($id)
    {
        $data = Galon::where('id', $id)->first();
        return response()->json($data);
    }

    public function addData(Request $request)
    {
        $this->validate($request, [
            'id_galon' => 'required',
            'harga_satuan' => 'required|integer',
            'harga_jual' => 'required|integer',
            'jumlah_beli' => 'required|integer',
            'total_harga_beli' => 'required|integer',
        ]);

        Pembelian::create([
            'id_galon' => $request->id_galon,
            'harga_satuan' => $request->harga_satuan,
            'jumlah_beli' => $request->jumlah_beli,
            'total_harga_beli' => $request->total_harga_beli,
        ]);

        $galon = Galon::where('id', $request->id_galon)->first();
        $stok = $galon->jml_stok + $request->jumlah_beli;

        Galon::where('id', $galon->id)->update([
            'jml_stok' => $stok,
            'harga_awal' => $request->harga_satuan,
            'harga_jual' => $request->harga_jual,
        ]);

        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Menyimpan data Pembelian Galon',
        ]);
    }

    public function deleteData($id)
    {
        $data = Pembelian::where('id', $id)->first();
        $galon = Galon::where('id', $data->id_galon)->first();
        $stok = $galon->jml_stok - $data->jumlah_beli;
        if ($data) {
            Galon::where('id', $galon->id)->update([
                'jml_stok' => $stok,
            ]);
            $data->delete();

            return response()->json([
                'code' => 200,
                'message' => 'Berhasil Hapus data Pembelian Galon ',
            ]);
        }
    }
}
