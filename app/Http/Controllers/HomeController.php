<?php

namespace App\Http\Controllers;

use App\Charts\ChartTransaksiSetahun;
use App\DetailTransaksi;
use App\Galon;
use App\Pembelian;
use App\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function indexHome()
    {
        $data = Galon::all();
        return view('indexGuest', ['data' => $data]);
    }

    public function dashboardAdmin(ChartTransaksiSetahun $chart)
    {

        $trjml = Transaksi::whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->count();

        $dataglnterjual = DetailTransaksi::whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))
            ->get();

        $datagln = Galon::all();

        $databelistok = Pembelian::whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->get();

        $glnterjual = 0;

        foreach ($dataglnterjual as $data) {
            $glnterjual = $glnterjual + $data->jumlah;
        }

        $stokgln = 0;

        foreach ($datagln as $data) {
            $stokgln = $stokgln + $data->jml_stok;
        }

        $stokdibeli = 0;

        foreach ($databelistok as $data) {
            $stokdibeli = $stokdibeli + $data->jumlah_beli;
        }

        if (Auth::user()->role == 1) {
            return view('admin.dashboard', ['chart' => $chart->build(), 'trjml' => $trjml, 'glnterjual' => $glnterjual, 'stokgln' => $stokgln, 'stokdibeli' => $stokdibeli]);
        } else {
            return view('karyawan.dashboard.index', ['chart' => $chart->build(), 'trjml' => $trjml, 'glnterjual' => $glnterjual, 'stokgln' => $stokgln, 'stokdibeli' => $stokdibeli]);
        }
    }
}
