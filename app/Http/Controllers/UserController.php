<?php

namespace App\Http\Controllers;

use App\Admin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function updateProfile(Request $request, $id_user)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date',
            'jenis_kelamin' => 'required|max:1',
            'alamat' => 'required',
            'username' => 'required|max:16',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->with('gagal', 'Gagal Update profile!');
        }

        if ($request->passwordlama == "" && $request->passwordbaru == "") {
            Admin::where('id_user', $id_user)->update([
                'nama' => $request->nama,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'jenis_kelamin' => $request->jenis_kelamin,
                'alamat' => $request->alamat,
            ]);
            User::where('id', $id_user)->update([
                'username' => $request->username,
            ]);
            return back()->with('sukses', 'Berhasil update profile!');
        } elseif ($request->passwordlama && $request->passwordbaru) {
            $check = User::where('id', $id_user)->first();
            if (Hash::check($request->passwordlama, $check->password)) {
                Admin::where('id_user', $id_user)->update([
                    'nama' => $request->nama,
                    'tempat_lahir' => $request->tempat_lahir,
                    'tanggal_lahir' => $request->tanggal_lahir,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'alamat' => $request->alamat,
                ]);
                User::where('id', $id_user)->update([
                    'username' => $request->username,
                    'password' => bcrypt($request->passwordbaru),
                ]);
                return back()->with('sukses', 'Berhasil update profile!');
            } else {
                return back()->with('gagal', 'Gagal update profile, Password lama anda salah!');
            }
        } else {
            return back()->with('gagal', 'Gagal update profile!');
        }
    }

    public function adminProfile()
    {
        $users = User::join('admins', 'admins.id_user', '=', 'users.id')
            ->where('users.username', Auth::user()->username)->first();

        return view('admin.profile', compact(['users']));
    }

    public function karyawanProfile()
    {
        $users = User::join('karyawans', 'karyawans.id_user', '=', 'users.id')
            ->where('users.username', Auth::user()->username)->first();

        return view('karyawan.profile', compact(['users']));
    }
}
