<?php

namespace App\Http\Controllers;

use App\Kurir;
use App\User;
use Illuminate\Http\Request;

class KurirController extends Controller
{
    public function index()
    {
        return view('admin.kurir.index');
    }

    public function indexKR()
    {
        return view('karyawan.kurir.index');
    }

    public function indexData()
    {
        $data = Kurir::join('users', 'users.id', '=', 'kurirs.id_user')
            ->select('users.username', 'kurirs.*')
            ->orderBy('kurirs.id', 'asc')->get();

        return response()->json($data);
    }

    public function addData(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date',
            'jenis_kelamin' => 'required|max:1',
            'alamat' => 'required',
            'username' => 'required|min:8|max:16',
            'password' => 'required|min:8|max:16',
        ]);

        $user = User::create([
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'role' => 3,
        ]);

        Kurir::create([
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat' => $request->alamat,
            'id_user' => $user->id,
        ]);

        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Menyimpan data Kurir',
        ]);
    }

    public function detailData($id)
    {
        $data = Kurir::join('users', 'users.id', '=', 'kurirs.id_user')
            ->select('users.username', 'kurirs.*')
            ->where('kurirs.id', $id)->first();

        return response()->json($data);
    }

    public function updateData(Request $request, $id)
    {
        if ($request->password == '') {
            $this->validate($request, [
                'nama' => 'required',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required|date',
                'jenis_kelamin' => 'required|max:1',
                'alamat' => 'required',
                'username' => 'required|min:8|max:16',
            ]);
        } else {
            $this->validate($request, [
                'nama' => 'required',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required|date',
                'jenis_kelamin' => 'required|max:1',
                'alamat' => 'required',
                'username' => 'required|min:8|max:16',
                'password' => 'required|min:8|max:16',
            ]);
        }

        Kurir::where('id', $id)->update([
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat' => $request->alamat,
        ]);

        $kurir = Kurir::where('id', $id)->first();

        if ($request->password == '') {
            User::where('id', $kurir->id_user)->update([
                'username' => $request->username,
            ]);
        } else {
            User::where('id', $kurir->id_user)->update([
                'username' => $request->username,
                'password' => bcrypt($request->password),
            ]);
        }
        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Menyimpan data Kurir ',
        ]);
    }

    public function deleteData($id)
    {
        $kurir = Kurir::where('id', $id)->first();
        if ($kurir) {
            User::where('id', $kurir->id_user)->delete();
            $kurir->delete();

            return response()->json([
                'code' => 200,
                'message' => 'Berhasil Hapus data Kurir ',
            ]);
        }
    }
}
