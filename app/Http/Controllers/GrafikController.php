<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Charts\ChartGrafik;
use Illuminate\Http\Request;

class GrafikController extends Controller
{
    public function indexPenjualanAd(Request $request, ChartGrafik $chart)
    {
        if ($request->method() == 'GET') {
            $date = Carbon::now()->format('Y-m');
            return view('admin.grafik.penjualan.index', ['chart' => $chart->penjualanBulan($date)]);
        } else {
            if (!is_null($request->bulan)) {
                return view('admin.grafik.penjualan.index', ['chart' => $chart->penjualanBulan($request->bulan)]);
            } elseif (!is_null($request->tahun)) {
                return view('admin.grafik.penjualan.index', ['chart' => $chart->penjualanTahun($request->tahun)]);
            }
        }
    }

    public function indexPenjualan(Request $request, ChartGrafik $chart)
    {
        if ($request->method() == 'GET') {
            $date = Carbon::now()->format('Y-m');
            return view('karyawan.grafik.penjualan.index', ['chart' => $chart->penjualanBulan($date)]);
        } else {
            if (!is_null($request->bulan)) {
                return view('karyawan.grafik.penjualan.index', ['chart' => $chart->penjualanBulan($request->bulan)]);
            } elseif (!is_null($request->tahun)) {
                return view('karyawan.grafik.penjualan.index', ['chart' => $chart->penjualanTahun($request->tahun)]);
            }
        }
    }

    public function indexMemberAd(Request $request, ChartGrafik $chart)
    {
        if ($request->method() == 'GET') {
            $date = Carbon::now()->format('Y-m');
            return view('admin.grafik.member.index', ['chart' => $chart->memberBulan($date)]);
        } else {
            if (!is_null($request->bulan)) {
                return view('admin.grafik.member.index', ['chart' => $chart->memberBulan($request->bulan)]);
            } elseif (!is_null($request->tahun)) {
                return view('admin.grafik.member.index', ['chart' => $chart->memberTahun($request->tahun)]);
            }
        }
    }

    public function indexMember(Request $request, ChartGrafik $chart)
    {
        if ($request->method() == 'GET') {
            $date = Carbon::now()->format('Y-m');
            return view('karyawan.grafik.member.index', ['chart' => $chart->memberBulan($date)]);
        } else {
            if (!is_null($request->bulan)) {
                return view('karyawan.grafik.member.index', ['chart' => $chart->memberBulan($request->bulan)]);
            } elseif (!is_null($request->tahun)) {
                return view('karyawan.grafik.member.index', ['chart' => $chart->memberTahun($request->tahun)]);
            }
        }
    }

    public function indexPendapatanAd(Request $request, ChartGrafik $chart)
    {
        if ($request->method() == 'GET') {
            $date = Carbon::now()->format('Y-m');
            return view('admin.grafik.pendapatan.index', ['chart' => $chart->pendapatanBulan($date)]);
        } else {
            if (!is_null($request->bulan)) {
                return view('admin.grafik.pendapatan.index', ['chart' => $chart->pendapatanBulan($request->bulan)]);
            } elseif (!is_null($request->tahun)) {
                return view('admin.grafik.pendapatan.index', ['chart' => $chart->pendapatanTahun($request->tahun)]);
            }
        }
    }

    public function indexPendapatan(Request $request, ChartGrafik $chart)
    {
        if ($request->method() == 'GET') {
            $date = Carbon::now()->format('Y-m');
            return view('karyawan.grafik.pendapatan.index', ['chart' => $chart->pendapatanBulan($date)]);
        } else {
            if (!is_null($request->bulan)) {
                return view('karyawan.grafik.pendapatan.index', ['chart' => $chart->pendapatanBulan($request->bulan)]);
            } elseif (!is_null($request->tahun)) {
                return view('karyawan.grafik.pendapatan.index', ['chart' => $chart->pendapatanTahun($request->tahun)]);
            }
        }
    }
}
