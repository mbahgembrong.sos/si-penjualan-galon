<?php

namespace App\Http\Controllers;

use App\Desa;
use Illuminate\Http\Request;

class DesaController extends Controller
{
    public function index()
    {
        return view('admin.lainnya.desa');
    }

    public function indexKR()
    {
        return view('karyawan.lainnya.desa');
    }

    public function indexData()
    {
        $data = Desa::all();

        return response()->json($data);
    }

    public function addData(Request $request)
    {
        $this->validate($request, [
            'nama_desa' => 'required',
            'ongkir' => 'required|integer',
            'kode_pos' => 'required|numeric'
        ]);

        Desa::create([
            'nama_desa' => $request->nama_desa,
            'ongkir' => $request->ongkir,
            'kode_pos' => $request->kode_pos
        ]);

        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Menyimpan data Desa',
        ]);
    }

    public function detailData($id)
    {
        $data = Desa::where('id', $id)->first();

        return response()->json($data);
    }

    public function updateData(Request $request, $id)
    {
        $this->validate($request, [
            'nama_desa' => 'required',
            'ongkir' => 'required|integer',
        ]);

        Desa::where('id', $id)->update([
            'nama_desa' => $request->nama_desa,
            'ongkir' => $request->ongkir,
        ]);

        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Menyimpan data Desa ',
        ]);
    }

    public function deleteData($id)
    {
        $desa = Desa::where('id', $id)->first();
        if ($desa) {
            $desa->delete();

            return response()->json([
                'code' => 200,
                'message' => 'Berhasil Hapus data Desa ',
            ]);
        }
    }
}
