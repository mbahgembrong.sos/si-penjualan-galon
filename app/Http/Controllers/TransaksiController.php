<?php

namespace App\Http\Controllers;

use App\DetailTransaksi;
use App\Galon;
use App\Karyawan;
use App\Kurir;
use App\Transaksi;
use Barryvdh\DomPDF\PDF as DomPDFPDF;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Mockery\Undefined;
use PDF;

class TransaksiController extends Controller
{
    public function index()
    {
        return view('admin.transaksi.index');
    }

    public function indexKR()
    {
        return view('karyawan.transaksi.index');
    }

    public function trackingTRKR()
    {
        return view('karyawan.transaksi.tracking');
    }

    public function trackingTR()
    {
        return view('admin.transaksi.tracking');
    }

    public function indexData()
    {
        $data = Transaksi::join('pelanggans', 'pelanggans.id', '=', 'transaksis.id_pelanggan')
            ->join('desas', 'desas.id', '=', 'pelanggans.id_desa')
            ->select(
                'transaksis.*',
                'pelanggans.nama as nama_pelanggan',
                'pelanggans.alamat',
                'desas.nama_desa as desa',
            )
            ->orderBy('transaksis.created_at', 'DESC')->get();

        $transaksiall = [];
        $transaksi = [];
        $i = 0;
        foreach ($data as $datas) {

            $nama_karyawan = '-';
            $nama_kurir = '-';

            if ($datas->id_karyawan != null) {
                $kry = Karyawan::where('id', $datas->id_karyawan)->get();
                foreach ($kry as $data) {
                    $nama_karyawan = $data->nama;
                }
            }

            if ($datas->id_kurir != null) {
                $krr = Kurir::where('id', $datas->id_kurir)->get();
                foreach ($krr as $data) {
                    $nama_kurir = $data->nama;
                }
            }

            $transaksi += [
                $i => [
                    'id' => $datas->id,
                    'created_at' => date('Y-m-d H:i:s',  strtotime($datas->created_at)),
                    'nama_pelanggan' => $datas->nama_pelanggan,
                    'nama_karyawan' => $nama_karyawan,
                    'nama_kurir' => $nama_kurir,
                    'alamat' => "(" . $datas->desa . ")<br>" . $datas->alamat,
                    'total' => $datas->total,
                    'status' => $datas->status,
                    'keterangan' => $datas->keterangan,
                    'bukti_transaksi' => $datas->bukti_transaksi
                ],
            ];

            $i += 1;
        }

        $transaksiall = $transaksi;

        return response()->json($transaksiall);
    }

    public function getKaryawan($id)
    {
        $data = Karyawan::where('id', $id)->first();
        return response()->json($data);
    }

    public function getKurir($id)
    {
        $data = Kurir::where('id', $id)->first();
        return response()->json($data);
    }

    public function getDetailTr($id)
    {
        $datas = Transaksi::join('pelanggans', 'pelanggans.id', '=', 'transaksis.id_pelanggan')
            ->join('desas', 'desas.id', '=', 'pelanggans.id_desa')
            ->select(
                'transaksis.*',
                'pelanggans.nama as nama_pelanggan',
                'pelanggans.alamat',
                'desas.nama_desa as desa',
            )
            ->where('transaksis.id', $id)
            ->orderBy('transaksis.created_at', 'DESC')->first();

        $transaksiall = [];
        $transaksi = [];

        $nama_karyawan = '-';
        $nama_kurir = '-';

        if ($datas->id_karyawan != null) {
            $kry = Karyawan::where('id', $datas->id_karyawan)->get();
            foreach ($kry as $data) {
                $nama_karyawan = $data->nama;
            }
        }

        if ($datas->id_kurir != null) {
            $krr = Kurir::where('id', $datas->id_kurir)->get();
            foreach ($krr as $data) {
                $nama_kurir = $data->nama;
            }
        }

        $details = DetailTransaksi::join('transaksis', 'transaksis.id', '=', 'detail_transaksis.id_transaksi')
            ->join('galons', 'galons.id', '=', 'detail_transaksis.id_galon')
            ->where('transaksis.id', $id)
            ->select('galons.*', 'detail_transaksis.*')->get();

        $transaksi = [
            'id' => $id,
            'created_at' => date('Y-m-d H:i:s',  strtotime($datas->created_at)),
            'nama_pelanggan' => $datas->nama_pelanggan,
            'nama_karyawan' => $nama_karyawan,
            'nama_kurir' => $nama_kurir,
            'alamat' => "(" . $datas->desa . ")<br>" . $datas->alamat,
            'total' => $datas->total,
            'status' => $datas->status,
            'keterangan' => $datas->keterangan,
            'bukti_transaksi' => $datas->bukti_transaksi,
            'diskon' => $datas->diskon,
            'jenis_potongan' => $datas->jenis_potongan,
            'ongkir' => $datas->ongkir,
            'detail' => $details,
        ];

        $transaksiall = $transaksi;

        return response()->json($transaksiall);
    }

    public function rekapTr()
    {
        return view('admin.transaksi.rekapTr');
    }

    public function rekapTrKR()
    {
        return view('karyawan.transaksi.rekapTr');
    }

    public function dataRekapTr($bln, $thn)
    {
        $data = Transaksi::join('pelanggans', 'pelanggans.id', '=', 'transaksis.id_pelanggan')
            ->join('desas', 'desas.id', '=', 'pelanggans.id_desa')
            ->select(
                'transaksis.*',
                'pelanggans.nama as nama_pelanggan',
                'pelanggans.alamat',
                'desas.nama_desa as desa',
            )
            ->whereYear('transaksis.created_at', $thn)
            ->whereMonth('transaksis.created_at', $bln)
            ->where('transaksis.status', 4)
            ->orderBy('transaksis.created_at', 'DESC')->get();

        $transaksiall = [];
        $transaksi = [];
        $i = 0;
        foreach ($data as $datas) {

            $nama_karyawan = '-';
            $nama_kurir = '-';

            if ($datas->id_karyawan != null) {
                $kry = Karyawan::where('id', $datas->id_karyawan)->get();
                foreach ($kry as $data) {
                    $nama_karyawan = $data->nama;
                }
            }

            if ($datas->id_kurir != null) {
                $krr = Kurir::where('id', $datas->id_kurir)->get();
                foreach ($krr as $data) {
                    $nama_kurir = $data->nama;
                }
            }

            $transaksi += [
                $i => [
                    'id' => $datas->id,
                    'created_at' => date('Y-m-d H:i:s',  strtotime($datas->created_at)),
                    'nama_pelanggan' => $datas->nama_pelanggan,
                    'nama_karyawan' => $nama_karyawan,
                    'nama_kurir' => $nama_kurir,
                    'alamat' => "(" . $datas->desa . ")<br>" . $datas->alamat,
                    'total' => $datas->total,
                    'status' => $datas->status,
                    'keterangan' => $datas->keterangan,
                    'bukti_transaksi' => $datas->bukti_transaksi
                ],
            ];

            $i += 1;
        }

        $transaksiall = $transaksi;

        return response()->json($transaksiall);
    }

    public function dataRekapTrPrint($bln, $thn)
    {
        $data = Transaksi::join('pelanggans', 'pelanggans.id', '=', 'transaksis.id_pelanggan')
            ->join('karyawans', 'karyawans.id', '=', 'transaksis.id_karyawan')
            ->join('kurirs', 'kurirs.id', '=', 'transaksis.id_kurir')
            ->join('desas', 'desas.id', '=', 'pelanggans.id_desa')
            ->select(
                'transaksis.*',
                'pelanggans.nama as nama_pelanggan',
                'pelanggans.alamat',
                'desas.nama_desa as desa',
                'karyawans.nama as nama_karyawan',
                'kurirs.nama as nama_kurir'
            )
            ->whereYear('transaksis.created_at', $thn)
            ->whereMonth('transaksis.created_at', $bln)
            ->where('transaksis.status', 4)
            ->orderBy('transaksis.created_at', 'DESC')->get();

        $pdf = PDF::loadview('admin.transaksi.rekapTrPrintView', compact(['data']))->setPaper('F4', 'landscape');
        $namefile = 'rekapTransaksi_' . $bln . '-' . $thn . '_' . now() . '.pdf';
        return $pdf->download($namefile);
    }

    public function dataRekapTrPrintKr($bln, $thn)
    {
        $data = Transaksi::join('pelanggans', 'pelanggans.id', '=', 'transaksis.id_pelanggan')
            ->join('karyawans', 'karyawans.id', '=', 'transaksis.id_karyawan')
            ->join('kurirs', 'kurirs.id', '=', 'transaksis.id_kurir')
            ->join('desas', 'desas.id', '=', 'pelanggans.id_desa')
            ->select(
                'transaksis.*',
                'pelanggans.nama as nama_pelanggan',
                'pelanggans.alamat',
                'desas.nama_desa as desa',
                'karyawans.nama as nama_karyawan',
                'kurirs.nama as nama_kurir'
            )
            ->whereYear('transaksis.created_at', $thn)
            ->whereMonth('transaksis.created_at', $bln)
            ->where('transaksis.status', 4)
            ->orderBy('transaksis.created_at', 'DESC')->get();

        $pdf = PDF::loadview('karyawan.transaksi.rekapTrPrintView', compact(['data']))->setPaper('F4', 'landscape');
        $namefile = 'rekapTransaksi_' . $bln . '-' . $thn . '_' . now() . '.pdf';
        return $pdf->download($namefile);
    }

    // ===== Karyawan Konfirmasi ======
    public function konfirmasiPesanan($id)
    {
        $getidkr = Karyawan::where('id_user', auth()->user()->id)->first();

        Transaksi::where('id', $id)->update([
            'status' => 1,
            'id_karyawan' => $getidkr->id,
        ]);

        $dtrx = DetailTransaksi::where('id_transaksi', $id)->get();
        foreach ($dtrx as $galons) {
            $gln = Galon::where('id', $galons->id_galon)->first();
            $gln->update([
                'jml_stok' => ($gln->jml_stok - $galons->jumlah)
            ]);
        }

        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Konfirmasi Pesanan dengan No. Transaksi : ' . $id,
        ]);
    }

    public function batalkanPesanan($id, $ket)
    {
        $trx = Transaksi::where('id', $id)->first();
        if ($trx->status == 1 || $trx->status == 2) {
            $dtrx = DetailTransaksi::where('id_transaksi', $id)->get();
            foreach ($dtrx as $galons) {
                $gln = Galon::where('id', $galons->id_galon)->select('jml_stok')->first();
                $jml_stok = $gln->jml_stok + $galons->jumlah;
                Galon::where('id', $galons->id_galon)->update([
                    'jml_stok' => $jml_stok
                ]);
            }
        }


        Transaksi::where('id', $id)->update([
            'status' => 5,
            'keterangan' => $ket
        ]);

        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Membatalkan Pesanan dengan No. Transaksi : ' . $id,
        ]);
    }

    public function gagalkanPembayaran($id, $ket)
    {
        Transaksi::where('id', $id)->update([
            'status' => 2,
            'keterangan' => $ket
        ]);

        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Menggagalkan Pembayaran dengan No. Transaksi : ' . $id,
        ]);
    }

    public function konfirmasiPembayaran(Request $request, $id)
    {
        $this->validate($request, [
            'kurir_tr' => 'required',
        ]);

        Transaksi::where('id', $id)->update([
            'id_kurir' => $request->kurir_tr,
            'status' => 3,
        ]);

        return response()->json([
            'code' => 200,
            'message' => 'Berhasil Konfirmasi Pembayaran dengan No. Transaksi : ' . $id,
        ]);
    }

    public function dataKurir()
    {
        $kurir = Kurir::all();
        return response()->json($kurir);
    }
}
