<?php

use App\Http\Controllers\TransaksiController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@indexHome')->name('index');

Route::get('/login', function () {
    return view('login');
})->name('login');

Route::post('/login-request', 'AuthController@login')->name('login.request');

Route::get('/logout', 'AuthController@logout')->name('logout');

Route::group(['middleware' => ['web', 'auth', 'roles']], function () {

    Route::group(['roles' => 'admin'], function () {

        Route::get('admin/dashboard', 'HomeController@dashboardAdmin')->name('admin.dashboard');
        Route::get('admin/profile', 'UserController@adminProfile')->name('admin.profile');
        Route::post('admin/profile/{id_user}/update', 'UserController@updateProfile')->name('admin.profile.update');

        //Admin Kelola Karywaan
        Route::get('admin/karyawan', 'KaryawanController@index')->name('admin.karyawan');
        Route::get('admin/karyawan/data', 'KaryawanController@indexData')->name('admin.karyawan.data');
        Route::post('admin/karyawan', 'KaryawanController@addData')->name('admin.karyawan.add');
        Route::get('admin/karyawan/{id}', 'KaryawanController@detailData')->name('admin.karyawan.detail');
        Route::post('admin/karyawan/{id}', 'KaryawanController@updateData')->name('admin.karyawan.update');
        Route::delete('admin/karyawan/{id}', 'KaryawanController@deleteData')->name('admin.karyawan.delete');

        //Admin Kelola Kurir
        Route::get('admin/kurir', 'KurirController@index')->name('admin.kurir');
        Route::get('admin/kurir/data', 'KurirController@indexData')->name('admin.kurir.data');
        Route::post('admin/kurir', 'KurirController@addData')->name('admin.kurir.add');
        Route::get('admin/kurir/{id}', 'KurirController@detailData')->name('admin.kurir.detail');
        Route::post('admin/kurir/{id}', 'KurirController@updateData')->name('admin.kurir.update');
        Route::delete('admin/kurir/{id}', 'KurirController@deleteData')->name('admin.kurir.delete');

        //Admin Kelola Pelanggan
        Route::get('admin/pelanggan', 'PelangganController@index')->name('admin.pelanggan');
        Route::get('admin/pelanggan/data', 'PelangganController@indexData')->name('admin.pelanggan.data');
        Route::post('admin/pelanggan', 'PelangganController@addData')->name('admin.pelanggan.add');
        Route::get('admin/pelanggan/{id}', 'PelangganController@detailData')->name('admin.pelanggan.detail');
        Route::post('admin/pelanggan/{id}', 'PelangganController@updateData')->name('admin.pelanggan.update');
        Route::delete('admin/pelanggan/{id}', 'PelangganController@deleteData')->name('admin.pelanggan.delete');
        Route::get('admin/pelanggan/datadesa', 'PelangganController@dataDesa')->name('admin.pelanggan.datadesa');
        Route::post('admin/pelanggan/{id}/changemember', 'PelangganController@changeMember')->name('admin.pelanggan.changemember');

        //Admin Kelola Galon
        Route::get('admin/galon', 'GalonController@index')->name('admin.galon');
        Route::get('admin/galon/data', 'GalonController@indexData')->name('admin.galon.data');
        Route::post('admin/galon', 'GalonController@addData')->name('admin.galon.add');
        Route::get('admin/galon/{id}', 'GalonController@detailData')->name('admin.galon.detail');
        Route::post('admin/galon/{id}', 'GalonController@updateData')->name('admin.galon.update');
        Route::delete('admin/galon/{id}', 'GalonController@deleteData')->name('admin.galon.delete');

        //Admin Kelola Desa
        Route::get('admin/desa', 'DesaController@index')->name('admin.desa');
        Route::get('admin/desa/data', 'DesaController@indexData')->name('admin.desa.data');
        Route::post('admin/desa', 'DesaController@addData')->name('admin.desa.add');
        Route::get('admin/desa/{id}', 'DesaController@detailData')->name('admin.desa.detail');
        Route::post('admin/desa/{id}', 'DesaController@updateData')->name('admin.desa.update');
        Route::delete('admin/desa/{id}', 'DesaController@deleteData')->name('admin.desa.delete');

        //Admin Kelola Diskon
        Route::get('admin/diskon', 'DiskonController@index')->name('admin.diskon');
        Route::get('admin/diskon/data', 'DiskonController@indexData')->name('admin.diskon.data');
        Route::post('admin/diskon', 'DiskonController@addData')->name('admin.diskon.add');
        Route::get('admin/diskon/{id}', 'DiskonController@detailData')->name('admin.diskon.detail');
        Route::post('admin/diskon/{id}', 'DiskonController@updateData')->name('admin.diskon.update');
        Route::delete('admin/diskon/{id}', 'DiskonController@deleteData')->name('admin.diskon.delete');

        //Admin Kelola Pembelian
        Route::get('admin/pembelian', 'PembelianController@index')->name('admin.pembelian');
        Route::get('admin/pembelian/data', 'PembelianController@indexData')->name('admin.pembelian.data');
        Route::get('admin/pembelian/dataGalon', 'PembelianController@getDataGalon')->name('admin.pembelian.dataGalon');
        Route::get('admin/pembelian/dataGalon/{id}', 'PembelianController@getDataGalonSelected')->name('admin.pembelian.dataGalonSelected');
        Route::post('admin/pembelian', 'PembelianController@addData')->name('admin.pembelian.add');
        Route::delete('admin/pembelian/{id}', 'PembelianController@deleteData')->name('admin.pembelian.delete');

        //Admin Kelola Transaksi
        Route::get('admin/transaksi', 'TransaksiController@index')->name('admin.transaksi');
        Route::get('admin/transaksi/data', 'TransaksiController@indexData')->name('admin.transaksi.data');
        Route::get('admin/transaksi/dataDetailTransaksi/{id}', 'TransaksiController@getDetailTr')->name('admin.transaksi.getDetailTransaksi');
        Route::get('admin/tracking', 'TransaksiController@trackingTR')->name('admin.transaksi.tracking');

        //Admin Rekap Transaksi
        Route::get('admin/transaksi/rekap', 'TransaksiController@rekapTr')->name('admin.transaksi.rekap');
        Route::get('admin/transaksi/rekap/{bln}/{thn}', 'TransaksiController@dataRekapTr')->name('admin.transaksi.rekap.data');
        Route::get('admin/transaksi/rekap/{bln}/{thn}/print', 'TransaksiController@dataRekapTrPrint')->name('admin.transaksi.rekap.cetak');

        Route::group(['prefix'  =>  'admin/grafik'], function () {
            // Grafik Penjualan
            Route::group(['prefix'  =>  'penjualan'], function () {
                Route::get('/', 'GrafikController@indexPenjualanAd')->name('admin.grafikPenjualan');
                Route::post('/', 'GrafikController@indexPenjualanAd')->name('admin.searchGrafikPenjualan');
            });
            // Grafik Member dan Non Member
            Route::group(['prefix'  =>  'member'], function () {
                Route::get('/', 'GrafikController@indexMemberAd')->name('admin.grafikMember');
                Route::post('/', 'GrafikController@indexMemberAd')->name('admin.searchGrafikMember');
            });
            // Grafik Pendapatan
            Route::group(['prefix'  =>  'pendapatan'], function () {
                Route::get('/', 'GrafikController@indexPendapatanAd')->name('admin.grafikPendapatan');
                Route::post('/', 'GrafikController@indexPendapatanAd')->name('admin.searchGrafikPendapatan');
            });
        });
    });

    Route::group(['roles'   => 'karyawan'], function () {

        Route::group(['prefix' => 'karyawan'], function () {
            Route::get('dashboard', 'HomeController@dashboardAdmin')->name('karyawan.dashboard');

            Route::get('/data/karyawan/{id}', 'TransaksiController@getKaryawan')->name('karyawan.getKaryawan');
            Route::get('/data/kurir/{id}', 'TransaksiController@getKurir')->name('karyawan.getKurir');

            Route::get('/profile', 'UserController@karyawanProfile')->name('karyawan.profile');
            Route::post('/profile/{id_user}/update', 'UserController@updateProfile')->name('karyawan.profile.update');

            Route::group(['prefix' => 'galon'], function () {
                Route::get('/', 'GalonController@indexKR')->name('karyawan.galon');
                Route::get('/data', 'GalonController@indexData')->name('karyawan.galon.data');
            });

            Route::group(['prefix' => 'kurir'], function () {
                Route::get('/', 'KurirController@indexKR')->name('karyawan.kurir');
                Route::get('/data', 'KurirController@indexData')->name('karyawan.kurir.data');
            });

            Route::group(['prefix' => 'pelanggan'], function () {
                Route::get('/', 'PelangganController@indexKR')->name('karyawan.pelanggan');
                Route::get('/data', 'PelangganController@indexData')->name('karyawan.pelanggan.data');
            });

            Route::group(['prefix' => 'desa'], function () {
                Route::get('/', 'DesaController@indexKR')->name('karyawan.desa');
                Route::get('/data', 'DesaController@indexData')->name('karyawan.desa.data');
            });

            Route::group(['prefix' => 'diskon'], function () {
                Route::get('/', 'DiskonController@indexKR')->name('karyawan.diskon');
                Route::get('/data', 'DiskonController@indexData')->name('karyawan.diskon.data');
            });

            Route::group(['prefix' => 'transaksi'], function () {
                Route::get('/', 'TransaksiController@indexKR')->name('karyawan.transaksi');
                Route::get('/data', 'TransaksiController@indexData')->name('karyawan.transaksi.data');
                Route::get('/dataDetailTransaksi/{id}', 'TransaksiController@getDetailTr')->name('karyawan.transaksi.getDetailTransaksi');
                Route::get('/{id}/konfirmasiPesanan', 'TransaksiController@konfirmasiPesanan')->name('karyawan.transaksi.konfirmasiPesanan');
                Route::get('/{id}/batalkanPesanan/{ket}', 'TransaksiController@batalkanPesanan')->name('karyawan.transaksi.batalkanPesanan');
                Route::get('/{id}/gagalkanPembayaran/{ket}', 'TransaksiController@gagalkanPembayaran')->name('karyawan.transaksi.gagalkanPembayaran');
                Route::post('/{id}/konfirmasiPembayaran', 'TransaksiController@konfirmasiPembayaran')->name('karyawan.transaksi.konfirmasiPembayaran');
                Route::get('/datakurir', 'TransaksiController@dataKurir')->name('karyawan.transaksi.dataKurir');

                Route::get('/tracking', 'TransaksiController@trackingTRKR')->name('karyawan.transaksi.tracking');

                //Admin Rekap Transaksi
                Route::get('/rekap', 'TransaksiController@rekapTrKR')->name('karyawan.transaksi.rekap');
                Route::get('/rekap/{bln}/{thn}', 'TransaksiController@dataRekapTr')->name('karyawan.transaksi.rekap.data');
                Route::get('/rekap/{bln}/{thn}/print', 'TransaksiController@dataRekapTrPrintKr')->name('karyawan.transaksi.rekap.cetak');
            });

            Route::group(['prefix'  =>  'grafik'], function () {
                // Grafik Penjualan
                Route::group(['prefix'  =>  'penjualan'], function () {
                    Route::get('/', 'GrafikController@indexPenjualan')->name('karyawan.grafikPenjualan');
                    Route::post('/', 'GrafikController@indexPenjualan')->name('karyawan.searchGrafikPenjualan');
                });
                // Grafik Member dan Non Member
                Route::group(['prefix'  =>  'member'], function () {
                    Route::get('/', 'GrafikController@indexMember')->name('karyawan.grafikMember');
                    Route::post('/', 'GrafikController@indexMember')->name('karyawan.searchGrafikMember');
                });
                // Grafik Pendapatan
                Route::group(['prefix'  =>  'pendapatan'], function () {
                    Route::get('/', 'GrafikController@indexPendapatan')->name('karyawan.grafikPendapatan');
                    Route::post('/', 'GrafikController@indexPendapatan')->name('karyawan.searchGrafikPendapatan');
                });
            });
        });
    });
});
