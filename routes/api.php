<?php

use App\Http\Controllers\Api\ApiAuthControlller;
use App\Http\Controllers\Api\ApiGalonControlller;
use App\Http\Controllers\Api\ApiLaporanKurirControlller;
use App\Http\Controllers\Api\ApiTransaksiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('api-login', [ApiAuthControlller::class, 'login'])->name('apilogin');
Route::post('api-register', [ApiAuthControlller::class, 'register'])->name('apiregister');
Route::post('api-logout', [ApiAuthControlller::class, 'logout'])->name('apilogout')->middleware('auth:sanctum');

// Update Profile
Route::get('api-update/{id}', [ApiAuthControlller::class, 'edit'])->name('apiedit')->middleware('auth:sanctum');
Route::post('api-update/{id}', [ApiAuthControlller::class, 'update'])->name('apiupdate')->middleware('auth:sanctum');


// Get Galon
Route::get('api-galon', [ApiGalonControlller::class, 'index'])->name('apigalon')->middleware('auth:sanctum');

// Get Desa
Route::get('api-desa', [ApiAuthControlller::class, 'listDesa'])->name('apidesa');

// Laporan Kurir
Route::get('api-laporan-kurir/{id}', [ApiLaporanKurirControlller::class, 'index'])->name('apilaporankurir')->middleware('auth:sanctum');

// Transaksi
Route::get('api-transaksi/pelanggan/transaksi/', [ApiTransaksiController::class, 'index'])->name('apilisttransaksi')->middleware('auth:sanctum');
Route::post('api-transaksi/pelanggan/transaksi/create', [ApiTransaksiController::class, 'create'])->name('apitransaksicreate')->middleware('auth:sanctum');
Route::post('api-transaksi/pelanggan/transaksi/update', [ApiTransaksiController::class, 'update'])->name('apitransaksiupdate')->middleware('auth:sanctum');

// List Order
Route::get('api-transaksi/kurir/order', [ApiTransaksiController::class, 'listOrder'])->name('apikurirorder')->middleware('auth:sanctum');
Route::post('api-transaksi/kurir/order', [ApiTransaksiController::class, 'listOrderUpdate'])->name('apikurirorderupdate')->middleware('auth:sanctum');
