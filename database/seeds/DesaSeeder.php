<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DesaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('desas')->insert([
            'nama_desa' => 'Desa Kediri',
            'ongkir' => 3000
        ]);
    }
}
