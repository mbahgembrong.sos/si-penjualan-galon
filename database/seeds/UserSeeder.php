<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'username' => 'admin',
            'password' => Hash::make('admin'),
            'id_role' => 1,
        ], [
            'username' => 'karyawan',
            'password' => Hash::make('karyawan'),
            'id_role' => 2,
        ], [
            'username' => 'jukir',
            'password' => Hash::make('jukir'),
            'id_role' => 3,
        ], [
            'username' => 'pelanggan',
            'password' => Hash::make('pelanggan'),
            'id_role' => 4,
        ]]);
    }
}
