<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JukirSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jukirs')->insert([
            'nama' => 'jukir1',
            'tempat_lahir' => 'Kediri',
            'tanggal_lahir' => '2022-01-01',
            'jenis_kelamin' => 'L',
            'alamat' => 'Kediri',
            'id_user' => 3
        ]);
    }
}
