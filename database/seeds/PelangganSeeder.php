<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PelangganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pelanggans')->insert([
            'nama' => 'Pelanggan1',
            'tempat_lahir' => 'Kediri',
            'tanggal_lahir' => '2022-01-01',
            'jenis_kelamin' => 'L',
            'alamat' => 'Kediri',
            'id_desa' => 1,
            'id_user' => 4
        ]);
    }
}
