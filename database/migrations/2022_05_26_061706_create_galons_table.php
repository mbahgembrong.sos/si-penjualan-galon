<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGalonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galons', function (Blueprint $table) {
            $table->id();
            $table->string('merk');
            $table->string('isi_galon', 64);
            $table->unsignedBigInteger('jml_stok')->default(0);
            $table->unsignedBigInteger('harga_awal');
            $table->unsignedBigInteger('harga_jual');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galons');
    }
}
