<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_pelanggan');
            $table->unsignedBigInteger('id_karyawan');
            $table->unsignedBigInteger('id_jukir');
            $table->unsignedBigInteger('id_diskon')->nullable();
            $table->unsignedBigInteger('total');
            $table->string('status', 1)->default('0');
            //0 = belum dikonfirmasi, 1 = belum dibayar, 2 = pembayaran gagal ,3 = belum dikirim, 4 = selesai
            //, 5 = dibatalkan
            $table->text('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
