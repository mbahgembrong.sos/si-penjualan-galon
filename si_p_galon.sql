-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 11, 2022 at 07:04 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `si_p_galon`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `id_user`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', 'Kediri', '2022-05-30', 'L', 'Jln. mojoroto, kediri', 1, NULL, '2022-06-03 05:44:37');

-- --------------------------------------------------------

--
-- Table structure for table `desas`
--

CREATE TABLE `desas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_desa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ongkir` bigint(16) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `desas`
--

INSERT INTO `desas` (`id`, `nama_desa`, `ongkir`, `created_at`, `updated_at`) VALUES
(1, 'Desa Kediri', 3000, NULL, NULL),
(2, 'Desa Mojoroto', 5000, NULL, NULL),
(4, 'Desa Ngasem', 10000, '2022-06-08 02:44:16', '2022-06-08 02:44:16');

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksis`
--

CREATE TABLE `detail_transaksis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_transaksi` bigint(20) UNSIGNED NOT NULL,
  `id_galon` bigint(20) UNSIGNED NOT NULL,
  `harga` bigint(32) UNSIGNED NOT NULL,
  `jumlah` bigint(16) UNSIGNED NOT NULL,
  `total_harga` bigint(64) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_transaksis`
--

INSERT INTO `detail_transaksis` (`id`, `id_transaksi`, `id_galon`, `harga`, `jumlah`, `total_harga`, `created_at`, `updated_at`) VALUES
(3, 2, 1, 18000, 12, 216000, '2022-06-09 11:06:19', '2022-06-09 11:06:19'),
(4, 2, 2, 7000, 20, 140000, '2022-06-09 11:08:00', '2022-06-09 11:08:00'),
(5, 3, 1, 18000, 5, 90000, '2022-06-09 11:06:19', '2022-06-09 11:06:19'),
(6, 3, 2, 7000, 10, 70000, '2022-06-09 11:08:00', '2022-06-09 11:08:00'),
(15, 16, 2, 9000, 5, 45000, '2022-07-07 03:06:47', '2022-07-07 03:06:47'),
(16, 16, 6, 25000, 5, 125000, '2022-07-07 03:06:47', '2022-07-07 03:06:47');

-- --------------------------------------------------------

--
-- Table structure for table `diskons`
--

CREATE TABLE `diskons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_diskon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_potongan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diskon` bigint(64) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `diskons`
--

INSERT INTO `diskons` (`id`, `nama_diskon`, `jenis_potongan`, `diskon`, `created_at`, `updated_at`) VALUES
(0, 'Tanpa Diskon', 'Langsung', 0, NULL, NULL),
(1, 'Diskon Member', 'Langsung', 15000, '2022-06-08 09:46:57', '2022-06-08 09:46:57'),
(2, 'Diskon Promo 2022', 'Persen', 10, '2022-06-08 03:15:52', '2022-06-08 03:21:48');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `galons`
--

CREATE TABLE `galons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `merk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi_galon` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jml_stok` bigint(16) UNSIGNED NOT NULL DEFAULT 0,
  `harga_awal` bigint(64) UNSIGNED NOT NULL,
  `harga_jual` bigint(64) UNSIGNED NOT NULL,
  `gambar_galon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galons`
--

INSERT INTO `galons` (`id`, `merk`, `isi_galon`, `jml_stok`, `harga_awal`, `harga_jual`, `gambar_galon`, `created_at`, `updated_at`) VALUES
(1, 'Aqua', '19', 5, 17000, 18000, '1655203570.jpg', '2022-06-04 09:55:56', '2022-07-04 06:49:57'),
(2, 'Cleo', '19', 18, 7000, 9000, '1655203770.jpg', '2022-06-05 05:58:08', '2022-07-07 04:27:42'),
(6, 'Le Minerale', '17', 22, 20000, 25000, '1657020050.png', '2022-07-01 03:51:41', '2022-07-07 04:27:42');

-- --------------------------------------------------------

--
-- Table structure for table `karyawans`
--

CREATE TABLE `karyawans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `karyawans`
--

INSERT INTO `karyawans` (`id`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `id_user`, `created_at`, `updated_at`) VALUES
(1, 'Karyawan1', 'Kediri', '2022-01-01', 'L', 'Kediri', 2, NULL, NULL),
(7, 'pakjas', 'kediri', '2022-06-08', 'L', 'kediri wates', 16, '2022-06-03 05:31:59', '2022-06-03 05:31:59');

-- --------------------------------------------------------

--
-- Table structure for table `kurirs`
--

CREATE TABLE `kurirs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kurirs`
--

INSERT INTO `kurirs` (`id`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `id_user`, `created_at`, `updated_at`) VALUES
(1, 'Kurir nomer 1', 'Kediri', '2022-01-01', 'L', 'Kediri', 3, NULL, '2022-07-08 03:43:25'),
(5, 'yosi', 'kediri', '2022-06-09', 'L', 'pojok', 15, '2022-06-03 05:28:08', '2022-06-03 05:28:08');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(115, '2014_10_12_000000_create_users_table', 1),
(116, '2014_10_12_100000_create_password_resets_table', 1),
(117, '2019_08_19_000000_create_failed_jobs_table', 1),
(118, '2022_05_26_051219_create_roles_table', 1),
(119, '2022_05_26_053421_create_transaksis_table', 1),
(120, '2022_05_26_053437_create_karyawans_table', 1),
(121, '2022_05_26_053526_create_jukirs_table', 1),
(122, '2022_05_26_053618_create_pelanggans_table', 1),
(123, '2022_05_26_054726_create_diskons_table', 1),
(124, '2022_05_26_055059_create_desas_table', 1),
(125, '2022_05_26_061706_create_galons_table', 1),
(126, '2022_05_26_073103_create_detail_transaksis_table', 1),
(127, '2022_05_26_081948_create_pembelians_table', 1),
(128, '2022_05_27_084141_create_admins_table', 1),
(129, '2019_12_14_000001_create_personal_access_tokens_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pelanggans`
--

CREATE TABLE `pelanggans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `member` tinyint(1) NOT NULL DEFAULT 0,
  `id_desa` bigint(20) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `lang` double DEFAULT NULL,
  `long` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pelanggans`
--

INSERT INTO `pelanggans` (`id`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `member`, `id_desa`, `id_user`, `lang`, `long`, `created_at`, `updated_at`) VALUES
(1, 'Pelanggan1', 'Kediri', '2022-01-01', 'L', 'Kediri', 1, 1, 4, NULL, NULL, NULL, '2022-06-09 04:10:42'),
(4, 'Dicky Septiana', 'Kediri', '1999-09-18', 'L', 'Jl. Mojoroto no 32, Desa Mojoroto, Kediri', 0, 2, 19, NULL, NULL, '2022-06-09 04:12:07', '2022-06-09 04:12:07'),
(5, 'Regavit Setiono', 'Kediri', '1996-05-03', 'L', 'Mojoroto, Gg 3', 0, 2, 20, NULL, NULL, '2022-07-07 01:30:16', '2022-07-09 06:00:15');

-- --------------------------------------------------------

--
-- Table structure for table `pembelians`
--

CREATE TABLE `pembelians` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_galon` bigint(20) UNSIGNED NOT NULL,
  `harga_satuan` bigint(64) UNSIGNED NOT NULL,
  `jumlah_beli` bigint(16) UNSIGNED NOT NULL,
  `total_harga_beli` bigint(64) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pembelians`
--

INSERT INTO `pembelians` (`id`, `id_galon`, `harga_satuan`, `jumlah_beli`, `total_harga_beli`, `created_at`, `updated_at`) VALUES
(1, 2, 5000, 50, 250000, '2022-06-08 06:36:45', '2022-06-08 06:36:45'),
(2, 1, 15000, 20, 300000, '2022-06-08 06:40:03', '2022-06-08 06:40:03'),
(5, 6, 20000, 22, 440000, '2022-07-01 03:52:57', '2022-07-01 03:52:57');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(39, 'App\\User', 3, 'Infinix', '589d17c51a6c689efccb20fc2ae793ddeca16cfb6ecfb3d797a274db79c30a9d', '[\"*\"]', '2022-07-09 10:01:19', '2022-07-08 04:35:38', '2022-07-09 10:01:19'),
(40, 'App\\User', 3, 'Infinix', 'bded32bebd7f4469b4ef8fbaaeaf6bb8a5a9ce80faf1b34c6fd3f6e820921fad', '[\"*\"]', NULL, '2022-07-08 05:02:56', '2022-07-08 05:02:56'),
(41, 'App\\User', 3, 'Infinix', 'ab14fa4984934ebb5130bf81cc316543d7827b3d75d1af47c48f42212beba3d0', '[\"*\"]', NULL, '2022-07-08 05:04:41', '2022-07-08 05:04:41'),
(42, 'App\\User', 3, 'Infinix', 'dfc874d41425f79b235458a7b190a71d9060c41255aa26c1c29f37a45d3c8ecc', '[\"*\"]', NULL, '2022-07-08 05:06:25', '2022-07-08 05:06:25'),
(43, 'App\\User', 3, 'Infinix', '4e515921baaed50161057d9210f850f62078b97335fffdc469b9d13aaaccc141', '[\"*\"]', NULL, '2022-07-08 05:07:00', '2022-07-08 05:07:00'),
(44, 'App\\User', 3, 'Infinix', '83c3ed32277c2a514bbb442ab718181a0999547d6588775e95ecd29790d025cb', '[\"*\"]', NULL, '2022-07-08 06:55:21', '2022-07-08 06:55:21'),
(45, 'App\\User', 3, 'Infinix', 'af88b163aa6c6852755f5dc52f67199cbe72459cb1edd5a97a1ec18137645d0d', '[\"*\"]', NULL, '2022-07-08 06:55:41', '2022-07-08 06:55:41'),
(46, 'App\\User', 3, 'Infinix', '9c792da423d9b16160b50a7509f7b4091aaa22506f0b1a3aad2d52b1f18ac624', '[\"*\"]', NULL, '2022-07-08 06:56:25', '2022-07-08 06:56:25'),
(47, 'App\\User', 3, 'Infinix', '2f55ebeb0da553f058c4a804d83f23a5863fa0fcb7d8080853c564250a88f435', '[\"*\"]', NULL, '2022-07-08 06:56:44', '2022-07-08 06:56:44'),
(48, 'App\\User', 3, 'Infinix', '7f31260b9cbd7091f549a7ba5b2bf1c0625ed617ab7679d9bffbfef03a8ce655', '[\"*\"]', NULL, '2022-07-08 06:57:05', '2022-07-08 06:57:05'),
(49, 'App\\User', 3, 'Infinix', '54d327afc47c4a600adf6b786d84d1de5c4b551eade6ff43d6d93f5856ca33bd', '[\"*\"]', NULL, '2022-07-08 06:58:36', '2022-07-08 06:58:36'),
(50, 'App\\User', 3, 'Infinix', 'ef6963aad1ad7b91e6f1d72480d6abb4191aee3fb5bfb6c224c0a70aec407cd6', '[\"*\"]', NULL, '2022-07-08 06:59:56', '2022-07-08 06:59:56'),
(51, 'App\\User', 3, 'Infinix', '050f8e920274d5ae3ce17456faa07c7af1c6b605e70b8dbf21882c7c84a2ebd9', '[\"*\"]', NULL, '2022-07-08 07:00:23', '2022-07-08 07:00:23'),
(52, 'App\\User', 3, 'Infinix', 'f3c919be32acc4d4e51c4ddc247b3ef5834af46e2c09bb292954e7bb177ca888', '[\"*\"]', NULL, '2022-07-08 07:02:39', '2022-07-08 07:02:39'),
(53, 'App\\User', 3, 'Infinix', 'a287dc5c098fa60e2f82c0cb0c8f9b5ad6ab09226427e9818d2ff277c564e28c', '[\"*\"]', NULL, '2022-07-08 07:02:52', '2022-07-08 07:02:52'),
(54, 'App\\User', 3, 'Infinix', '02002b037a9724c1945142ccc83027967935d55f32afb7e435154b2f2db05b4b', '[\"*\"]', NULL, '2022-07-08 07:05:10', '2022-07-08 07:05:10'),
(55, 'App\\User', 3, 'Infinix', '05c378bd44fb87df5856b96379949f3df21a94ab6097e0d2ac0754e3122f4b14', '[\"*\"]', NULL, '2022-07-08 07:05:45', '2022-07-08 07:05:45'),
(56, 'App\\User', 3, 'Infinix', '139c73ab4799bddb97e5b6a425c68d51ab516fea5dd78f235bf0dfdc42608704', '[\"*\"]', NULL, '2022-07-08 07:30:37', '2022-07-08 07:30:37'),
(57, 'App\\User', 4, 'Infinix', 'b039442cb511e86dc5dd83788fb11605f21faf074414ee13ece980df80274e6a', '[\"*\"]', NULL, '2022-07-08 07:30:56', '2022-07-08 07:30:56'),
(58, 'App\\User', 4, 'Infinix', 'e4d38608490d70a6cfb08b2f7095adbfdbfa421d52044e11853744b6f208a719', '[\"*\"]', NULL, '2022-07-08 07:31:14', '2022-07-08 07:31:14'),
(59, 'App\\User', 4, 'Infinix', '4af55a8ae4d399d2a498f23d32e9c04098750e4c42f67f9702d2063383a40da4', '[\"*\"]', NULL, '2022-07-08 07:32:16', '2022-07-08 07:32:16'),
(60, 'App\\User', 4, 'Infinix', '03b3862dc78df2611ea662a4dda87743848e88ef7a2b7a2fe7311bbb2c4f73eb', '[\"*\"]', NULL, '2022-07-08 07:32:32', '2022-07-08 07:32:32'),
(61, 'App\\User', 4, 'Infinix', 'bbf00f61004cbcf6d1733be74c213faef70d01c0bddb20b3a1e9470f8a1ff19b', '[\"*\"]', NULL, '2022-07-08 07:33:02', '2022-07-08 07:33:02'),
(62, 'App\\User', 4, 'Infinix', 'bacb62860f14dbb6352d80422e43f309adbc71c44ba588196b4ae2978b4fe8c5', '[\"*\"]', NULL, '2022-07-08 07:34:05', '2022-07-08 07:34:05'),
(63, 'App\\User', 4, 'Infinix', '9c411038b12f3682c31bd3d5b8d01c67d5aa4bd4305b18f3d6bbb30d4ff296e8', '[\"*\"]', NULL, '2022-07-08 07:34:15', '2022-07-08 07:34:15'),
(64, 'App\\User', 4, 'Infinix', 'b00ee1c03085d31bda3358eee8af02f7c04a7806e2e18d6d16f43e03040f0995', '[\"*\"]', NULL, '2022-07-08 07:34:28', '2022-07-08 07:34:28'),
(65, 'App\\User', 4, 'Infinix', '49190d053c18bbdb30cacb9a873313f19ac6642e1fb3b2071a66e0acd6133da3', '[\"*\"]', NULL, '2022-07-08 07:34:33', '2022-07-08 07:34:33'),
(66, 'App\\User', 4, 'Infinix', '62fa936d2f1b1220a56d7c6fe961ab039e18d3e5a116d40ec1768c9eb93b4ddf', '[\"*\"]', NULL, '2022-07-09 05:54:39', '2022-07-09 05:54:39'),
(67, 'App\\User', 4, 'Infinix', '1719f3ef94e72dca1e032b40e09df57e2d1ccefe93bc3e2d8a305163a487bbd2', '[\"*\"]', NULL, '2022-07-09 06:01:48', '2022-07-09 06:01:48');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_role` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `nama_role`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'karyawan', NULL, NULL),
(3, 'kurir', NULL, NULL),
(4, 'pelanggan', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaksis`
--

CREATE TABLE `transaksis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_pelanggan` bigint(20) UNSIGNED NOT NULL,
  `id_karyawan` bigint(20) UNSIGNED DEFAULT NULL,
  `id_kurir` bigint(20) UNSIGNED DEFAULT NULL,
  `id_diskon` bigint(20) UNSIGNED DEFAULT NULL,
  `jenis_potongan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `diskon` bigint(32) UNSIGNED DEFAULT NULL,
  `ongkir` bigint(32) UNSIGNED DEFAULT NULL,
  `total` bigint(64) UNSIGNED NOT NULL,
  `bukti_transaksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksis`
--

INSERT INTO `transaksis` (`id`, `id_pelanggan`, `id_karyawan`, `id_kurir`, `id_diskon`, `jenis_potongan`, `diskon`, `ongkir`, `total`, `bukti_transaksi`, `status`, `keterangan`, `created_at`, `updated_at`) VALUES
(2, 1, NULL, 5, 1, 'Langsung', 15000, 3000, 344000, NULL, '0', '-', '2022-06-09 11:01:00', '2022-07-02 22:04:28'),
(3, 4, 1, 5, 2, 'Persen', 10, 5000, 149000, NULL, '1', '-', '2022-06-09 11:14:33', '2022-07-04 06:49:57'),
(16, 5, 1, 5, 0, 'Langsung', 0, 5000, 175000, '1657241221.jpg', '3', '-', '2022-07-07 03:06:47', '2022-07-09 10:16:35'),
(17, 5, NULL, NULL, 0, 'Langsung', 0, 5000, 0, NULL, '0', '-', '2022-07-09 09:19:02', '2022-07-09 09:19:02'),
(18, 5, NULL, NULL, 0, 'Langsung', 0, 5000, 0, NULL, '0', '-', '2022-07-09 09:20:44', '2022-07-09 09:20:44'),
(19, 5, NULL, NULL, 0, 'Langsung', 0, 5000, 0, NULL, '0', '-', '2022-07-09 09:21:47', '2022-07-09 09:21:47'),
(20, 5, NULL, NULL, 0, 'Langsung', 0, 5000, 0, NULL, '0', '-', '2022-07-09 09:26:12', '2022-07-09 09:26:12'),
(21, 5, NULL, NULL, 0, 'Langsung', 0, 5000, 0, NULL, '0', '-', '2022-07-09 09:38:30', '2022-07-09 09:38:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` bigint(20) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$9K/P6MDuixwTrpes/y6gju5nMb2tLXBSzBvefEbVQfXrKT2u1lpkS', 1, NULL, NULL, '2022-06-03 05:44:37'),
(2, 'karyawan', '$2y$10$EH..c9Fc6t9KykLcBbRhwe6hSpGw0T1zsYWTEgKjHDXhsJpNwqJbm', 2, NULL, NULL, NULL),
(3, 'kurirtest', '$2y$10$6v8w19B8EDqlK.aUW4vPQ.ewwS7.kETgrvGrWREiZ2B7VwK/W4HbO', 3, NULL, NULL, '2022-07-08 03:43:25'),
(4, 'pelanggan', '$2y$10$dtH9jfHjjIDPl/zatshPDuZtJDtijmKTOnjXH64hwUn0t0JWEMnPy', 4, NULL, NULL, NULL),
(15, 'yosi saputro', '$2y$10$bB/YgPvwG5qB1VLvZKpQL.9OkIfz9DcX/k1BYrYx.cFy3XbqkG5li', 3, NULL, '2022-06-03 05:28:08', '2022-06-03 05:28:08'),
(16, 'yosisaputro', '$2y$10$OrJKOjvoIsY0Qfb9Ldv4KO5X/WhHyZ6kn13a9V7cYkTaJ.DjFcj2a', 2, NULL, '2022-06-03 05:31:59', '2022-06-03 05:31:59'),
(19, 'dickysepti', '$2y$10$XNHRdO08qWy5uj3Kpa2O5.XSGKcmvPMQJ.mzqNcGXJHx.cZHVPGsW', 4, NULL, '2022-06-09 04:12:07', '2022-06-09 04:12:07'),
(20, 'regavitpl1', '$2y$10$kKM0Pzh9SHxULn18wNaXCuMwEUkGNzSwZQzPjqaKO6XHAlVIt9l8C', 4, NULL, '2022-07-07 01:16:30', '2022-07-09 06:00:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `desas`
--
ALTER TABLE `desas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_transaksis`
--
ALTER TABLE `detail_transaksis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_transaksi` (`id_transaksi`,`id_galon`),
  ADD KEY `id_galon` (`id_galon`);

--
-- Indexes for table `diskons`
--
ALTER TABLE `diskons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galons`
--
ALTER TABLE `galons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `karyawans`
--
ALTER TABLE `karyawans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `kurirs`
--
ALTER TABLE `kurirs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pelanggans`
--
ALTER TABLE `pelanggans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_desa` (`id_desa`,`id_user`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `pembelians`
--
ALTER TABLE `pembelians`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_galon` (`id_galon`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksis`
--
ALTER TABLE `transaksis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pelanggan` (`id_pelanggan`,`id_karyawan`,`id_kurir`,`id_diskon`),
  ADD KEY `id_diskon` (`id_diskon`),
  ADD KEY `id_karyawan` (`id_karyawan`),
  ADD KEY `id_kurir` (`id_kurir`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD KEY `role` (`role`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `desas`
--
ALTER TABLE `desas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `detail_transaksis`
--
ALTER TABLE `detail_transaksis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `diskons`
--
ALTER TABLE `diskons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `galons`
--
ALTER TABLE `galons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `karyawans`
--
ALTER TABLE `karyawans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kurirs`
--
ALTER TABLE `kurirs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `pelanggans`
--
ALTER TABLE `pelanggans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pembelians`
--
ALTER TABLE `pembelians`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transaksis`
--
ALTER TABLE `transaksis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
